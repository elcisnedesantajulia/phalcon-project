<?php
declare(strict_types=1);

use App\Providers\AuthProvider;
use App\Providers\CacheProvider;
use App\Providers\ConfigProvider;
use App\Providers\DbProvider;
use App\Providers\DispatcherProvider;
use App\Providers\MenuProvider;
use App\Providers\SessionProvider;
use App\Providers\ViewProvider;

return [
    AuthProvider::class,
    ConfigProvider::class, // Config must be first than Cache and Db
    CacheProvider::class,
    DbProvider::class,
    DispatcherProvider::class,
    MenuProvider::class,
    SessionProvider::class,
    ViewProvider::class,
];
