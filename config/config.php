<?php
declare(strict_types=1);

use function App\root_path;

return [
    'database' => [
        'host' => getenv('DB_HOST'),
        'username' => getenv('DB_USERNAME'),
        'password' => getenv('DB_PASSWORD'),
        'dbname' => getenv('DB_NAME'),

    ],
    'application' => [
        'baseUri' => getenv('APP_BASE_URI'),
        'viewsDir' => root_path('themes/gentelella/'),
        'cacheDir' => root_path('var/cache/'),
        'logsDir' => root_path('var/logs/'),
    ],
];
