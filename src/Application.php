<?php
declare(strict_types=1);


namespace App;


use Exception;
use Phalcon\Di\DiInterface;
use Phalcon\Di\FactoryDefault;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Http\ResponseInterface;
use Phalcon\Mvc\Application as MvcApplication;

class Application
{
    const APPLICATION_PROVIDER = 'bootstrap';

    /** @var MvcApplication */
    protected $app;
    /** @var DiInterface */
    protected $di;
    /** @var string */
    protected $rootPath;

    /**
     * Application constructor.
     * @param string $rootPath
     * @throws Exception
     */
    function __construct(string $rootPath)
    {
        $this->di = new FactoryDefault();
        $this->app = $this->createApplication();
        $this->rootPath = $rootPath;

        $this->di->setShared(self::APPLICATION_PROVIDER, $this);

        $this->initializeProviders();
    }

    function getRootPath(): string
    {
        return $this->rootPath;
    }

    function run(): string
    {
        //$uri = key_exists('_url', $_REQUEST) ? $_REQUEST['_url'] : '/';
        $uri = $_REQUEST['_url'] ?? '/';
        /** @var ResponseInterface $response */
        $response = $this->app->handle($uri);

        return (string) $response->getContent();
    }

    private function createApplication(): MvcApplication
    {
        return new MvcApplication($this->di);
    }

    /**
     * @throws Exception
     */
    private function initializeProviders(): void
    {
        $filename = "{$this->rootPath}/config/providers.php";
        if (!file_exists($filename) || !is_readable($filename)) {
            throw new Exception('File providers.php does not exist or is not readable');
        }

        /** @noinspection PhpIncludeInspection */
        $providers = include_once $filename;
        foreach ($providers as $providerClass) {
            /** @var ServiceProviderInterface $provider */
            $provider = new $providerClass;
            $provider->register($this->di);
        }
    }
}
