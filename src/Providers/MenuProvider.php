<?php
declare(strict_types=1);


namespace App\Providers;


use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Rcc\Phalcon\Gui\Menus\Main\Menu;

class MenuProvider implements ServiceProviderInterface
{
    protected $providerName = 'menu';

    /**
     * @inheritDoc
     */
    function register(DiInterface $di): void
    {
        $di->setShared($this->providerName, function () {
            return new Menu();
        });
    }
}
