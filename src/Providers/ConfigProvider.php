<?php
declare(strict_types=1);


namespace App\Providers;


use App\Application;
use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface
{
    protected $providerName = 'config';

    /**
     * @inheritDoc
     */
    function register(DiInterface $di): void
    {
        /** @var Application $application */
        $application = $di->getShared(Application::APPLICATION_PROVIDER);
        $rootPath = $application->getRootPath();

        $di->setShared($this->providerName, function () use ($rootPath) {
            /** @noinspection PhpIncludeInspection */
            $config = include "{$rootPath}/config/config.php";

            return new Config($config);
        });
    }
}
