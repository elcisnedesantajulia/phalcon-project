<?php
declare(strict_types=1);
/**
 * @Filename: CacheService.php
 * @Description:
 * @CreatedAt: 27/04/20 14:15
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Providers;


class CacheService
{
    /** @var string */
    private $name;
    /** @var string */
    private $dirName;
    /** @var int */
    private $lifetime_secs;

    /**
     * CacheService constructor.
     * @param string $name
     * @param string $dirName
     * @param int $ttl_secs
     */
    function __construct(string $name, string $dirName, int $ttl_secs = 3600)
    {
        $this->name = $name;
        $this->dirName = $dirName;
        $this->lifetime_secs = $ttl_secs;
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    function getLifetimeSecs(): int
    {
        return $this->lifetime_secs;
    }

    function generateFilePath(string $cacheDir): string
    {
        return "{$cacheDir}{$this->dirName}/";
    }
}
