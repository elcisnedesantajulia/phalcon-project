<?php
declare(strict_types=1);


namespace App\Providers;


use Phalcon\Config;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class DbProvider implements ServiceProviderInterface
{
    protected $providerName = 'db';

    /**
     * @inheritDoc
     */
    function register(DiInterface $di): void
    {
        /** @var Config $config */
        $config = $di->getShared('config')->get('database');

        $di->setShared($this->providerName, function () use ($config) {
            return new Mysql($config->toArray());
        });
    }
}
