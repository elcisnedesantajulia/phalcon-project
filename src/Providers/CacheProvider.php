<?php
declare(strict_types=1);
/**
 * @Filename: CacheProvider.php
 * @Description:
 * @CreatedAt: 27/04/20 14:14
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Providers;


use Phalcon\Cache;
use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;

class CacheProvider implements ServiceProviderInterface
{
    /** @var CacheService[] */
    private $services;

    function __construct()
    {
        $this->services = [
            new CacheService('genericCache', 'generic', 5 * 60),
            new CacheService('zprofilesCache', 'zprofiles', 7 * 24 * 60 * 60),
        ];
    }

    function register(DiInterface $di): void
    {
        foreach ($this->services as $service) {
            $this->registerService($di, $service);
        }
    }

    /**
     * @return string[]
     */
    function generateServicesList(): array
    {
        $output = [];
        foreach ($this->services as $service) {
            $output[] = $service->getName();
        }

        return $output;
    }

    private function registerService(DiInterface $di, CacheService $service)
    {
        /** @var Config $config */
        $config = $di->getShared('config');
        $cacheDir = (string) $config->path('application.cacheDir');

        $di->setShared($service->getName(), function () use ($service, $cacheDir) {
            $serializerFactory = new SerializerFactory();
            $adapter = new Cache\Adapter\Stream($serializerFactory, [
                'defaultSerializer' => 'Php',
                'lifetime' => $service->getLifetimeSecs(),
                'storageDir' => $service->generateFilePath($cacheDir),
            ]);

            return new Cache($adapter);
        });
    }
}
