<?php
declare(strict_types=1);
/**
 * @Filename: SessionProvider.php
 * @Description:
 * @CreatedAt: 30/04/20 21:50
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Providers;


use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Session\Adapter\Stream as SessionAdapter;
use Phalcon\Session\Manager as SessionManager;

class SessionProvider implements ServiceProviderInterface
{
    /** @var string */
    protected $providerName = 'session';

    function register(DiInterface $di): void
    {
        /** @var Config $config */
        $config = $di->getShared('config');
        $handler = new SessionAdapter([
            'savePath' => "{$config->path('application.cacheDir')}session/",
        ]);

        $di->set($this->providerName, function () use ($handler) {
            $session = new SessionManager();
            $session->setAdapter($handler);
            $session->start();

            return $session;
        });
    }
}
