<?php
declare(strict_types=1);


namespace App\Providers;


use Exception;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Dispatcher\Exception as DispatchException;
use Phalcon\Events\Event;
use Phalcon\Events\Manager;
use Phalcon\Mvc\Dispatcher;

class DispatcherProvider implements ServiceProviderInterface
{
    protected $providerName = 'dispatcher';

    /**
     * @inheritDoc
     * @noinspection PhpUnusedParameterInspection
     */
    function register(DiInterface $di): void
    {
        $di->set($this->providerName, function () {
            $eventsManager = new Manager();
            $eventsManager->attach(
                'dispatch:beforeException',
                function (Event $event, Dispatcher $dispatcher, Exception $exception) {
                    if ($exception instanceof DispatchException) {
                        $dispatcher->forward([
                            'controller' => 'error',
                            'action' => 'show404',
                        ]);

                        return false;
                    }

                    return true;
                }
            );

            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('App\Controllers');
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        });
    }
}
