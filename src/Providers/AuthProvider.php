<?php
declare(strict_types=1);
/**
 * @Filename: AuthProvider.php
 * @Description:
 * @CreatedAt: 01/05/20 22:26
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Providers;


use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Rcc\Phalcon\Acl\Auth;

class AuthProvider implements ServiceProviderInterface
{
    protected $providerName = 'auth';

    function register(DiInterface $di): void
    {
        $di->setShared($this->providerName, Auth::class);
    }
}
