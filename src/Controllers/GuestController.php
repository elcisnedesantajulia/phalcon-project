<?php
declare(strict_types=1);
/**
 * @Filename: GuestController.php
 * @Description:
 * @CreatedAt: 30/04/20 13:08
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Controllers;


use Rcc\Phalcon\Acl\Exception as AclException;
use Rcc\Phalcon\Acl\Handler as AclHandler;
use Rcc\Phalcon\Ajax\Exception as AjaxException;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Phalcon\Utils\LoggerFactory;

class GuestController extends ControllerBase
{
    function initialize()
    {
        if ($this->user->isLoggedIn()) {
            $this->response->redirect('')->send();
        }
    }

    function indexAction()
    {

    }

    function ajaxLoginAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->login()->toArray())->send();
        } catch (AclException $e) {
            LoggerFactory::generic()->warning("guest/ajaxLogin aclException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        }
    }

    function ajaxRegisterAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->register()->toArray())->send();
        } catch (AclException $e) {
            LoggerFactory::generic()->warning("guest/ajaxRegister aclException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("guest/ajaxRegister ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        } catch (CacheException $e) {
            LoggerFactory::generic()->warning("guest/ajaxRegister cacheException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal system error')->setJsonContent('Internal system error')->send();
        }
    }
}
