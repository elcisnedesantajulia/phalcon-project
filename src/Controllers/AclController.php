<?php
declare(strict_types=1);
/**
 * @Filename: AclController.php
 * @Description:
 * @CreatedAt: 05/05/20 13:42
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Controllers;


use Rcc\Phalcon\Acl\Exception;
use Rcc\Phalcon\Acl\Handler as AclHandler;
use Rcc\Phalcon\Acl\Users\Catalogs\Main;
use Rcc\Phalcon\Ajax\Exception as AjaxException;
use Rcc\Phalcon\Ajax\Response\Response;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Phalcon\Utils\LoggerFactory;
use Rcc\Views\Users\AclUsersList;
use Rcc\Views\Users\ModalsFactory;

class AclController extends ControllerBase
{
    function initialize()
    {
        $this->view->setLayout('index');
    }

    function usersAction()
    {
        try {
            $catalog = Main::readFromDb();
            $this->view->setVar('usersList', new AclUsersList($catalog));
            $this->view->setVar('newUserModal', ModalsFactory::newUser());
            $this->view->setVar('editUserModal', ModalsFactory::editUser());
            $this->view->setVar('restorePasswordModal', ModalsFactory::restorePassword());
            $this->view->setVar('blockUserModal', ModalsFactory::blockUser());
            $this->view->setVar('unblockUserModal', ModalsFactory::unblockUser());
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/users aclException {$e->getMessage()}");
            $this->response->redirect()->send();
        } catch (CacheException $e) {
            LoggerFactory::generic()->warning("acl/users cacheException {$e->getMessage()}");
            $this->response->redirect()->send();
        }
    }

    function profilesAction()
    {

    }

    function ajaxNewUserAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->newUser()->toArray())->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("acl/ajaxNewUser ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/ajaxNewUser aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (CacheException $e) {
            LoggerFactory::generic()->warning("acl/ajaxNewUser cacheException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        }
    }

    function ajaxEditUserAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->editUser()->toArray())->send();
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/ajaxEditUser aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("acl/ajaxEditUser ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        }
    }

    function ajaxBlockUserAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->blockUser()->toArray())->send();
            //$this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/ajaxBlockUser aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("acl/ajaxBlockUser ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        }
    }

    function ajaxUnblockUserAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->unblockUser()->toArray())->send();
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/ajaxUnblockUser aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("acl/ajaxUnblockUser ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        }
    }

    function ajaxRestorePassAction()
    {
        try {
            $this->response->setJsonContent((new AclHandler())->restorePass()->toArray())->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("acl/ajaxRestorePass ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        } catch (Exception $e) {
            LoggerFactory::generic()->warning("acl/ajaxRestorePass aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        }
    }
}
