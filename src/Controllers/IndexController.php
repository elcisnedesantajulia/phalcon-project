<?php
declare(strict_types=1);


namespace App\Controllers;


use Rcc\Phalcon\Acl\Exception as AclException;
use Rcc\Phalcon\Acl\Profiles\Catalogs\Main;
use Rcc\Phalcon\Acl\RequestFactory;
use Rcc\Phalcon\Ajax\Exception as AjaxException;
use Rcc\Phalcon\Ajax\Response\Response;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Phalcon\Hexcodes\Hexcode;
use Rcc\Phalcon\Utils\LoggerFactory;

class IndexController extends ControllerBase
{
    function indexAction()
    {
        //var_dump($_GET); exit;
    }

    function testAction()
    {
        var_dump(Main::readFromDb(Main::FILTER_NOT_ROOT_NOT_ANONYMOUS)->count()); exit;
    }

    function ajaxLogoutAction()
    {
        try {
            $this->auth->identifyAsGuest();
            $this->response->setJsonContent((new Response())->toArray())->send();
        } catch (AclException $e) {
            LoggerFactory::generic()->warning("index/ajaxLogout aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        } catch (CacheException $e) {
            LoggerFactory::generic()->warning("index/ajaxLogout cacheException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        }
    }

    function ajaxChangePassAction()
    {
        try {
            $response = $this->user->changePasswordFromRequest(RequestFactory::changePassRequest()->populateFromHttp());
            $this->response->setJsonContent($response->toArray())->send();
        } catch (AjaxException $e) {
            LoggerFactory::generic()->warning("index/ajaxChangePass ajaxException {$e->getMessage()}");
            $this->response->setStatusCode(400, 'Bad request')->setJsonContent('Bad request')->send();
        } catch (AclException $e) {
            LoggerFactory::generic()->warning("index/ajaxChangePass aclException {$e->getMessage()}");
            $this->response->setStatusCode(500, 'Internal server error')->setJsonContent('Internal server error')->send();
        }
    }
}
