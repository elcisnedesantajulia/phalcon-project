<?php
declare(strict_types=1);


namespace App\Controllers;


use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;
use Rcc\Phalcon\Acl\Auth;
use Rcc\Phalcon\Acl\Users\Item as UserItem;
use Rcc\Phalcon\Gui\Menus\Main\Menu;
use Rcc\Phalcon\Utils\ActionInfo;
use Rcc\Views\Users\ModalsFactory;

/**
 * @property Menu menu
 * @property Auth auth
 */
abstract class ControllerBase extends Controller
{
    /** @var UserItem */
    protected $user;
    /** @var ActionInfo */
    protected $actionInfo;

    /**
     * @param Dispatcher $dispatcher
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpUnused
     */
    function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $this->user = $this->auth->requireUserItem();
        $this->actionInfo = ActionInfo::readFromDispatcher($dispatcher);
        if (!($this->user->isLoggedIn() || $this->actionInfo->isGuestController())) {
            $this->session->set('actionInfo', $this->actionInfo);
            $this->response->redirect('guest')->send();
        }
        if ($this->actionInfo->isAjax()) {
            $this->view->disable();
        } else {
            //preferentemente poner aqui asignaciones a variables dentro de view;
            $this->view->setVar('resource', $this->actionInfo->getResourceName());
            $this->view->setVar('user', $this->user);
            $this->view->setVar('changePasswordModal', ModalsFactory::changePassword());
            // $this->view->setVar('mainMenu', $this->menu); //TODO Decidir que hacer con Provider Menu
        }
    }
}
