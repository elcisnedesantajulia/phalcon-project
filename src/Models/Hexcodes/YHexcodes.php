<?php
declare(strict_types=1);
/**
 * @Filename: YHexcodes.php
 * @Description:
 * @CreatedAt: 02/05/20 22:28
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Models\Hexcodes;


use App\Models\ModelsBase;

class YHexcodes extends ModelsBase
{
    public $hex;
    public $purpose;
}
