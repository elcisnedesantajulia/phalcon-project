<?php
declare(strict_types=1);
/**
 * @Filename: ZProfiles.php
 * @Description:
 * @CreatedAt: 01/05/20 18:09
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Models\Acl;


/**
 * @property ZUsers[] users
 * @method static findFirstByKind(string $kind)
 */
class ZProfiles extends AclBase
{
    public $caption;
    public $kind;
    public $system_required;
    public $blocked;

    function initialize()
    {
        parent::initialize();

        $this->hasMany(
            'id',
            ZUsers::class,
            'profileId',
            [
                'alias' => 'users',
            ]
        );
    }
}
