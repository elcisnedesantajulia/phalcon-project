<?php
declare(strict_types=1);
/**
 * @Filename: AclBase.php
 * @Description:
 * @CreatedAt: 30/04/20 12:49
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Models\Acl;


use App\Models\ModelsBase;

abstract class AclBase extends ModelsBase
{

}
