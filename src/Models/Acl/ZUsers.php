<?php
declare(strict_types=1);
/**
 * @Filename: ZUsers.php
 * @Description:
 * @CreatedAt: 30/04/20 12:49
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace App\Models\Acl;


/**
 * @property ZProfiles profile
 * @method static findFirstByEmail(string $email)
 */
class ZUsers extends AclBase
{
    public $email;
    public $password;
    public $full_name;
    public $profileId;
    public $blocked;

    function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'profileId',
            ZProfiles::class,
            'id',
            [
                'alias' => 'profile',
            ]
        );
    }
}
