<?php
declare(strict_types=1);


namespace App\Models;


use Phalcon\Messages\MessageInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * @property int id
 * @property string mtime
 * @property string ctime
 * @method static findFirstById(int $id)
 */
abstract class ModelsBase extends Model
{
    function initialize()
    {
        $this->addBehavior(new Timestampable([
            'beforeCreate' => [
                'field' => 'ctime',
                'format' => 'Y-m-d H:i:s',
            ],
            'beforeUpdate' => [
                'field' => 'ctime',
                'format' => 'Y-m-d H:i:s',
            ],
        ]));
    }

    function getMessage(): string
    {
        /** @var string[] $messages */
        $messages = $this->getMessages();
        if (isset($messages[0])) {
            /** @var MessageInterface $message */
            $message = $messages[0];
            return $message->getMessage();
            /*if (is_string($message)) {
                return $message;
            }
            return get_class($message);*/
        }

        return '';
    }
}
