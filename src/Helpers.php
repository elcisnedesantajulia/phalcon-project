<?php
declare(strict_types=1);

namespace App;

use Phalcon\Di;

function container()
{
    $default = Di::getDefault();
    $args = func_get_args();
    if (empty($args)) {
        return $default;
    }

    return call_user_func_array([$default, 'get'], $args);
}

function root_path(string $suffix = ''): string
{
    /** @var Application $application */
    $application = container(Application::APPLICATION_PROVIDER);

    return join(DIRECTORY_SEPARATOR, [$application->getRootPath(), ltrim($suffix, DIRECTORY_SEPARATOR)]);
}
