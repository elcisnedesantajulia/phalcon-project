<?php
declare(strict_types=1);
/**
 * @Filename: Tr.php
 * @Description:
 * @CreatedAt: 25/04/20 9:32
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Html\Tags;


class Tr extends Ul
{
    protected $elementType = 'tr';

    function appendBasicTd(string $plainText = ''): Tr
    {
        $this->append((new Td())->appendText($plainText));

        return $this;
    }

    function appendBasicTh(string $plainText = ''): Tr
    {
        $this->append((new Th())->appendText($plainText));

        return $this;
    }
}
