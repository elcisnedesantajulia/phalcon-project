<?php


namespace Rcc\Html;


interface Element extends Dom
{
    function append(Dom $dom): Element;

    function pushOneClass(string $class): Element;

    function show(): Element;

    function hide(): Element;
}
