<?php
declare(strict_types=1);
/**
 * @Filename: Span.php
 * @Description:
 * @CreatedAt: 28/04/20 19:05
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Common\Money;


use Rcc\Datatypes\Money\Money;
use Rcc\Html\Tags\Span as TagsSpan;

class Span extends TagsSpan
{

    /**
     * Span constructor.
     * @param Money $money
     * @param array $classes
     */
    function __construct(Money $money, array $classes = [])
    {
        parent::__construct();
        if (!empty($classes)) {
            $this->pushClasses($classes);
        }
        if (!$money->isPositive()) {
            $this->pushOneClass('text-danger');
        }
        $this->appendText($money->getString('$ '));
    }
}
