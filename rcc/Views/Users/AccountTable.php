<?php
declare(strict_types=1);
/**
 * @Filename: AccountTable.php
 * @Description:
 * @CreatedAt: 07/05/20 12:26
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Users;


use Rcc\Html\Tags\Table;
use Rcc\Html\Tags\Tr;
use Rcc\Phalcon\Acl\Users\Item as UsersItem;

class AccountTable extends Table
{

    function __construct(UsersItem $userItem)
    {
        parent::__construct();
        $this->pushClasses(['table-sm', 'table-hover', 'table-borderless'])
            ->append((new Tr())->appendBasicTh('Email')->appendBasicTd($userItem->getEmail()))
            ->append((new Tr())->appendBasicTh('Perfil')->appendBasicTd($userItem->getProfileCaption()))
            ->append((new Tr())->appendBasicTh('Fecha de registro')->appendBasicTd($userItem->getCreatedDate()->generateYmd()));
    }
}
