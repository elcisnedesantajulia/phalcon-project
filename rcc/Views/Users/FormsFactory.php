<?php
declare(strict_types=1);
/**
 * @Filename: FormsFactory.php
 * @Description:
 * @CreatedAt: 06/05/20 22:06
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Users;


use Rcc\Html\Complex\Form\FieldsetPassword;
use Rcc\Html\Complex\Form\FieldsetSelect\Select as FieldsetSelect;
use Rcc\Html\Complex\Form\FieldsetText;
use Rcc\Html\Complex\Form\FormRow;
use Rcc\Html\Dom;
use Rcc\Html\Tags\Form;
use Rcc\Phalcon\Acl\Exception as AclException;
use Rcc\Phalcon\Acl\Profiles\Catalogs\Main;
use Rcc\Phalcon\Cache\Exception as CacheException;

abstract class FormsFactory
{

    /**
     * @return Dom
     * @throws AclException
     * @throws CacheException
     */
    static function newUser(): Dom
    {
        $profileSelect = new FieldsetSelect('nu-profile', ['col-12'], 'Perfil',
            '...Elige un perfil...', 'Campo requerido');
        foreach (Main::readFromDb(Main::FILTER_NOT_ANONYMOUS)->getItems() as $profileItem) { //TODO manejar permisos en esta función para determinar si se muestran todos los perfiles o solo los que no son de root
            $profileSelect->pushOption("{$profileItem->getId()}", $profileItem->getCaption());
        }

        return (new Form('new-user-form'))
            ->append((new FormRow('row1'))
                ->append(new FieldsetText('nu-name', ['col-12', 'col-lg-6'], 'Nombre',
                    'Campo requerido', '* Nombre'))
                ->append(new FieldsetText('nu-email', ['col-12', 'col-lg-6'], 'Email',
                    'Debes introducir un email válido', '* Email'))
                ->append($profileSelect)
                ->append(new FieldsetPassword('nu-pass', ['col-12', 'col-lg-6'], 'Password',
                    'El password debe tener al menos 6 caracteres', '* Password'))
                ->append(new FieldsetPassword('nu-confirm', ['col-12', 'col-lg-6'], 'Confirmación',
                    'La confirmación no coincide', 'Confirma password'))
            );
    }

    /**
     * @return Dom
     * @throws AclException
     * @throws CacheException
     */
    static function editUser(): Dom
    {
        $profileSelect = new FieldsetSelect('eu-profile', ['col-12'], 'Perfil');
        foreach (Main::readFromDb(Main::FILTER_NOT_ANONYMOUS)->getItems() as $profileItem) { //TODO manejar permisos en esta función para determinar si se muestran todos los perfiles o solo los que no son de root
            $profileSelect->pushOption("{$profileItem->getId()}", $profileItem->getCaption());
        }

        return (new Form('edit-user-form'))
            ->append((new FormRow('row1'))
                ->append(new FieldsetText('eu-name', ['col-12'], 'Nombre',
                    'Campo requerido', '* Nombre'))
                ->append(new FieldsetText('eu-email', ['col-12'], 'Email',
                    'Debes introducir un email válido', '* Email', true))
                ->append($profileSelect)
            );
    }

    static function restorePassword(): Dom
    {
        return (new Form('restore-password-form'))
            ->append((new FormRow('row1'))
                ->append(new FieldsetText('rp-name', ['col-12'], 'Nombre',
                    'Campo requerido', '* Nombre', true))
                ->append(new FieldsetPassword('rp-pass', ['col-12'], 'Password nuevo',
                    'El password debe tener al menos 6 caracteres', '* Password nuevo'))
                ->append(new FieldsetPassword('rp-confirm', ['col-12'], 'Confirmación',
                    'La confirmación no coincide', 'Confirma password'))
            );
    }

    static function changePassword(): Dom
    {
        return (new Form('change-password-form'))
            ->append((new FormRow('row1'))
                ->append(new FieldsetPassword('chp-old', ['col-12'], 'Password actual',
                    'El password debe tener al menos 6 caracteres', '* Password actual'))
                ->append(new FieldsetPassword('chp-pass', ['col-12'], 'Password nuevo',
                    'El password debe tener al menos 6 caracteres', '* Password nuevo'))
                ->append(new FieldsetPassword('chp-confirm', ['col-12'], 'Confirmación',
                    'La confirmación no coincide', '* Confirma password'))
            );
    }
}
