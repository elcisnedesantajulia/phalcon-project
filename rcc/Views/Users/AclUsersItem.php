<?php
declare(strict_types=1);
/**
 * @Filename: AclUsersItem.php
 * @Description:
 * @CreatedAt: 05/05/20 14:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Users;


use Rcc\Html\Complex\Card\HeaderMenu\ButtonItem;
use Rcc\Html\Tags\Td;
use Rcc\Html\Tags\Tr;
use Rcc\Phalcon\Acl\Users\Item as UsersItem;

class AclUsersItem extends Tr
{

    function __construct(UsersItem $usersItem)
    {
        $acciones = (new Td())->pushOneClass('text-right')
            ->append((new ButtonItem('pencil', 'primary', 'Editar usuario'))
                ->pushCustomData('toggle', 'modal')
                ->pushCustomData('target', '#eu-modal')
                ->pushCustomData('id', "{$usersItem->getId()}")
                ->pushCustomData('name', "{$usersItem->getFullName()}")
                ->pushCustomData('email', "{$usersItem->getEmail()}")
                ->pushCustomData('profile', "{$usersItem->getProfileId()}")
            )
            ->append((new ButtonItem('key', 'info', 'Restaurar password'))
                ->pushCustomData('toggle', 'modal')
                ->pushCustomData('target', '#rp-modal')
                ->pushCustomData('id', "{$usersItem->getId()}")
                ->pushCustomData('name', "{$usersItem->getFullName()}")
            );

        if ($usersItem->isBlocked(true)) {
            $acciones->append((new ButtonItem('circle-check', 'success', 'Desbloquear usuario'))
                ->pushCustomData('toggle', 'modal')
                ->pushCustomData('target', '#ubu-modal')
                ->pushCustomData('id', "{$usersItem->getId()}")
                ->pushCustomData('name', "{$usersItem->getFullName()}")
            );
        } else {
            $acciones->append((new ButtonItem('ban', 'danger', 'Bloquear usuario'))
                ->pushCustomData('toggle', 'modal')
                ->pushCustomData('target', '#bu-modal')
                ->pushCustomData('id', "{$usersItem->getId()}")
                ->pushCustomData('name', "{$usersItem->getFullName()}")
            );
        }

        parent::__construct();
        $this->appendBasicTd($usersItem->getFullName())
            ->appendBasicTd($usersItem->getEmail())
            ->appendBasicTd($usersItem->getProfileCaption())
            ->append($acciones);
    }
}
