<?php
declare(strict_types=1);
/**
 * @Filename: ModalsFactory.php
 * @Description:
 * @CreatedAt: 07/05/20 11:49
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Users;


use Rcc\Html\Complex\Modal\Footer;
use Rcc\Html\Complex\Modal\Modal;
use Rcc\Html\PlainText;
use Rcc\Phalcon\Acl\Exception as AclException;
use Rcc\Phalcon\Acl\Users\Item as UsersItem;
use Rcc\Phalcon\Cache\Exception as CacheException;

abstract class ModalsFactory
{

    /**
     * @return Modal
     * @throws AclException
     * @throws CacheException
     */
    static function newUser(): Modal
    {
        return new Modal(
            'nu-modal', // NU === New User
            Modal::SIZE_LARGE,
            new PlainText('Crear nuevo usuario'),
            FormsFactory::newUser()
        );
    }

    /**
     * @return Modal
     * @throws AclException
     * @throws CacheException
     */
    static function editUser(): Modal
    {
        return new Modal(
            'eu-modal', // EU === Edit User
            Modal::SIZE_SMALL,
            new PlainText('Editar usuario'),
            FormsFactory::editUser()
        );
    }

    static function restorePassword(): Modal
    {
        return new Modal(
            'rp-modal', // RP === Restore Password
            Modal::SIZE_SMALL,
            new PlainText('Restaurar password'),
            FormsFactory::restorePassword()
        );
    }

    static function changePassword(): Modal
    {
        return new Modal(
            'chp-modal', // CHP === CHange Password
            Modal::SIZE_SMALL,
            new PlainText('Cambiar mi password'),
            FormsFactory::changePassword()
        );
    }

    static function blockUser(): Modal
    {
        return new Modal(
            'bu-modal', // BU === Block User
            Modal::SIZE_SMALL,
            new PlainText('Bloquear Usuario'),
            new PlainText('Confirma que deseas bloquear al usuario')
        );
    }

    static function unblockUser(): Modal
    {
        return new Modal(
            'ubu-modal', // UBU === UnBlock User
            Modal::SIZE_SMALL,
            new PlainText('Desbloquear Usuario'),
            new PlainText('Confirma que deseas desbloquear al usuario')
        );
    }

    static function userAccount(UsersItem $userItem): Modal
    {
        /** @noinspection PhpParamsInspection */
        return (new Modal(
            'ua-modal', // UA === User Account
            Modal::SIZE_LARGE,
            new PlainText($userItem->getFullName()),
            new AccountTable($userItem)
        ))->setFooter(Footer::onlyBack());
    }
}
