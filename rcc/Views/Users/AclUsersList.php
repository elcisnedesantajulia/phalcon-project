<?php
declare(strict_types=1);
/**
 * @Filename: AclUsersList.php
 * @Description:
 * @CreatedAt: 05/05/20 14:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Views\Users;


use Rcc\Html\Tags\Div;
use Rcc\Html\Tags\Table;
use Rcc\Html\Tags\Tr;
use Rcc\Phalcon\Acl\Users\Catalog;

class AclUsersList extends Div
{

    function __construct(Catalog $catalog)
    {
        parent::__construct('acl-users-list');

        $table = (new Table())->pushClasses(['table-sm', 'table-hover'])
            ->appendTheadRow((new Tr())->pushOneClass('text-center')
                ->appendBasicTh('Nombre')
                ->appendBasicTh('Email')
                ->appendBasicTh('Perfil')
                ->appendBasicTh());

        foreach ($catalog->getItems() as $userItem) {
            $table->appendTbodyRow(new AclUsersItem($userItem));
        }

        $this->append($table);
    }
}
