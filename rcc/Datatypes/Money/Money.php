<?php
declare(strict_types=1);
/**
 * @Filename: Money.php
 * @Description: This class allows to store and manage money
 * @CreatedAt: 23/04/20 22:00
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Money;


class Money
{
    const FACTOR_IVA = 1.16;
    const FLAG_SOLO_IMPORTE = 1;
    const FLAG_SOLO_IVA = 2;
    const FLAG_IVA_INCLUIDO = 3;

    /** @var int En centavos sin IVA */
    private $centavos;

    private function __construct(int $centavos)
    {
        $this->centavos = $centavos;
    }

    function getCentavos(float $factor = 1.0, int $flag = self::FLAG_SOLO_IMPORTE): int
    {
        $importeCentavos = $this->centavos * $factor;

        if ($flag == self::FLAG_SOLO_IVA) {
            return (int) round($importeCentavos * (self::FACTOR_IVA - 1));
        }

        if ($flag == self::FLAG_IVA_INCLUIDO) {
            return (int) round($importeCentavos * self::FACTOR_IVA);
        }

        return (int) round($importeCentavos);
    }

    function getFloat(float $factor = 1.0, int $flag = self::FLAG_SOLO_IMPORTE): float
    {
        return $this->getCentavos($factor, $flag) / 100;
    }

    function getString(string $prefix = '', float $factor = 1.0, int $flag = self::FLAG_SOLO_IMPORTE): string
    {
        return $prefix . number_format($this->getFloat($factor, $flag), 2);
    }

    function incrementCentavos(int $centavos): Money
    {
        $this->centavos += $centavos;

        return $this;
    }

    function isPositive(): bool
    {
        return $this->centavos >= 0;
    }

    static function fromCentavos(int $centavos): Money
    {
        return new self($centavos);
    }

    static function fromFloat(float $monto): Money
    {
        return self::fromCentavos((int) round($monto * 100));
    }

    static function fromString(string $monto): Money
    {
        return self::fromFloat((float) $monto);
    }

    static function zero(): Money
    {
        return self::fromCentavos(0);
    }
}
