<?php
declare(strict_types=1);
/**
 * @Filename: Hour.php
 * @Description:
 * @CreatedAt: 24/04/20 16:57
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Time\Ids;


use Rcc\Datatypes\Time\DateTime;

class Hour
{
    /** @var int */
    private $id;

    /**
     * Hour constructor.
     * @param int $id
     */
    private function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    static function fromMin(Min $min): Hour
    {
        return new self(($min->getId() - ($min->getId() % DateTime::MINS_IN_AN_HOUR)) / DateTime::MINS_IN_AN_HOUR);
    }

    static function fromDatetime(DateTime $datetime): Hour
    {
        return self::fromMin(Min::fromDatetime($datetime));
    }
}
