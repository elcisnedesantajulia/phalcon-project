<?php
declare(strict_types=1);
/**
 * @Filename: Sec.php
 * @Description:
 * @CreatedAt: 24/04/20 14:22
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Time\Ids;


use Rcc\Datatypes\Time\DateTime;

class Sec
{
    /** @var int */
    private $id;

    private function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    static function fromDatetime(DateTime $datetime): Sec
    {
        return new self($datetime->getTimestamp() + $datetime->getOffset());
    }
}
