<?php
declare(strict_types=1);
/**
 * @Filename: Min.php
 * @Description:
 * @CreatedAt: 24/04/20 16:17
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Time\Ids;


use Rcc\Datatypes\Time\DateTime;

class Min
{
    /** @var int */
    private $id;

    /**
     * Min constructor.
     * @param int $id
     */
    private function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    static function fromSec(Sec $sec): Min
    {
        return new self(($sec->getId() - ($sec->getId() % DateTime::SECS_IN_A_MIN)) / DateTime::SECS_IN_A_MIN);
    }

    static function fromDatetime(DateTime $datetime): Min
    {
        return self::fromSec(Sec::fromDatetime($datetime));
    }
}
