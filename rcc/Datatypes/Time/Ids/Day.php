<?php
declare(strict_types=1);
/**
 * @Filename: Day.php
 * @Description:
 * @CreatedAt: 24/04/20 17:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Time\Ids;


use Rcc\Datatypes\Time\DateTime;

class Day
{
    /** @var int */
    private $id;

    /**
     * Day constructor.
     * @param int $id
     */
    private function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    function generateYmd(): string
    {
        $epoch = $this->id * DateTime::secondsInADay();

        return (new DateTime("@{$epoch}"))->generateYmd();
    }

    static function fromHour(Hour $hour): Day
    {
        return new self(($hour->getId() - ($hour->getId() % DateTime::HOURS_IN_A_DAY)) / DateTime::HOURS_IN_A_DAY);
    }

    static function fromDatetime(DateTime $datetime): Day
    {
        return self::fromHour(Hour::fromDatetime($datetime));
    }

    static function now(): Day
    {
        return self::fromDatetime(new DateTime());
    }
}
