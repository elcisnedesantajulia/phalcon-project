<?php
declare(strict_types=1);
/**
 * @Filename: DateTime.php
 * @Description:
 * @CreatedAt: 24/04/20 14:26
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Time;


use DateTimeZone;
use Exception;

class DateTime extends \DateTime
{
    const SECS_IN_A_MIN = 60;
    const MINS_IN_AN_HOUR = 60;
    const HOURS_IN_A_DAY = 24;
    const DAYS_IN_A_WEEK = 7;
    const DAYS_IN_A_MONTH = 30;
    const DAYS_IN_A_YEAR = 365;

    /**
     * DateTime constructor.
     * @param string $time
     * @param DateTimeZone|null $timezone
     */
    function __construct($time = 'now', DateTimeZone $timezone = null)
    {
        try {
            parent::__construct($time, $timezone);
        } catch (Exception $e) {
            try {
                parent::__construct('now');
            } catch (Exception $e) {
            }
        }
    }

    function generateTimeAgo(string $prefix = ''): string
    {
        $lastedTime = $this->getTimestamp() - time();
        if ($lastedTime <= 0) {
            return $this->addPrefix('justo ahora', $prefix);
        }

        $units = [
            ['singular' => 'año', 'plural' => 'años', 'secs' => self::secondsInAYear()],
            ['singular' => 'mes', 'plural' => 'meses', 'secs' => self::secondsInAMonth()],
            ['singular' => 'semana', 'plural' => 'semanas', 'secs' => self::secondsInAWeek()],
            ['singular' => 'día', 'plural' => 'dias', 'secs' => self::secondsInADay()],
            ['singular' => 'hora', 'plural' => 'horas', 'secs' => self::secondsInAnHour()],
            ['singular' => 'minuto', 'plural' => 'minutos', 'secs' => self::SECS_IN_A_MIN],
            ['singular' => 'segundo', 'plural' => 'segundos', 'secs' => 1],
        ];
        foreach ($units as $unit) {
            if ($lastedTime >= $unit) {
                $amount = floor($lastedTime / $unit['secs']);
                $caption = $amount > 1 ? $unit['plural']: $unit['singular'];
                return $this->addPrefix("hace {$amount} {$caption}", $prefix);
            }
        }

        return 'Rcc\DateTime::generateTimeAgo error';
    }

    function generateMysqlTimestamp(): string
    {
        return $this->format('Y-m-d H:i:s');
    }

    function generateYmd(): string
    {
        return $this->format('Y-m-d');
    }

    static function secondsInAnHour(): int
    {
        return self::SECS_IN_A_MIN * self::MINS_IN_AN_HOUR;
    }

    static function secondsInADay(): int
    {
        return self::secondsInAnHour() * self::HOURS_IN_A_DAY;
    }

    static function secondsInAWeek(): int
    {
        return self::secondsInADay() * self::DAYS_IN_A_WEEK;
    }

    static function secondsInAMonth(): int
    {
        return self::secondsInADay() * self::DAYS_IN_A_MONTH;
    }

    static function secondsInAYear(): int
    {
        return self::secondsInADay() * self::DAYS_IN_A_YEAR;
    }

    private function addPrefix(string $output, string $prefix = ''): string
    {
        return ucfirst("{$prefix}{$output}");
    }
}
