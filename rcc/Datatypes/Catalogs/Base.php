<?php
declare(strict_types=1);
/**
 * @Filename: Base.php
 * @Description:
 * @CreatedAt: 23/04/20 13:04
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Catalogs;


abstract class Base
{
    protected $items = [];

    function getItems(): array
    {
        return $this->items;
    }

    function count(): int
    {
        return count($this->items);
    }

    function isEmpty(): bool
    {
        return $this->count() == 0;
    }
}
