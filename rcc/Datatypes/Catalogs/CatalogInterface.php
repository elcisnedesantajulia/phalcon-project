<?php
/**
 * @Filename: CatalogInterface.php
 * @Description:
 * @CreatedAt: 23/04/20 13:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Datatypes\Catalogs;


interface CatalogInterface
{
    function getItems(): array;

    function count(): int;

    function isEmpty(): bool;
}
