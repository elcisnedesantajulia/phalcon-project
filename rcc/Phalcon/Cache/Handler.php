<?php
declare(strict_types=1);
/**
 * @Filename: Handler.php
 * @Description:
 * @CreatedAt: 27/04/20 15:45
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Cache;


use Phalcon\Cache;
use Phalcon\Di\Exception as DiException;
use Phalcon\Di\Injectable;

class Handler extends Injectable
{
    /** @var string */
    private $diServiceName;
    /** @var Cache */
    private $diService;
    /** @var string */
    private $key;

    /**
     * Handler constructor.
     * @param string $diServiceName
     * @param string $key
     * @throws Exception
     */
    function __construct(string $diServiceName, string $key)
    {
        try {
            $this->diService = $this->di->get($diServiceName);
        } /** @noinspection PhpRedundantCatchClauseInspection */ catch (DiException $e) {
            throw new Exception("CacheHandler::__construct diServiceName {$diServiceName} doesnt exist");
        }
        $this->diServiceName = $diServiceName;
        $this->key = $key;
    }

    function has():bool
    {
        try {
            return $this->diService->has($this->key);
        } catch (Cache\Exception\InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function get()
    {
        if ($this->has()) {
            try {
                return $this->diService->get($this->key);
            } catch (Cache\Exception\InvalidArgumentException $e) {
                throw new Exception("CacheHandler::get() diServiceName {$this->diServiceName} key {$this->key} CacheInvalidArgumentException {$e->getMessage()}");
            }
        }

        throw new Exception("CacheHandler::get() diServiceName {$this->diServiceName} unable to get cache for key {$this->key}");
    }

    /**
     * @param $payload
     * @throws Exception
     */
    function save($payload)
    {
        try {
            $succeeded = $this->diService->set($this->key, $payload);
            if (!$succeeded) {
                throw new Exception("CacheHandler::save() diServiceName {$this->diServiceName} unable to save cache for key {$this->key}");
            }
        } catch (Cache\Exception\InvalidArgumentException $e) {
            throw new Exception("CacheHandler::save() diServiceName {$this->diServiceName} key {$this->key} CacheInvalidArgumentException {$e->getMessage()}");
        }
    }
}
