<?php
declare(strict_types=1);
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 27/04/20 15:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Cache;


class Exception extends \Exception
{

}
