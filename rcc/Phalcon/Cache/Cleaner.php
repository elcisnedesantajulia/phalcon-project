<?php
declare(strict_types=1);
/**
 * @Filename: Cleaner.php
 * @Description:
 * @CreatedAt: 28/04/20 13:09
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Cache;


use App\Providers\CacheProvider;
use Phalcon\Cache;
use Phalcon\Di\Injectable;

class Cleaner extends Injectable
{
    /** @var string[] */
    private $services;

    private function __construct()
    {
        $this->services = (new CacheProvider())->generateServicesList();
    }

    static function clean(string $diServiceName)
    {
        $cleaner = new self();
        if (in_array($diServiceName, $cleaner->services)) {
            $cleaner->clearCacheService($diServiceName);
        }
    }

    static function cleanAll()
    {
        $cleaner = new self();
        foreach ($cleaner->services as $service) {
            $cleaner->clearCacheService($service);
        }
    }

    private function clearCacheService(string $diServiceName)
    {
        /** @var Cache $cacheService */
        $cacheService = $this->di->getShared($diServiceName);
        $cacheService->clear();
    }
}
