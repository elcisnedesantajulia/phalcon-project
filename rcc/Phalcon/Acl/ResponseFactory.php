<?php
declare(strict_types=1);
/**
 * @Filename: ResponseFactory.php
 * @Description:
 * @CreatedAt: 02/05/20 20:03
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl;


use Phalcon\Di;
use Phalcon\Security;
use Rcc\Phalcon\Ajax\Response\Response;

abstract class ResponseFactory
{
    static function incorrectEmailPassword(): Response
    {
        /** @var Security $security */
        $security = Di::getDefault()->get('security');

        return new Response(
            false,
            'Email y/o Password incorrectos',
            [
                'key' => $security->getTokenKey(),
                'token' => $security->getToken(),
            ]
        );
    }
}
