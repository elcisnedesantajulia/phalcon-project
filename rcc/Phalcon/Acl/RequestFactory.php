<?php
declare(strict_types=1);
/**
 * @Filename: RequestFactory.php
 * @Description:
 * @CreatedAt: 01/05/20 17:12
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl;


use Rcc\Phalcon\Ajax\Request\Fields\IntField;
use Rcc\Phalcon\Ajax\Request\Fields\StringField;
use Rcc\Phalcon\Ajax\Request\Request;
use Rcc\Phalcon\Ajax\Request\Validators\Positive;
use Rcc\Phalcon\Ajax\Request\Validators\Regex;

abstract class RequestFactory
{
    static function loginRequest(): Request
    {
        return (new Request())
            ->pushField(
                (new StringField('email', true))
                    ->pushPhalconFilter('email')
                    ->pushValidator(new Regex(Regex::PATTERN_EMAIL))
            )
            ->pushField((new StringField('pass'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)))
            ->pushField((new StringField('key')))
            ->pushField((new StringField('token')));
    }

    static function registerRequest(): Request
    {
        return (new Request())
            ->pushField((new StringField('name'))->pushValidator(new Regex(Regex::PATTERN_AT_LEAST_TWO_CHARS)))
            ->pushField(
                (new StringField('email', true))
                    ->pushPhalconFilter('email')
                    ->pushValidator(new Regex(Regex::PATTERN_EMAIL))
            )
            ->pushField((new StringField('pass'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)))
            ->pushField((new StringField('key')))
            ->pushField((new StringField('token')));
    }

    static function newUserRequest(): Request
    {
        return (new Request())
            ->pushField((new StringField('name'))->pushValidator(new Regex(Regex::PATTERN_AT_LEAST_TWO_CHARS)))
            ->pushField(
                (new StringField('email', true))
                    ->pushPhalconFilter('email')
                    ->pushValidator(new Regex(Regex::PATTERN_EMAIL))
            )
            ->pushField((new IntField('profile'))->pushValidator(new Positive()))
            ->pushField((new StringField('pass'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)));
    }

    static function editUserRequest(): Request
    {
        return (new Request())
            ->pushField((new IntField('id'))->pushValidator(new Positive()))
            ->pushField((new StringField('name'))->pushValidator(new Regex(Regex::PATTERN_AT_LEAST_TWO_CHARS)))
            ->pushField((new IntField('profile'))->pushValidator(new Positive()));
    }

    static function changePassRequest(): Request
    {
        return (new Request())
            ->pushField((new StringField('old'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)))
            ->pushField((new StringField('pass'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)));
    }

    static function restorePassRequest(): Request
    {
        return (new Request())
            ->pushField((new IntField('id'))->pushValidator(new Positive()))
            ->pushField((new StringField('pass'))->pushValidator(new Regex(Regex::PATTERN_PASSWORD)));
    }

    static function blockUserRequest(): Request
    {
        return (new Request())
            ->pushField((new IntField('id'))->pushValidator(new Positive()));
    }

    static function unblockUserRequest(): Request
    {
        return self::blockUserRequest();
    }
}
