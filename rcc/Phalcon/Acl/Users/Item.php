<?php
declare(strict_types=1);
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 01/05/20 18:06
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Users;


use App\Models\Acl\ZUsers;
use Phalcon\Di;
use Phalcon\Security;
use Rcc\Datatypes\Time\DateTime;
use Rcc\Datatypes\Time\Ids\Day;
use Rcc\Html\Complex\Modal\Modal;
use Rcc\Phalcon\Acl\Exception;
use Rcc\Phalcon\Acl\Profiles\Item as ProfilesItem;
use Rcc\Phalcon\Ajax\Exception as AjaxException;
use Rcc\Phalcon\Ajax\Request\Request;
use Rcc\Phalcon\Ajax\Response\Response;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Views\Users\ModalsFactory;

class Item
{
    /** @var int */
    private $id;
    /** @var Day */
    private $createdDate;
    /** @var string */
    private $email;
    /** @var string */
    private $password;
    /** @var string */
    private $fullName;
    /** @var int */
    private $profileId;
    /** @var ProfilesItem */
    private $profileItem;
    /** @var boolean */
    private $blocked;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Day
     */
    function getCreatedDate(): Day
    {
        return $this->createdDate;
    }

    /**
     * @return string
     */
    function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @return int
     */
    function getProfileId(): int
    {
        return $this->profileId;
    }

    function getProfileCaption(): string
    {
        return $this->profileItem->getCaption();
    }

    function isLoggedIn(): bool
    {
        return !$this->profileItem->isAnonymous();
    }

    /**
     * @param bool $onlyUserLevel
     * @return bool
     */
    function isBlocked(bool $onlyUserLevel = false): bool
    {
        if ($onlyUserLevel) {
            return $this->blocked;
        }
        return $this->blocked || $this->profileItem->isBlocked();
    }

    /**
     * This function is used in index layout
     * @return Modal
     * @noinspection PhpUnused
     */
    function generateUserAccountModal(): Modal
    {
        return ModalsFactory::userAccount($this);
    }

    function validatePassword(string $hash): bool
    {
        return self::requireSecurityService()->checkHash($hash, $this->password);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws AjaxException
     * @throws Exception
     */
    function changePasswordFromRequest(Request $request): Response
    {
        $old = $request->getString('old');
        $pass = $request->getString('pass');
        if ($old == $pass) {
            return new Response(false, 'El nuevo password debe ser diferente del password actual');
        }
        if (!$this->validatePassword($old)) {
            return new Response(false, 'El password actual es incorrecto');
        }

        /*$userItem =*/ $this->changePassword($pass);

        return new Response();
    }

    /**
     * @param string $password
     * @return $this
     * @throws Exception
     */
    function changePassword(string $password): Item
    {
        $model = $this->requireModel();
        $model->password = self::requireSecurityService()->hash($password);
        if (!$model->save()) {
            throw new Exception("UserItem::changePassword() id: {$this->id} no se puede guardar en db -> {$model->getMessage()}");
        }

        return $this;
    }

    /**
     * @param Request $request
     * @return $this
     * @throws Exception
     * @throws AjaxException
     */
    function editFromRequest(Request $request): Item
    {
        $model = $this->requireModel();
        $model->full_name = $request->getString('name');
        $model->profileId = $request->getInt('profile');
        if (!$model->save()) {
            throw new Exception("UserItem::block() id: {$this->id} no se puede guardar en db -> {$model->getMessage()}");
        }

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    function block(): Item
    {
        $model = $this->requireModel();
        $model->blocked = '1';
        if (!$model->save()) {
            throw new Exception("UserItem::block() id: {$this->id} no se puede guardar en db -> {$model->getMessage()}");
        }

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    function unblock(): Item
    {
        $model = $this->requireModel();
        $model->blocked = '0';
        if (!$model->save()) {
            throw new Exception("UserItem::unblock() id: {$this->id} no se puede guardar en db -> {$model->getMessage()}");
        }

        return $this;
    }

    static function readFromModel(ZUsers $model): Item
    {
        $item = new self();
        $item->id = (int) $model->id;
        $item->createdDate = Day::fromDatetime(new DateTime($model->ctime));
        $item->email = $model->email;
        $item->password = $model->password;
        $item->fullName = $model->full_name;
        $item->profileId = (int) $model->profileId;
        $item->profileItem = ProfilesItem::readFromModel($model->profile);
        $item->blocked = (bool) $model->blocked;

        return $item;
    }

    /**
     * @param int $id
     * @return Item
     * @throws Exception
     */
    static function readFromDb(int $id): Item
    {
        $model = ZUsers::findFirstById($id);
        if (!$model) {
            throw new Exception("UsersItem::readFromDb() id {$id} no existe el model");
        }

        return self::readFromModel($model);
    }

    static function exists(string $email): bool
    {
        try {
            /*$item =*/ self::findByEmail($email);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param string $email
     * @return Item
     * @throws Exception
     */
    static function findByEmail(string $email): Item
    {
        $model = ZUsers::findFirstByEmail($email);
        if (!$model) {
            throw new Exception("UserItem::findByEmail() email {$email} no existe model");
        }

        return self::readFromModel($model);
    }

    /**
     * @return Item
     * @throws Exception
     * @throws CacheException
     */
    static function generateAnonymous(): Item
    {
        $item = new self();
        $item->id = 0;
        $item->createdDate = Day::now();
        $item->email = 'anony@mo.us';
        $item->password = 'top secret';
        $item->fullName = 'Anonymous Guest';
        $item->profileItem = ProfilesItem::readAnonymous();
        $item->profileId = $item->profileItem->getId();
        $item->blocked = false;

        return $item;
    }

    /**
     * @param Request $request
     * @return Item
     * @throws CacheException
     * @throws Exception
     * @throws AjaxException
     */
    static function createFromRegisterRequest(Request $request): Item
    {
        $model = new ZUsers([
            'email' => $request->getString('email'),
            'password' => self::requireSecurityService()->hash($request->getString('pass')),
            'full_name' => $request->getString('name'),
            'profileId' => ProfilesItem::readKindNew()->getId(),
        ]);

        if (!$model->save()) {
            throw new Exception("UserItem::createFromRegisterRequest() no se pudo guardar en DB: {$model->getMessage()}");
        }

        return self::readFromDb((int) $model->id);
    }

    /**
     * @param Request $request
     * @return Item
     * @throws AjaxException
     * @throws CacheException
     * @throws Exception
     */
    static function createFromNewUserRequest(Request $request): Item
    {
        $profileId = $request->getInt('profile');
        // Si no existe se produce una excepción
        /*$profileItem =*/ ProfilesItem::readFromCache($profileId);

        $model = new ZUsers([
            'email' => $request->getString('email'),
            'password' => self::requireSecurityService()->hash($request->getString('pass')),
            'full_name' => $request->getString('name'),
            'profileId' => $profileId,
        ]);

        if (!$model->save()) {
            throw new Exception("UserItem::createFromNewUserRequest() no se pudo guardar en DB: {$model->getMessage()}");
        }

        return self::readFromDb((int) $model->id);
    }

    static private function requireSecurityService(): Security
    {
        return Di::getDefault()->get('security');
    }

    /**
     * @return ZUsers
     * @throws Exception
     */
    private function requireModel(): ZUsers
    {
        $model = ZUsers::findFirstById($this->id);
        if (!$model) {
            throw new Exception("UserItem::requireModel() id: {$this->id}, no se puede obtener el model");
        }

        return $model;
    }
}
