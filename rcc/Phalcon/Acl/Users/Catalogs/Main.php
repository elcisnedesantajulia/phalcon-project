<?php
declare(strict_types=1);
/**
 * @Filename: Main.php
 * @Description:
 * @CreatedAt: 05/05/20 13:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Users\Catalogs;


use App\Models\Acl\ZUsers;
use Phalcon\Mvc\Model\Resultset\Simple;
use Rcc\Datatypes\Catalogs\Base;
use Rcc\Phalcon\Acl\Users\Catalog;
use Rcc\Phalcon\Acl\Users\Item;

class Main extends Base implements Catalog
{
    /** @var Item[] */
    protected $items = [];

    private function __construct()
    {
    }

    static function readFromDb(): Catalog
    {
        $catalog = new self();

        /** @var Simple $resultset */
        $resultset = ZUsers::find();
        if (count($resultset) == 0) {
            return $catalog;
        }

        foreach ($resultset as $model) {
            $catalog->items[] = Item::readFromModel($model);
        }

        return $catalog;
    }
}
