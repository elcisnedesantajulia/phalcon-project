<?php
/**
 * @Filename: Catalog.php
 * @Description:
 * @CreatedAt: 05/05/20 13:54
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Users;


use Rcc\Datatypes\Catalogs\CatalogInterface;

interface Catalog extends CatalogInterface
{
    /**
     * @return Item[]
     */
    function getItems(): array;
}
