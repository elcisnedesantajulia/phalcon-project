<?php
declare(strict_types=1);
/**
 * @Filename: Handler.php
 * @Description:
 * @CreatedAt: 01/05/20 18:25
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl;


use Phalcon\Di\Injectable;
use Rcc\Phalcon\Acl\Users\Item as UserItem;
use Rcc\Phalcon\Ajax\Exception as AjaxException;
use Rcc\Phalcon\Ajax\Response\Response;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Phalcon\Utils\ActionInfo;

/**
 * @property Auth auth
 */
class Handler extends Injectable
{

    /**
     * @return Response
     * @throws Exception
     */
    function login(): Response
    {
        try {
            $request = RequestFactory::loginRequest()->populateFromHttp();
            if (!$this->security->checkToken($request->getString('key'), $request->getString('token'))) {
                throw new Exception("Incorrect CSRF token");
            }

            try {
                /*$userItem =*/ $this->auth->check($request->getString('email'), $request->getString('pass'));
            } catch (Exception $e) {
                return ResponseFactory::incorrectEmailPassword();
            }
            $response = new Response();
            /** @var ActionInfo $sessionActionInfo */
            $sessionActionInfo = $this->session->get('actionInfo');
            if (is_object($sessionActionInfo) == 'ActionInfo') {
                $response->pushField('actionDestino', $sessionActionInfo->getResourceName());
                $this->session->set('actionInfo', null);
            }

            return $response;
        } catch (AjaxException $e) {
            return ResponseFactory::incorrectEmailPassword();
        }
    }

    /**
     * @return Response
     * @throws Exception
     * @throws AjaxException
     * @throws CacheException
     */
    function register(): Response
    {
        $request = RequestFactory::registerRequest()->populateFromHttp();
        if (!$this->security->checkToken($request->getString('key'), $request->getString('token'))) {
            throw new Exception("Incorrect CSRF token");
        }

        $email = $request->getString('email');
        if (UserItem::exists($email)) {
            return new Response(false, "El email {$email} ya existe en este sistema", [
                'key' => $this->security->getTokenKey(),
                'token' => $this->security->getToken(),
            ]);
        }

        /*$userItem =*/ UserItem::createFromRegisterRequest($request);
        /*$userItem =*/ $this->auth->check($email, $request->getString('pass'));

        // TODO if needed, add a redirect to a previously saved desired resource
        // TODO if needed, save vars from session to user

        return new Response();
    }

    /**
     * @return Response
     * @throws AjaxException
     * @throws Exception
     * @throws CacheException
     */
    function newUser(): Response
    {
        $request = RequestFactory::newUserRequest()->populateFromHttp();
        $email = $request->getString('email');
        if (UserItem::exists($email)) {
            return Response::unsuccess("El email {$email} ya existe en este sistema");
        }

        /*$userItem =*/ UserItem::createFromNewUserRequest($request);

        return new Response();
    }

    /**
     * @return Response
     * @throws AjaxException
     * @throws Exception
     */
    function editUser(): Response
    {
        $request = RequestFactory::editUserRequest()->populateFromHttp();
        $userItem = UserItem::readFromDb($request->getInt('id'));
        /*$userItem =*/ $userItem->editFromRequest($request);

        return new Response();
    }

    /**
     * @return Response
     * @throws AjaxException
     * @throws Exception
     */
    function restorePass(): Response
    {
        $request = RequestFactory::restorePassRequest()->populateFromHttp();
        $userItem = UserItem::readFromDb($request->getInt('id'));
        /*$userItem =*/ $userItem->changePassword($request->getString('pass'));

        return new Response();
    }

    /**
     * @return Response
     * @throws AjaxException
     * @throws Exception
     */
    function blockUser(): Response
    {
        $request = RequestFactory::blockUserRequest()->populateFromHttp();
        $userItem = UserItem::readFromDb($request->getInt('id'));
        /*$userItem =*/ $userItem->block();

        return new Response();
    }

    /**
     * @return Response
     * @throws AjaxException
     * @throws Exception
     */
    function unblockUser(): Response
    {
        $request = RequestFactory::unblockUserRequest()->populateFromHttp();
        $userItem = UserItem::readFromDb($request->getInt('id'));
        /*$userItem =*/ $userItem->unblock();

        return new Response();
    }
}
