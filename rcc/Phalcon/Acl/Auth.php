<?php
declare(strict_types=1);
/**
 * @Filename: Auth.php
 * @Description:
 * @CreatedAt: 01/05/20 18:03
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl;


use Phalcon\Di\Injectable;
use Rcc\Phalcon\Acl\Users\Item as UserItem;
use Rcc\Phalcon\Cache\Exception as CacheException;

class Auth extends Injectable
{
    const SESSION_USER = 'user';

    /**
     * @param string $email
     * @param string $password
     * @return UserItem
     * @throws Exception
     */
    function check(string $email, string $password): UserItem
    {
        $userItem = UserItem::findByEmail($email);
        if ($userItem->isBlocked()) {
            throw new Exception("Auth::check() userId {$userItem->getId()} is blocked");
        }
        if (!$userItem->validatePassword($password)) {
            throw new Exception("Auth::check() userId {$userItem->getId()} password {$password} incorrecto");
        }

        $this->session->set(self::SESSION_USER, $userItem);

        return $userItem;
    }

    /**
     * @return UserItem
     * @throws CacheException
     * @throws Exception
     */
    function requireUserItem(): UserItem
    {
        if ($this->session->has(self::SESSION_USER)) {
            return $this->session->get(self::SESSION_USER);
        }

        return $this->identifyAsGuest();
    }

    /**
     * @return UserItem
     * @throws CacheException
     * @throws Exception
     */
    function identifyAsGuest(): UserItem
    {
        $userItem = UserItem::generateAnonymous();
        $this->session->set(self::SESSION_USER, $userItem);

        return $userItem;
    }
}
