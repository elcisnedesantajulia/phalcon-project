<?php
declare(strict_types=1);
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 01/05/20 18:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl;


class Exception extends \Exception
{

}
