<?php
/**
 * @Filename: Catalog.php
 * @Description:
 * @CreatedAt: 07/05/20 10:44
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Profiles;


use Rcc\Datatypes\Catalogs\CatalogInterface;

interface Catalog extends CatalogInterface
{
    /**
     * @return Item[]
     */
    function getItems(): array;
}
