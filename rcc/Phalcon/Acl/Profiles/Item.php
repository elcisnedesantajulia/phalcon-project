<?php
declare(strict_types=1);
/**
 * @Filename: Item.php
 * @Description:
 * @CreatedAt: 01/05/20 18:07
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Profiles;


use App\Models\Acl\ZProfiles;
use Rcc\Phalcon\Acl\Exception;
use Rcc\Phalcon\Cache\Exception as CacheException;
use Rcc\Phalcon\Cache\Handler as CacheHandler;

class Item
{
    const CACHE_SERVICE_NAME = 'zprofilesCache';
    const CACHE_PREFIX = 'id';
    const KIND_ROOT = 'root';
    const KIND_ADMIN = 'admin';
    const KIND_NEW = 'new';
    const KIND_ANONYMOUS = 'anonymous';
    const KIND_NORMAL = 'normal';

    /** @var int */
    private $id;
    /** @var string */
    private $caption;
    /** @var string */
    private $kind;
    /** @var bool */
    private $systemRequired;
    /** @var bool */
    private $blocked;

    private function __construct()
    {
    }

    /**
     * @return int
     */
    function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return string
     */
    function getKind(): string
    {
        return $this->kind;
    }

    function isRoot(): bool
    {
        return $this->kind == self::KIND_ROOT;
    }

    function isAnonymous(): bool
    {
        return $this->kind == self::KIND_ANONYMOUS;
    }

    /**
     * @return bool
     */
    function isBlocked(): bool
    {
        return $this->blocked;
    }

    static function readFromModel(ZProfiles $model): Item
    {
        $item = new self();
        $item->id = (int) $model->id;
        $item->caption = $model->caption;
        $item->kind = $model->kind;
        $item->systemRequired = $model->system_required;
        $item->blocked = (bool) $model->blocked;

        return $item;
    }

    /**
     * @param int $id
     * @return Item
     * @throws CacheException
     * @throws Exception
     */
    static function readFromDb(int $id): Item
    {
        $model = ZProfiles::findFirstById($id);
        if (!$model) {
            throw new Exception("ProfilesItem::readFromDb() id {$id} no existe el model");
        }

        $item = self::readFromModel($model);
        self::requireCacheHandler($id)->save($item);

        return $item;
    }

    /**
     * @param int $id
     * @return Item
     * @throws CacheException
     * @throws Exception
     */
    static function readFromCache(int $id): Item
    {
        $cacheHandler = self::requireCacheHandler($id);
        if ($cacheHandler->has()) {
            return $cacheHandler->get();
        }

        return self::readFromDb($id);
    }

    /**
     * @return Item
     * @throws CacheException
     * @throws Exception
     */
    static function readKindNew(): Item
    {
        /** @var ZProfiles $model */
        $model = ZProfiles::findFirstByKind(self::KIND_NEW);
        if (!$model) {
            throw new Exception("ProfilesItem::readKindNew() no existe el model con kind new");
        }

        return self::readFromCache((int) $model->id);
    }

    /**
     * @return Item
     * @throws Exception
     * @throws CacheException
     */
    static function readAnonymous(): Item
    {
        /** @var ZProfiles $model */
        $model = ZProfiles::findFirstByKind(self::KIND_ANONYMOUS);
        if (!$model) {
            throw new Exception("ProfilesItem::readAnonymous() no existe el model con kind anonymous");
        }

        return self::readFromCache((int) $model->id);
    }

    /**
     * @param int $id
     * @return CacheHandler
     * @throws CacheException
     */
    static private function requireCacheHandler(int $id): CacheHandler
    {
        return new CacheHandler(self::CACHE_SERVICE_NAME, self::CACHE_PREFIX . $id);
    }
}
