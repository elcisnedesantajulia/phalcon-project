<?php
declare(strict_types=1);
/**
 * @Filename: Main.php
 * @Description:
 * @CreatedAt: 07/05/20 10:44
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Acl\Profiles\Catalogs;


use App\Models\Acl\ZProfiles;
use Phalcon\Mvc\Model\Resultset\Simple;
use Rcc\Datatypes\Catalogs\Base;
use Rcc\Phalcon\Acl\Exception;
use Rcc\Phalcon\Acl\Profiles\Catalog;
use Rcc\Phalcon\Acl\Profiles\Item;
use Rcc\Phalcon\Cache\Exception as CacheException;

class Main extends Base implements Catalog
{
    const FILTER_ALL = 0;
    const FILTER_NOT_ROOT = 1;
    const FILTER_NOT_ANONYMOUS = 2;
    const FILTER_NOT_ROOT_NOT_ANONYMOUS = 3;

    /** @var Item[] */
    protected $items = [];

    private function __construct()
    {
    }

    /**
     * @param int $filter
     * @return Catalog
     * @throws Exception
     * @throws CacheException
     */
    static function readFromDb(int $filter = self::FILTER_ALL): Catalog
    {
        $catalog = new self();

        /** @var Simple $resultset */
        $resultset = ZProfiles::find(self::generateParameters($filter));
        if (count($resultset) == 0) {
            return $catalog;
        }

        foreach ($resultset as $model) {
            $catalog->items[] = Item::readFromCache((int) $model->id);
        }

        return $catalog;
    }

    static private function generateParameters(int $filter): array
    {
        if ($filter == self::FILTER_ALL) {
            return [];
        }
        if ($filter == self::FILTER_NOT_ROOT) {
            return [
                'kind != :kind:',
                'bind' => ['kind' => Item::KIND_ROOT]
            ];
        }
        if ($filter == self::FILTER_NOT_ANONYMOUS) {
            return [
                'kind != :kind:',
                'bind' => ['kind' => Item::KIND_ANONYMOUS]
            ];
        }
        if ($filter == self::FILTER_NOT_ROOT_NOT_ANONYMOUS) {
            return [
                'kind != :kind1: AND kind != :kind2:',
                'bind' => [
                    'kind1' => Item::KIND_ROOT,
                    'kind2' => Item::KIND_ANONYMOUS,
                ]
            ];
        }

        return [];
    }
}
