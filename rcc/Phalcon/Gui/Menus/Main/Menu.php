<?php
declare(strict_types=1);


namespace Rcc\Phalcon\Gui\Menus\Main;


use Rcc\Html\Complex\Nav\Dropdown\Menu as DropdownMenu;
use Rcc\Html\Complex\Nav\Nav as BsNav;
use Rcc\Html\Dom;
use Rcc\Html\Tags\A;
use Rcc\Html\Tags\Button;
use Rcc\Html\Tags\Div;
use Rcc\Html\Tags\Img;
use Rcc\Html\Tags\Nav;
use Rcc\Html\Tags\Span;
use Rcc\Phalcon\Babilonia\Projects\Catalogs\Main as ProjectsCatalog;
use Rcc\Phalcon\Utils\UrlsFactory;

class Menu extends Nav
{
    private $name = 'gui-main-menu';

    function __construct()
    {
        parent::__construct($this->name, ['navbar', 'navbar-expand-lg', 'navbar-dark', 'bg-dark']);
        $this->append($this->generateContainer());
    }

    private function generateContainer(): Dom
    {
        return (new Div("{$this->name}-container", ['container']))
            ->append($this->generateBrand())
            ->append($this->generateCollapseButton())
            ->append($this->generateCollapseNavbar());
    }

    private function generateBrand(): Dom
    {
        return (new A(UrlsFactory::fromResource()))
            ->pushOneClass('navbar-brand')
            ->append((new Img(UrlsFactory::fromResource('favicon/favicon-32x32.png'), 'Babilonia Logo'))->setHeight(30))
            ->appendText('Babilonia Music');
    }

    private function generateCollapseButton(): Dom
    {
        return (new Button("{$this->name}-button", ['navbar-toggler'], false, false))
            ->pushCustomData('toggle', 'collapse')
            ->pushCustomData('target', "#{$this->generateCollapsibleHtmlId()}")
            ->pushProperty('aria-controls', $this->generateCollapsibleHtmlId())
            ->pushProperty('aria-expanded', 'false')
            ->pushProperty('aria-label', 'Toggle navigation')
            ->append((new Span())->pushOneClass('navbar-toggler-icon'));
    }

    private function generateCollapseNavbar(): Dom
    {
        return (new Div($this->generateCollapsibleHtmlId(), ['collapse', 'navbar-collapse']))
            ->append($this->generateBootstrapNav());
    }

    private function generateBootstrapNav(): Dom
    {
        return (new BsNav("{$this->name}-nav", ['navbar-nav', 'mr-auto']))
            ->appendNavItem(UrlsFactory::fromResource('members'), 'Artistas')
            ->append($this->generateProyectosDropdown());
    }

    private function generateProyectosDropdown(): Dom
    {
        $dropdown = (new DropdownMenu("{$this->name}-projects", 'Proyectos'));

        $dropdown->appendDropdownItem(UrlsFactory::fromResource('projects/new'), 'Crear Nuevo proyecto');

        return $dropdown;
    }

    /* TODO delete this function
    private function generateCollapseUl(): Dom
    {
        return (new Ul())->pushClasses(['navbar-nav', 'mr-auto'])
            ->append((new Li())->pushOneClass('nav-item'));
    }*/

    private function generateCollapsibleHtmlId(): string
    {
        return "{$this->name}-collapsible";
    }
}
