<?php
declare(strict_types=1);
/**
 * @Filename: IntField.php
 * @Description:
 * @CreatedAt: 07/05/20 18:34
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Fields;


use Rcc\Phalcon\Ajax\Exception;
use Rcc\Phalcon\Ajax\Request\Request;
use Rcc\Phalcon\Ajax\Request\Validators\IntValidator;

class IntField extends FieldBase
{
    /** @var int */
    protected $value;
    /** @var string[] */
    protected $phalconFilters = ['int'];
    /** @var IntValidator[] */
    private $validators = [];

    function getType(): string
    {
        return Request::TYPE_INT;
    }

    function getValue(): int
    {
        if (!$this->isPopulated()) {
            throw new Exception("IntField::getValue() please execute populateFromHttp() before");
        }
        if ($this->isEmpty()) {
            throw new Exception("IntField::getValue() this field is empty");
        }

        return $this->value;
    }

    function pushValidator(IntValidator $validator): Field
    {
        $this->validators[] = $validator;

        return $this;
    }

    function populateFromHttp(): Field
    {
        $this->populated = true;
        try {
            $value = (int)(new Helper($this))->getString();
        } catch (Exception $e) {
            if (!$this->isOptional()) {
                throw new Exception("IntField::populateFromHttp() mandatorio -> {$e->getMessage()}");
            }

            return $this;
        }

        if ($this->validate($value)) {
            $this->value = $value;

            return $this;
        }

        if ($this->isOptional()) {
            return $this;
        }

        throw new Exception("IntField::populateFromHttp() -> {$this->getName()} mandatorio no cumplió con las validaciones");
    }

    private function validate(int $value): bool
    {
        foreach ($this->validators as $validator) {
            if (!$validator->validate($value)) {
                return false;
            }
        }

        return true;
    }
}
