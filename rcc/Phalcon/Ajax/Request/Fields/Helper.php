<?php
declare(strict_types=1);
/**
 * @Filename: Helper.php
 * @Description:
 * @CreatedAt: 01/05/20 16:27
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Fields;


use Phalcon\Di\Injectable;
use Rcc\Phalcon\Ajax\Exception;

class Helper extends Injectable
{
    /** @var string */
    private $name;
    /** @var string[] */
    private $phalconFilters;

    function __construct(Field $field)
    {
        $this->name = $field->getName();
        $this->phalconFilters = $field->getPhalconFilters();
    }

    /**
     * @return string
     * @throws Exception
     */
    function getString(): string
    {
        if (!$this->request->has($this->name)) {
            throw new Exception("Helper::getString() el request no tiene el campo {$this->name}");
        }

        $output = (string) $this->request->get($this->name, $this->phalconFilters);
        if ($output === '') {
            throw new Exception("Helper::getString() -> field {$this->name}: el valor está vacío después de aplicar los phalconFilters");
        }

        return $output;
    }
}
