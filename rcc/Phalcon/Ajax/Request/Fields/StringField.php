<?php
declare(strict_types=1);
/**
 * @Filename: StringField.php
 * @Description:
 * @CreatedAt: 01/05/20 15:58
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Fields;


use Rcc\Phalcon\Ajax\Exception;
use Rcc\Phalcon\Ajax\Request\Request;
use Rcc\Phalcon\Ajax\Request\Validators\StringValidator;

class StringField extends FieldBase
{
    /** @var string */
    protected $value;
    /** @var string[] */
    protected $phalconFilters = ['trim', 'striptags'];
    /** @var StringValidator[] */
    private $validators = [];

    function getType(): string
    {
        return Request::TYPE_STRING;
    }

    function getValue(): string
    {
        if (!$this->isPopulated()) {
            throw new Exception("StringField::getValue() please execute populateFromHttp() before");
        }
        if ($this->isEmpty()) {
            throw new Exception("StringField::getValue() this field is empty");
        }

        return $this->value;
    }

    /**
     * @param StringValidator $validator
     * @return StringField
     */
    function pushValidator(StringValidator $validator): Field
    {
        $this->validators[] = $validator;

        return $this;
    }

    /**
     * @return StringField
     * @throws Exception
     */
    function populateFromHttp(): Field
    {
        $this->populated = true;
        try {
            $value = (new Helper($this))->getString();
        } catch (Exception $e) {
            if (!$this->isOptional()) {
                throw new Exception("StringField::populateFromHttp() mandatorio -> {$e->getMessage()}");
            }

            return $this;
        }

        if ($this->validate($value)) {
            $this->value = $value;

            return $this;
        }

        if ($this->isOptional()) {
            return $this;
        }

        throw new Exception("StringField::populateFromHttp() -> {$this->getName()} mandatorio no cumplió con las validaciones");
    }

    private function validate(string $value): bool
    {
        foreach ($this->validators as $validator) {
            if (!$validator->validate($value)) {
                return false;
            }
        }

        return true;
    }
}
