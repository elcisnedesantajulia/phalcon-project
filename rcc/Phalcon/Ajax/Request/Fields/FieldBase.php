<?php
declare(strict_types=1);
/**
 * @Filename: FieldBase.php
 * @Description:
 * @CreatedAt: 07/05/20 20:46
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Fields;


abstract class FieldBase implements Field
{
    /** @var string */
    private $name;
    /** @var bool */
    private $optional;
    /** @var string[] */
    protected $phalconFilters = [];
    /** @var bool */
    protected $populated = false;
    protected $value;

    /**
     * FieldBase constructor.
     * @param string $name
     * @param bool $optional
     * @param string[] $phalconFilters
     */
    function __construct(string $name, bool $optional = false, array $phalconFilters = [])
    {
        $this->name = $name;
        $this->optional = $optional;
        $this->phalconFilters = array_merge($this->phalconFilters, $phalconFilters);
    }

    /**
     * @return string
     */
    function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * @return string[]
     */
    function getPhalconFilters(): array
    {
        return $this->phalconFilters;
    }

    function pushPhalconFilter(string $phalconFilter): Field
    {
        $this->phalconFilters[] = $phalconFilter;

        return $this;
    }

    function isPopulated(): bool
    {
        return $this->populated;
    }


    function isEmpty(): bool
    {
        return is_null($this->value);
    }
}
