<?php
/**
 * @Filename: Field.php
 * @Description:
 * @CreatedAt: 01/05/20 12:53
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Fields;


use Rcc\Phalcon\Ajax\Exception;

interface Field
{
    function getName(): string;

    function getType(): string;

    /**
     * If method populateFromHttp() has been executed, indicates than user didnt send a valid value
     * @return bool
     */
    function isEmpty(): bool;

    /**
     * This flag indicates if method populateFromHttp() has been executed. Field can be populated but empty
     * @return bool
     */
    function isPopulated(): bool;

    /**
     * @throws Exception
     */
    function populateFromHttp(): Field;

    /**
     * @throws Exception
     */
    function getValue();

    /**
     * @return string[]
     */
    function getPhalconFilters(): array;
}
