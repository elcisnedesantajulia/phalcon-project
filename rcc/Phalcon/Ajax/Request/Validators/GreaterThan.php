<?php
declare(strict_types=1);
/**
 * @Filename: GreaterThan.php
 * @Description:
 * @CreatedAt: 07/05/20 19:15
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


class GreaterThan implements IntValidator
{
    /** @var int */
    private $exclusiveLimit;

    function __construct(int $exclusiveLimit)
    {
        $this->exclusiveLimit = $exclusiveLimit;
    }

    function validate(int $value): bool
    {
        if ($value > $this->exclusiveLimit) {
            return true;
        }

        return false;
    }
}
