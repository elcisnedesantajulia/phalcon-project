<?php
/**
 * @Filename: Validator.php
 * @Description:
 * @CreatedAt: 01/05/20 12:51
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


use Rcc\Phalcon\Ajax\Request\Fields\Field;

interface Validator
{
    function validate(string $value): bool;
}
