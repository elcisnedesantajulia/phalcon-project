<?php
declare(strict_types=1);
/**
 * @Filename: RegexValidator.php
 * @Description:
 * @CreatedAt: 01/05/20 17:08
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


class Regex implements StringValidator
{
    const PATTERN_EMAIL = "#^[a-zA-Z][\w.-]*@[\w.-]+\.\w+\s*$#";
    const PATTERN_PASSWORD = "#\S{6,16}#";
    const PATTERN_CP = "#\d{5}#";
    const PATTERN_USOCFDI = "#\w{3}#";
    const PATTERN_AT_LEAST_ONE_CHAR = "#\S#";
    const PATTERN_AT_LEAST_TWO_CHARS = "#\S{2}#";
    const PATTERN_AT_LEAST_ONE_WORDCHAR = "#\w#";
    const PATTERN_AT_LEAST_TWO_WORDCHARS = "#\w{2}#";
    const PATTERN_RGB_COLOR = "#[\da-fA-F]{6}#";

    /** @var string */
    private $pattern;

    /**
     * Regex constructor.
     * @param string $pattern
     */
    function __construct(string $pattern)
    {
        $this->pattern = $pattern;
    }

    function validate(string $value): bool
    {
        if (preg_match($this->pattern, $value)) {
            return true;
        }

        return false;
    }
}
