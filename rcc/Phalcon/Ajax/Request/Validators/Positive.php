<?php
declare(strict_types=1);
/**
 * @Filename: Positive.php
 * @Description:
 * @CreatedAt: 07/05/20 19:24
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


class Positive extends GreaterThan
{

    function __construct()
    {
        parent::__construct(0);
    }
}
