<?php
/**
 * @Filename: IntValidator.php
 * @Description:
 * @CreatedAt: 07/05/20 18:41
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


interface IntValidator
{
    function validate(int $value): bool;
}
