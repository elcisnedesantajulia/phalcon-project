<?php
/**
 * @Filename: StringValidator.php
 * @Description:
 * @CreatedAt: 01/05/20 16:05
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request\Validators;


interface StringValidator
{
    function validate(string $value): bool;
}
