<?php
/**
 * @Filename: Request.php
 * @Description:
 * @CreatedAt: 01/05/20 12:41
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Request;


use Rcc\Phalcon\Ajax\Exception;
use Rcc\Phalcon\Ajax\Request\Fields\Field;
use stdClass;

class Request
{
    const TYPE_STRING = 'string';
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOL = 'bool';
    const TYPE_OBJECT = 'object';

    private $populated = false;
    /** @var Field[] */
    private $fields = [];

    /**
     * @return bool
     */
    function isPopulated(): bool
    {
        return $this->populated;
    }

    function hasString(string $key): bool
    {
        return $this->hasType($key, self::TYPE_STRING);
    }

    /**
     * @param string $key
     * @return string
     * @throws Exception
     */
    function getString(string $key): string
    {
        if ($this->hasString($key)) {
            return $this->get($key)->getValue();
        }

        throw new Exception("Request::getString() key {$key} doesnt exist");
    }

    function hasInt(string $key): bool
    {
        return $this->hasType($key, self::TYPE_INT);
    }

    /**
     * @param string $key
     * @return int
     * @throws Exception
     */
    function getInt(string $key): int
    {
        if ($this->hasInt($key)) {
            return $this->get($key)->getValue();
        }

        throw new Exception("Request::getInt() key {$key} doesnt exist");
    }

    function hasFloat(string $key): bool
    {
        return $this->hasType($key, self::TYPE_FLOAT);
    }

    /**
     * @param string $key
     * @return float
     * @throws Exception
     */
    function getFloat(string $key): float
    {
        if ($this->hasFloat($key)) {
            return $this->get($key)->getValue();
        }

        throw new Exception("Request::getFloat() key {$key} doesnt exist");
    }

    function hasBool(string $key): bool
    {
        return $this->hasType($key, self::TYPE_BOOL);
    }

    /**
     * @param string $key
     * @return bool
     * @throws Exception
     */
    function getBool(string $key): bool
    {
        if ($this->hasBool($key)) {
            return $this->get($key)->getValue();
        }

        throw new Exception("Request::getBool() key {$key} doesnt exist");
    }

    function hasObject(string $key): bool
    {
        return $this->hasType($key, self::TYPE_OBJECT);
    }

    function getObject(string $key): stdClass
    {
        if ($this->hasObject($key)) {
            return $this->get($key)->getValue();
        }

        throw new Exception("Request::getObject() key {$key} doesnt exist");
    }

    /**
     * This function is useful for debugging
     * @return array
     */
    function generateArray(): array
    {
        $output = [];
        foreach ($this->fields as $key => $field) {
            $output[$key]['defined'] = get_class($field);
            if ($this->has($key)) {
                $output[$key]['populated'] = $field->getValue();
            }
        }

        return $output;
    }

    function pushField(Field $field): Request
    {
        $this->fields[$field->getName()] = $field;

        return $this;
    }

    /**
     * @return $this
     * @throws Exception
     */
    function populateFromHttp(): Request
    {
        if ($this->populated) {
            return $this;
        }

        foreach ($this->fields as $field) {
            $field->populateFromHttp();
        }
        $this->populated = true;

        return $this;
    }

    private function hasType(string $key, string $type): bool
    {
        if (!$this->has($key)) {
            return false;
        }

        return $this->fields[$key]->getType() == $type;
    }

    private function has(string $key): bool
    {
        if (!$this->isPopulated()) {
            return false;
        }

        if (!key_exists($key, $this->fields)) {
            return false;
        }

        $field = $this->fields[$key];
        if (!$field->isPopulated()) {
            return false;
        }

        return !$field->isEmpty();
    }

    /**
     * @param string $key
     * @return Field
     * @throws Exception
     */
    private function get(string $key): Field
    {
        if ($this->has($key)) {
            return $this->fields[$key];
        }

        throw new Exception("Request::get() key {$key} doesnt exist");
    }
}
