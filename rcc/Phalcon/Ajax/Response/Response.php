<?php
/**
 * @Filename: Response.php
 * @Description:
 * @CreatedAt: 01/05/20 12:35
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax\Response;


class Response
{
    /** @var bool */
    private $success;
    /** @var string */
    private $caption;
    /** @var array */
    private $fields;

    /**
     * Response constructor.
     * @param bool $success
     * @param string $caption
     * @param array $fields
     */
    function __construct(bool $success = true, string $caption = 'Success', array $fields = [])
    {
        $this->success = $success;
        $this->caption = $caption;
        $this->fields = $fields;
    }

    function pushField(string $key, $payload): Response
    {
        $this->fields[$key] =$payload;

        return $this;
    }

    function toArray(): array
    {
        return array_merge([
            'success' => $this->success,
            'caption' => $this->caption,
        ], $this->fields);
    }

    static function unsuccess(string $caption = 'Unsuccess')
    {
        return new self(false, $caption);
    }
}
