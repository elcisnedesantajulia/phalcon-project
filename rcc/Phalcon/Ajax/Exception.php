<?php
declare(strict_types=1);
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 01/05/20 13:57
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Ajax;


class Exception extends \Exception
{

}
