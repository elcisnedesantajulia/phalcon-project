<?php
declare(strict_types=1);


namespace Rcc\Phalcon\Utils;


use Phalcon\Config;
use Phalcon\Di\Injectable;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;

/**
 * @property Config config
 */
class LoggerFactory extends Injectable
{
    /** @var string */
    private $name;
    /** @var Stream */
    private $adapter;

    private function __construct(string $name = 'generic')
    {
        $this->name = $name;
        $this->adapter = new Stream("{$this->config->get('application')->logsDir}{$name}.log");
    }

    function generateLogger(): Logger
    {
        return new Logger(
            $this->name,
            [
                'main' => $this->adapter,
            ]
        );
    }

    static function generic(): Logger
    {
        $factory = new self();

        return $factory->generateLogger();
    }

    static function emergency(): Logger
    {
        $factory = new self('emergency');

        return $factory->generateLogger();
    }
}
