<?php
declare(strict_types=1);
/**
 * @Filename: ActionInfo.php
 * @Description:
 * @CreatedAt: 30/04/20 17:21
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Utils;


use Phalcon\Mvc\Dispatcher;

class ActionInfo
{
    const CONTROLLER_GUEST = 'guest';

    /** @var string */
    private $controller;
    /** @var string */
    private $action;

    /**
     * ActionInfo constructor.
     * @param string $controller
     * @param string $action
     */
    function __construct(string $controller = 'index', string $action = 'index')
    {
        $this->controller = $controller;
        $this->action = $action;
    }

    /**
     * @return string
     */
    function getController(): string
    {
        return $this->controller;
    }

    function isGuestController(): bool
    {
        return $this->getController() == self::CONTROLLER_GUEST;
    }

    /**
     * @return string
     */
    function getAction(): string
    {
        return $this->action;
    }

    function getResourceName(): string
    {
        return "{$this->controller}/{$this->action}";
    }

    function isAjax(): bool
    {
        return strpos($this->action, 'ajax') === 0;
    }

    static function readFromDispatcher(Dispatcher $dispatcher): ActionInfo
    {
        return new self($dispatcher->getControllerName(), $dispatcher->getActionName());
    }
}
