<?php
declare(strict_types=1);


namespace Rcc\Phalcon\Utils;


use Phalcon\Di\Injectable;

class UrlsFactory extends Injectable
{
    private function __construct()
    {
    }

    static function fromResource(string $resource = ''): string
    {
        return (new self())->url->get($resource);
    }

    static function fqnFromResource(string $resource = ''): string
    {
        //TODO implement this function adding config->website->server_addr
        return '';
    }
}
