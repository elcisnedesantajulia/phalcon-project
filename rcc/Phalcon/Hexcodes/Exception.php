<?php
declare(strict_types=1);
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 02/05/20 22:19
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Hexcodes;


class Exception extends \Exception
{

}
