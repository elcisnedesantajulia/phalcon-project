<?php
declare(strict_types=1);
/**
 * @Filename: Factory.php
 * @Description:
 * @CreatedAt: 02/05/20 22:18
 * @Author: Roman Cisneros romancc9@gmail.com
 * Code is poetry
 */

namespace Rcc\Phalcon\Hexcodes;


use App\Models\Hexcodes\YHexcodes;
use PDOException;
use Phalcon\Security\Exception as SecurityException;
use Phalcon\Security\Random;

class Hexcode
{
    const BYTES_LENGTH = 8; // To generate a 16 chars hex string. Example: a29f470508d5ccb8
    const PATTERN_HEXCODE = "#^[\da-f]{16}$#";

    /** @var Random */
    private $random;

    private function __construct()
    {
        $this->random = new Random();
    }

    static function generateNew(string $purpose): string
    {
        $item = new self();
        return $item->generateUnique($purpose);
    }

    static function hasPattern(string $hexcode): bool
    {
        return preg_match(self::PATTERN_HEXCODE, $hexcode);
    }

    private function generateUnique(string $purpose): string
    {
        $hex = '';
        while (empty($hex)) {
            $nominee = $this->generate();

            $model = new YHexcodes([
                'hex' => $nominee,
                'purpose' => $purpose,
            ]);
            try {
                if ($model->save()) {
                    $hex = $nominee;
                } else {
                    throw new Exception("Hexcode::generateUnique() No se pudo guardar el nuevo folio {$nominee} en la DB: {$model->getMessage()}");
                }
            } catch (PDOException $e) {
                continue;
            }
        }

        return $hex;
    }

    private function generate(): string
    {
        $hex = '';
        while (empty($hex)) {
            try {
                $nominee = $this->random->hex(self::BYTES_LENGTH);
            } catch (SecurityException $e) {
                throw new Exception("Hexcode::generate() problema con PhalconRandom::hex(): {$e->getMessage()}");
            }
            if (preg_match("#^\d#", $nominee)) { // Avoid starting with a number
                continue;
            }
            if (preg_match("#\d{3}#", $nominee)) { // Avoid having three o more adjacent numbers
                continue;
            }
            $hex = $nominee;
        }

        return $hex;
    }
}
