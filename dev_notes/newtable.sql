CREATE TABLE y_hexcodes (
 `id` int unsigned NOT NULL AUTO_INCREMENT,
 `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 `ctime` timestamp NULL DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `ctime` (`ctime`)
) ENGINE=InnoDB
