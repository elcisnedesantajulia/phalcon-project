
<!-- jQuery -->
<script src="{{ url('gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ url('gentelella/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('gentelella/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ url('gentelella/vendors/nprogress/nprogress.js') }}"></script>
<!-- PNotify -->
<script src="{{ url('gentelella/vendors/pnotify/dist/pnotify.js') }}"></script>
<script src="{{ url('gentelella/vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ url('gentelella/vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ url('gentelella/build/js/custom.min.js') }}"></script>