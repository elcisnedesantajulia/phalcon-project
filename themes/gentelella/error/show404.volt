
    <div class="col-md-12">
      <div class="col-middle">
        <div class="text-center text-center">
          <h1 class="error-number">404 - Not Found!</h1>
          <h2>Lamentamos los inconvenientes</h2>
          <p>La página que buscas no existe. Entra <a href="{{ url() }}">aquí</a> para continuar
          </p>
        </div>
      </div>
    </div>
