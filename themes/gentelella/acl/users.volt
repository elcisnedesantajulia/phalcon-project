<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Usuarios</h3>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12  ">
      <div class="x_panel">
        <div class="x_title">
          {#<h2>Plain Page</h2>#}
          <ul class="nav navbar-right panel_toolbox">
            {#<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>#}
            <li>
              <a class="text-success" href="#" data-toggle="modal" data-target="#nu-modal">
                <i class="fa fa-plus"></i> Nuevo Usuario
              </a>
            </li>
            {#</li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">Settings 1</a>
                <a class="dropdown-item" href="#">Settings 2</a>
              </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>#}
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          {{ usersList.toHtml() }}
        </div>
      </div>
    </div>
  </div>

  {{ newUserModal.toHtml() }}
  {{ editUserModal.toHtml() }}
  {{ restorePasswordModal.toHtml() }}
  {{ blockUserModal.toHtml() }}
  {{ unblockUserModal.toHtml() }}

</div>
