<div class="">
  <div class="page-title">
    <div class="title_left">
      <h3>Perfiles</h3>
    </div>
  </div>

  <div class="clearfix"></div>

  <div class="row">
    <div class="col-md-12 col-sm-12  ">
      <div class="x_panel">
        <div class="x_title">
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="text-success" href="#"><i class="fa fa-plus"></i> Nuevo Perfil</a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          {#{ usersList.toHtml() }#}
        </div>
      </div>
    </div>
  </div>

</div>
