<body class="nav-md">

<div class="container body">
  <div class="main_container">
    <div class="col-md-3 left_col">
      <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
          <a href="#" class="site_title"><img src="{{ url('favicon/favicon-32x32.png') }}" alt="Logo centinela"> <span>Centinela</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
          <div class="profile_pic">
            <img src="{{ url('img/profile_default.png') }}" alt="..." class="img-circle profile_img">
          </div>
          <div class="profile_info">
            <span>Bienvenido,</span>
            <h2>{{ user.getFullName() }}</h2>
          </div>
          <div class="clearfix"></div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
          <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
              <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="{{ url() }}">Dashboard</a></li>
                </ul>
              </li>
              <li><a><i class="fa fa-key"></i> Admin <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                  <li><a href="{{ url('acl/users') }}">Usuarios</a></li>
                  <li><a href="{{ url('acl/profiles') }}">Perfiles</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <!-- /sidebar menu -->

      </div>
    </div>

    <!-- top navigation -->
    <div class="top_nav">
      <div class="nav_menu">
        <div class="nav toggle">
          <a id="menu_toggle"><i class="fa fa-bars"></i></a>
        </div>
        <nav class="nav navbar-nav">
          <ul class=" navbar-right">
            <li class="nav-item dropdown open" style="padding-left: 15px;">
              <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                <img src="{{ url('img/profile_default.png') }}" alt="Profile pic">{{ user.getFullName() }}
              </a>
              <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item"  href="#" data-toggle="modal" data-target="#ua-modal"><i class="fa fa-user pull-right"></i> Mi cuenta</a>
                <a class="dropdown-item"  href="#" data-toggle="modal" data-target="#chp-modal"><i class="fa fa-key pull-right"></i> Cambiar password</a>
                <a id="profile-logout" class="dropdown-item"  href="#"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
              </div>
            </li>
          </ul>
        </nav>
      </div>
    </div>
    <!-- /top navigation -->

    <div class="right_col" role="main">

{{ content() }}

{{ user.generateUserAccountModal().toHtml() }}
{{ changePasswordModal.toHtml() }}

    </div>
    <!-- footer content -->
    <footer>
      <div class="pull-right">
        Softle 2020 contacto@softle.com
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
</div>

{{ partial('partials/scripts_dependencies') }}

<script src="{{ url('js/prototypes.js') }}"></script>
<script src="{{ url('js/modules.js') }}"></script>
<script src="{{ url('js/users.js') }}"></script>
<script src="{{ url('js/front.js') }}"></script>
<script type="text/javascript">
    $(document).ready(front.construct("{{ url() }}", "{{ resource }}"))
</script>

</body>
