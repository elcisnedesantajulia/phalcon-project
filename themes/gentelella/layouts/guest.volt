
<body class="login">

{{ content() }}

{{ partial('partials/scripts_dependencies') }}

<script src="{{ url('js/prototypes.js') }}"></script>
<script src="{{ url('js/guest.js') }}"></script>
<script type="text/javascript">
    $(document).ready(front.construct("{{ url() }}", "{{ resource }}"))
</script>
</body>
