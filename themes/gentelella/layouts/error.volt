
<body class="nav-md">
<div class="container body">
  <div class="main_container">

    {{ content() }}

  </div>
</div>

{{ partial('partials/scripts_dependencies') }}
</body>
