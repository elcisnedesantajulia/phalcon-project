<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Centinela</title>

  <!-- Bootstrap -->
  <link href="{{ url('gentelella/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="{{ url('gentelella/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <!-- NProgress -->
  <link href="{{ url('gentelella/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
  <!-- Animate.css -->
  <link href="{{ url('gentelella/vendors/animate.css/animate.min.css') }}" rel="stylesheet">
  <!-- PNotify -->
  <link href="{{ url('gentelella/vendors/pnotify/dist/pnotify.css') }}" rel="stylesheet">
  <link href="{{ url('gentelella/vendors/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">
  <link href="{{ url('gentelella/vendors/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">
  <!-- Open Iconic -->
  <link href="{{ url('open-iconic/font/css/open-iconic-bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom Theme Style -->
  <link href="{{ url('gentelella/build/css/custom.min.css') }}" rel="stylesheet">

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="{{ url('favicon/apple-touch-icon.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ url('favicon/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ url('favicon/favicon-16x16.png') }}">
  <link rel="manifest" href="{{ url('favicon/site.webmanifest') }}">
  <link rel="mask-icon" href="{{ url('favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
</head>

{{ content() }}

</html>