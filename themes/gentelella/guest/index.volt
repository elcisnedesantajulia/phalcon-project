
<div>
  <a class="hiddenanchor" id="signup"></a>
  <a class="hiddenanchor" id="signin"></a>

  <div class="login_wrapper">
    <div class="animate form login_form">
      <section class="login_content">
        <form id="guest-login-form" data-key="{{ security.getTokenKey() }}" data-token="{{ security.getToken() }}">
          <h1>Ingresar</h1>
          <div>
            <input id="guest-login-email" type="email" class="form-control" placeholder="Email" required="" />
          </div>
          <div>
            <input id="guest-login-pass" type="password" class="form-control" placeholder="Password" required="" />
          </div>
          <div>
            <a id="guest-login-submit" class="btn btn-default btn-success submit" href="#">Enviar</a>
            <a id="guest-reset-pass" class="reset_pass" href="#">¿Olvidaste tu password?</a>
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">¿No tienes cuenta?
              <a href="#signup" class="to_register"> Regístrate </a>
            </p>

          </div>
        </form>
      </section>
    </div>

    <div id="register" class="animate form registration_form">
      <section class="login_content">
        <form id="guest-register-form" data-key="{{ security.getTokenKey() }}" data-token="{{ security.getToken() }}">
          <h1>Crear Cuenta</h1>
          <div>
            <input id="guest-register-name" type="text" class="form-control mb-0 mt-4" placeholder="Nombre Completo" required="" />
            <div class="invalid-feedback">Este campo es requerido</div>
          </div>
          <div>
            <input id="guest-register-email" type="email" class="form-control mb-0 mt-4" placeholder="Email" required="" />
            <div class="invalid-feedback">Debes introducir un email válido</div>
          </div>
          <div>
            <input id="guest-register-pass" type="password" class="form-control mb-0 mt-4" placeholder="Password" required="" />
            <div class="invalid-feedback">El password debe tener al menos 6 caracteres</div>
          </div>
          <div>
            <input id="guest-register-confirm" type="password" class="form-control mb-0 mt-4" placeholder="Confirmar Password" required="" />
            <div class="invalid-feedback">La confirmación no coincide</div>
          </div>
          <div>
            <a id="guest-register-submit" class="btn btn-default btn-success submit text-light mt-4">Enviar</a>
          </div>

          <div class="clearfix"></div>

          <div class="separator">
            <p class="change_link">¿Ya tienes cuenta?
              <a href="#signin" class="to_register"> Ingresa </a>
            </p>

          </div>
        </form>
      </section>
    </div>
  </div>
</div>
