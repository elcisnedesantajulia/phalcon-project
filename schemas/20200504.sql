-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: localhost    Database: phalcon_project
-- ------------------------------------------------------
-- Server version	8.0.19-0ubuntu0.19.10.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `y_hexcodes`
--

DROP TABLE IF EXISTS `y_hexcodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `y_hexcodes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ctime` timestamp NULL DEFAULT NULL,
  `hex` char(16) NOT NULL,
  `purpose` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hex` (`hex`),
  KEY `ctime` (`ctime`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_hexcodes`
--

LOCK TABLES `y_hexcodes` WRITE;
/*!40000 ALTER TABLE `y_hexcodes` DISABLE KEYS */;
INSERT INTO `y_hexcodes` VALUES (1,'2020-05-03 03:45:29','2020-05-03 03:45:29','db31e26cfcab5c63','just testing'),(2,'2020-05-03 03:45:43','2020-05-03 03:45:43','e85d72aacb44fe2c','just testing'),(3,'2020-05-03 03:45:45','2020-05-03 03:45:45','fb9a12bc24ec9ba5','just testing'),(4,'2020-05-03 03:45:45','2020-05-03 03:45:45','b3a48b7a38b04bbc','just testing'),(5,'2020-05-03 03:45:46','2020-05-03 03:45:46','f75dcc31b7c8d6a3','just testing');
/*!40000 ALTER TABLE `y_hexcodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_profiles`
--

DROP TABLE IF EXISTS `z_profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `z_profiles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ctime` timestamp NULL DEFAULT NULL,
  `caption` varchar(64) NOT NULL,
  `kind` enum('root','admin','normal','anonymous') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'normal',
  `system_required` tinyint(1) NOT NULL DEFAULT '0',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ctime` (`ctime`),
  KEY `system_required` (`system_required`),
  KEY `blocked` (`blocked`),
  KEY `kind` (`kind`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_profiles`
--

LOCK TABLES `z_profiles` WRITE;
/*!40000 ALTER TABLE `z_profiles` DISABLE KEYS */;
INSERT INTO `z_profiles` VALUES (1,'2020-05-02 02:04:10','2020-04-30 17:57:38','Administrador de sistema','root',1,0),(2,'2020-05-02 02:34:54','2020-04-30 17:57:38','Administrador','admin',1,0),(3,'2020-04-30 18:00:23','2020-04-30 17:58:49','Usuario','normal',1,0),(4,'2020-05-02 02:04:19','2020-04-30 17:58:49','Anónimo','anonymous',1,0),(5,'2020-04-30 17:59:18','2020-04-30 17:59:18','Capturista','normal',0,0);
/*!40000 ALTER TABLE `z_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `z_users`
--

DROP TABLE IF EXISTS `z_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `z_users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ctime` timestamp NULL DEFAULT NULL,
  `email` varchar(64) NOT NULL,
  `password` char(60) NOT NULL,
  `full_name` varchar(64) NOT NULL,
  `profileId` int unsigned NOT NULL,
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `ctime` (`ctime`),
  KEY `blocked` (`blocked`),
  KEY `profileId` (`profileId`) USING BTREE,
  CONSTRAINT `z_users_ibfk_1` FOREIGN KEY (`profileId`) REFERENCES `z_profiles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `z_users`
--

LOCK TABLES `z_users` WRITE;
/*!40000 ALTER TABLE `z_users` DISABLE KEYS */;
INSERT INTO `z_users` VALUES (1,'2020-04-30 17:57:51','2020-04-30 03:39:25','romancc9@gmail.com','$2y$08$ZmRvNDJqVWt2dy9jM3BpSuiO7m4DOnv7DL1B6mTY78SZGXNkvuZ4C','Roman Cisneros',1,0);
/*!40000 ALTER TABLE `z_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-04  8:44:55
