
/**
 * Módulo usado para presentar los productos, filtrar por categoría y moverse por las páginas.
 * @type {{construct: construct}}
 */
let paginator = (function () {
    let urlTienda;
    let urlAjax;
    let addToCart;
    let catSelected = 0;

    let construct = function (urlBase, addToCartCallback, avoidAjaxLoadOnConstruct, isHome) {
        urlTienda = urlBase + 'tienda/index/';
        urlAjax = isHome ? urlBase + 'index/ajaxPaginator' : urlBase + 'tienda/ajaxPaginator';
        if (typeof addToCartCallback === 'function') {
            addToCart = addToCartCallback;
        }

        $('.btn-categoria').click(onClickButtonCategoria);

        if (avoidAjaxLoadOnConstruct === undefined) {
            let remote = newRemote();
            if (loader.jQSel.data('clase')) {
                remote.pushDataItem('clase', loader.jQSel.data('clase'));
            }
            if (loader.jQSel.data('subclase')) {
                remote.pushDataItem('subclase', loader.jQSel.data('subclase'));
            }
            remote.request();
        }
    };

    let changeCategoria = function (categoria) {
        catSelected = categoria;
        selectCategoria();
    };

    let selectCategoria = function () {
        if (detalleProducto.hasDom()) {
            detalleProducto.clear().hide();
            loader.show();
        }
        let remote = newRemote();
        remote.pushDataItem('subclase', catSelected);
        remote.request();
    };

    let addListenerToButtonPage = function () {
        $('.page-productos').click(onClickButtonPage);
    };

    let onClickButtonPage = function(event) {
        event.preventDefault();
        let remote = newRemote();
        remote.pushData('cat', catSelected);
        remote.pushData('page', $(this).data('page'));
        remote.request();
    };

    let onClickButtonCategoria = function () {
        window.location = urlTienda + $(this).data('id');
        //catSelected = $(this).data('id');
        //selectCategoria();
    };

    let onSuccessAjax = function (data) {
        // noinspection JSUnresolvedVariable
        if (data.success === false) {
            onErrorAjax();
            return;
        }
        loader.loading.hide();
        loader.empty.hide();
        //loader.header.replace(data.header, addListenerToButtonPage).show();
        if (data.footer) {
            loader.header.replace(data.header).show(); // Para que no se duplique el listenerButtonPage, se manda en la línea inferior
            loader.footer.replace(data.header, addListenerToButtonPage).show();
        } else {
            loader.header.replace(data.header, addListenerToButtonPage).show();
            loader.footer.clear().hide();
        }
        loader.body.replace(data.body, addToCart, true).show();
    };

    let onErrorAjax = function () {
        loader.loading.hide();
        loader.header.hide();
        loader.body.hide();
        loader.empty.show();
    };

    let newRemote = function () {
        return new Remote(urlAjax,'GET', onSuccessAjax, onErrorAjax);
    };

    let loader = new Loader('productos');
    let detalleProducto = new Container('detalleProducto');

    return {
        construct: construct,
        changeCategoria: changeCategoria,
    };
})();

let breadcrumb = (function () {

    let init = function() {
        $('#collapse-clase-id-' + clase.getDataValue('id')).collapse('show');
    };

    let onClickClase = function () {
        $('#collapse-clase-id-' + clase.getDataValue('id')).collapse('toggle');
    };

    let onClickSubclase = function () {
        paginator.changeCategoria(subclase.getDataValue('id'));
        //console.log('on click subclase ' + subclase.getDataValue('id'));
    };

    let clase = new Link('bread-clase', onClickClase);

    let subclase = new Link('bread-subclase', onClickSubclase);

    return {
        init: init,
    };
})();

let eligeSubclase = (function () {
    let urlAjax;

    let construct = function (urlBase) {
        urlAjax = urlBase + 'tienda/ajaxSubclases/';
        modal.jQSel.on('show.bs.modal', onModalShow);
    };

    let onModalShow = function (event) {
        loading.show();
        let btn = $(event.relatedTarget);
        title.replace(btn.data('caption'));
        let ajax = new Remote(urlAjax + btn.data('clase'), 'GET', onAjaxSuccess, onAjaxError);
        ajax.request();
    };

    let onAjaxSuccess = function (data) {
        loading.hide();
        list.replace(data.payload);
        //console.log(data);
    };

    let onAjaxError = function () {
        // Si falla el AJAX lo mejor es refrescar la página
        window.location.reload();
    };

    let modal = new Container('ChooseSubclaseModal');
    let title = new Container('ChooseSubclaseModalTitle');
    let loading = new Container('ChooseSubclaseModalLoading');
    let list = new Container('ChooseSubclaseModalList');

    return {
        construct: construct,
    };
})();

let buscador = (function () {
    let urlAjax;
    //let urlDetail;

    let construct = function (urlBase) {
        urlAjax = urlBase + 'tienda/ajaxSearch';
        //urlDetail = urlBase + 'tienda/detalle';
    };

    let onBuscadorInput = function () {
        if (/\w{3}\w*/.test(input.getVal())) {
            let ajax = new Remote(urlAjax, 'GET', onAjaxSuccess, onAjaxFails);
            ajax.pushDataItem('q', input.getVal());
            ajax.request();
        } else {
            results.hide();
        }
    };

    let onAjaxSuccess = function (data) {
        results.replaceData(data);
    };

    let onAjaxFails = function () {
        results.hide();
    };

    let input = new InputText('buscador-input', onBuscadorInput);

    let results = (function () {

        let replaceData = function (data) {
            console.log(data);

            if (data.cantidad === 0) {
                alert.show();
                list.clear().hide();
            } else {
                alert.hide();
                list.show().replace(data.payload);
            }

            container.jQSel.collapse('show');
        };

        let hide = function () {
            list.clear();
            container.jQSel.collapse('hide');
        };

        let container = new Container('buscador-collapse');
        let alert = new Container('buscador-alert');
        let list = new Container('buscador-list');

        return {
            replaceData: replaceData,
            hide: hide,
        };
    })();

    return {
        construct: construct,
    }
})();

let contacto = (function () {

    let urlAjax;

    let init = function (urlBase) {
        urlAjax = urlBase + 'index/ajaxContacto';
    };

    let onChangeForm = function () {
        let globals = new Globals();

        nombre_ok = nombre.validateRegex(globals.atLeastTwoChars);
        email_ok = email.validateRegex(globals.emailPattern);
        telefono_ok = telefono.validateRegex(globals.atLeastTwoChars);
        comentarios_ok = comentarios.validateRegex(globals.atLeastTwoChars);

        if (nombre_ok && email_ok && telefono_ok && comentarios_ok) {
            btn.enable();
        } else {
            btn.disable();
        }
    };

    let onSuccessAjax = function () {
        container.hide();
        jumbotron.show();
    };

    let onClickBtn = function () {
        console.log('Button contacto clicked');

        let ajax = new Remote(urlAjax, 'POST', onSuccessAjax);
        ajax.setDataObject({
            nombre: nombre.getVal(),
            empresa: empresa.getVal(),
            email: email.getVal(),
            telefono: telefono.getVal(),
            comentarios: comentarios.getVal(),
        });
        ajax.request();
    };

    let container = new Container('form-contacto', onChangeForm);
    let nombre = new InputText('nombre', onChangeForm);
    let empresa = new InputText('empresa');
    let email = new InputText('email', onChangeForm);
    let telefono = new InputText('telefono', onChangeForm);
    let comentarios = new InputText('comentarios', onChangeForm);
    let btn = new Button('btn-contacto-enviar', onClickBtn);
    let jumbotron = new Container('form-contacto-success');

    return {
        init: init,
    };
})();

let cart = (function () {
    let MONTO_MAXIMO = 35000.0;

    let construct = function (urlBase) {
        list.construct(urlBase);
        totales.construct(urlBase);
    };

    let addStoreListeners = function () {
        store.attachButtonsListeners();
    };

    let refreshViews = function () {
        list.refresh();
        totales.refresh();
        nav.refresh();
    };

    let list = (function () {
        let attachUrl;
        let lastAppendedId = 0;

        let construct = function (urlBase) {
            attachUrl = urlBase + 'cart/ajaxAttachItem';
            items.construct(urlBase);
            loadRemote.construct(urlBase);
            attachRemote.construct(urlBase);
        };

        let refresh = function () {
            if (getListItems().length === 0) {
                container.hide();
                empty.show();
            } else  {
                empty.hide();
                container.show();
            }
        };

        let clear = function () {
            container.clear();
        };

        let getListItems = function () {
            return container.jQSel.children('li');
        };

        let addNewItem = function (id, cantidad) {
            if($('#' + id).length) {
                console.log('El item ya existe en la lista');
                return;
            }
            container.append(items.createLiDom(id, false, 'Cargando item: ' + id, false));
            lastAppendedId = id;

            attachRemote.request(id, cantidad);
        };

        let empty = new Container('cart-list-empty');
        let container = new Container('cart-list');

        let items = (function () {
            let changeUrl;

            let construct = function (urlBase) {
                changeUrl = urlBase + 'cart/ajaxChangeCantidad';
            };

            let createLiDom = function (id, precio, append, dmil) {
                let li = $('<li>')
                    .attr('id', id)
                    .addClass('list-group-item')
                    .addClass('p-1');
                if(precio !== false) { li.attr('data-precio', precio); }
                if (dmil !== false) { li.attr('data-dmil', dmil); }
                if(append !== false) { li.append(append) }

                return li;
            };

            let configureInput = function (id) {
                // Se selecciona específicamente por htmlID porque si se aplica para todos y se duplica .inputSpinner()
                // se corrompen los elementos existentes
                let inputSelector = $('#input-' + id);

                inputSelector.inputSpinner();
                inputSelector.on('change', onInputChange);
            };

            let onInputChange = function (event) {
                //console.log('cart.list.items.onInputChange()');
                let changedInputElement = $(event.target);
                let ajax = new Remote(changeUrl, 'POST', '', onAjaxChangeFails);
                ajax.pushDataItem('id', event.target.id);
                ajax.pushDataItem('cantidad', changedInputElement.val());
                ajax.request();

                let liElement = changedInputElement.parents('li')[0];

                if(changedInputElement.val() === '0' || changedInputElement.val() === ''){
                    //console.log('borrando del carrito in-cart-' + changedInputElement.data('id'));
                    liElement.remove();
                    refreshViews();
                    store.notifyRemovedFromCart(changedInputElement.data('id'));
                    return;
                }

                //let monto = parseFloat($(liElement).data('precio')) * changedInputElement.val();

                let monto_centavos = Math.round(changedInputElement.val() * $(liElement).data('dmil') / 100);
                let iva_centavos = Math.round(monto_centavos * .16);

                $('#monto-' + liElement.id).empty().text(utils.moneyFormat((iva_centavos + monto_centavos) / 100));

                refreshViews();
            };

            let onAjaxChangeFails = function () {
                console.log('cart.list.items.onAjaxChangeFails()');
            };

            return {
                construct: construct,
                createLiDom: createLiDom,
                configureInput: configureInput,
            };
        })();

        let loadRemote = (function () {
            let urlAjax;

            let construct = function (urlBase) {
                urlAjax = urlBase + 'cart/ajaxItems';
                request();
            };

            let request = function () {
                let ajax = new Remote(urlAjax, 'GET', onSuccess, onFails);
                ajax.request();
            };

            let onSuccess = function (data) {
                if (data.length === 0) {
                    empty.show();
                    container.hide();
                    refreshViews();
                    return;
                }
                empty.hide();
                container.clear();
                $.each(data, function (key, value) {
                    container.append(items.createLiDom(value.id, value.precio, value.html, value.diezmilesimos));
                    items.configureInput(value.id);
                });
                refreshViews();
            };

            let onFails = function () {
                console.log('cart.list.loadRemote.onFails()');
            };

            return {
                construct: construct,
            }
        })();

        let attachRemote = (function () {
            let urlAjax;
            let lastId;

            let construct = function (urlBase) {
                urlAjax = urlBase + 'cart/ajaxAttachItem';
            };

            let request = function (id, cantidad) {
                lastId = id;
                let ajax = new Remote(urlAjax, 'POST', onSuccess, onFails);
                ajax.pushDataItem('hexProductoId', id);
                ajax.pushDataItem('cantidad', cantidad);
                ajax.request();
            };

            let onSuccess = function (data) {
                let li = new Container(lastId);
                li.replace(data.html);
                li.jQSel.attr('data-precio', data.precio);
                li.jQSel.attr('data-dmil', data.diezmilesimos);
                items.configureInput(lastId);
                refreshViews();
            };

            let onFails = function () {
                console.log('cart.list.attachRemote.onFails()');
            };

            return {
                construct: construct,
                request: request,
            };
        })();

        return {
            construct: construct,
            refresh: refresh,
            getListItems: getListItems,
            addNewItem: addNewItem,
            clear: clear,
        };
    })();

    let totales = (function () {
        let vaciarUrl;

        let construct = function (urlBase) {
            vaciarUrl = urlBase + 'cart/ajaxClear';
        };

        let refresh = function () {
            let children = list.getListItems();
            if (children.length === 0) {
                container.hide();
                return;
            }

            let monto_centavos;
            let total = 0.0;
            $.each(children, function(key, value) {

                //total += parseFloat($(value).data('precio')) * parseInt($('#input-' + value.id).val());

                monto_centavos = Math.round(parseFloat($('#input-' + value.id).val()) * parseFloat($(value).data('dmil')) / 100);
                total += (monto_centavos + Math.round(monto_centavos * 0.16)) / 100;
            });

            if (total > 35000) {
                estimacion.hide();
                linkFinalizar.hide();
                limitAlert.show();
            } else if (total <= 0) {
                linkFinalizar.hide();
            } else {
                limitAlert.hide();
                estimacion.show();
                linkFinalizar.show();
            }

            monto.replace(utils.moneyFormat(total));
            container.show();
        };

        let onClickVaciar = function () {
            let ajax = new Remote(vaciarUrl, 'POST', '', onVaciarFails);
            ajax.request();
            list.clear();
            refreshViews();
            store.notifyClearingCart();
        };

        let onVaciarFails = function () {
            console.log('cart.totales.onVaciarFails()');
        };

        let container = new Container('cart-list-footer');
        let monto = new Container('cart-list-total');
        let estimacion = new Container('cart-estimacion-entrega');
        let limitAlert = new Container('cart-limit-alert');
        let linkFinalizar = new Container('cart-link-finalizar');
        /*let btnVaciar =*/ new Button('cart-btn-vaciar', onClickVaciar);

        return {
            construct: construct,
            refresh: refresh,
        };
    })();

    let nav = (function () {

        let refresh = function () {
            container.replace(list.getListItems().length);
        };

        let container = new Container('cart-items-count');

        return {
            refresh: refresh,
        }
    })();

    let store = (function () {
        let attachButtonsListeners = function () {
            $('.add-to-cart').click(btnAddClick);
            configureItemInput();
        };

        let btnAddClick = function (event) {
            let clickedButton = $(event.target);
            //list.addNewItem(clickedButton.data('id'), clickedButton.data('cantidad'));
            list.addNewItem(clickedButton.attr('data-id'), clickedButton.attr('data-cantidad'));
            items.notifyAddedToCart(clickedButton.parents('div')[0]);
        };

        let configureItemInput = function () {
            let inputSelector = $('.add-to-cart-input');
            inputSelector.inputSpinner();
            inputSelector.on('input', onItemInputChange);
        };

        let onItemInputChange = function (event) {
            let inputElement = $(event.target);
            /*if(isNaN(parseInt(inputElement.val()))){
                inputElement.val(1);
            }*/
            $('#' + inputElement.data('btnid')).attr('data-cantidad', inputElement.val());
        };

        let items = (function () {

            let notifyClearingCart = function () {
                $('.in-cart').addClass('d-none');
                $('.not-in-cart').removeClass('d-none');
            };

            let notifyAddedToCart = function (item) {
                let notInCart = $(item);
                notInCart.addClass('d-none');
                let inCart = new Container(notInCart.data('partner'));
                inCart.show();
                //console.log(item);
            };

            let notifyRemovedFromCart = function (id) {
                let notInCart = new Container('not-in-cart-' + id);
                notInCart.show();
                let inCart = new Container(notInCart.jQSel.data('partner'));
                inCart.hide();
            };

            return {
                notifyClearingCart: notifyClearingCart,
                notifyAddedToCart: notifyAddedToCart,
                notifyRemovedFromCart: notifyRemovedFromCart,
            }
        })();

        return {
            attachButtonsListeners: attachButtonsListeners,
            notifyClearingCart: items.notifyClearingCart,
            notifyRemovedFromCart: items.notifyRemovedFromCart,
        };
    })();

    return {
        construct: construct,
        addStoreListeners: addStoreListeners,
    }
})();

let pago = (function () {
    let urlTienda;
    let urlSuccess;

    let construct = function (urlBase) {
        urlTienda = urlBase + 'tienda';
        urlSuccess = urlBase + 'pedidos/pedido/';
        facturacion.construct(urlBase);
        banco.construct(urlBase);
        tienda.construct(urlBase);
        tarjeta.construct(urlBase);
    };

    /*let metodoEntrega =*/ (function () {
        let onClickToggle = function () {
            //console.log($(this).children()[0]);
            let icon = $(this).children().first();
            if (icon.hasClass('oi-chevron-top')) {
                icon.removeClass('oi-chevron-top').addClass('oi-chevron-bottom');
            } else {
                icon.removeClass('oi-chevron-bottom').addClass('oi-chevron-top');
            }
        };

        /*let toggleMetodoEntrega =*/ new Button('toggle-metodo-entrega', onClickToggle);
    })();

    let facturacion = (function () {
        let construct = function (urlBase) {
            cardBody.construct(urlBase);
        };

        let getData = function () {
            if (checkbox.isChecked()) {
                return {
                    requiereFactura: 1,
                    idRfc: cardBody.getIdRfc(),
                    usoCfdi: cardBody.getUsoCfdiVal(),
                }
            }
            return {
                requiereFactura: 0,
                idRfc: 0,
                usoCfdi: 'G03',
            }
        };

        let validate = function () {
            let isValid = true;
            if (checkbox.isChecked()) {
                isValid = cardBody.validate();
            }

            if (isValid) {
                formaPago.container.show();
            } else {
                formaPago.container.hide();
            }
        };

        let requiereFactura = function () {
            checkbox.check();
        };

        let onChangeCheckbox = function () {
            validate();
            if (checkbox.isChecked()) {
                cardBody.container.show();
            } else {
                cardBody.container.hide();
            }
        };

        let checkbox = new InputCheckbox('requiere-factura', onChangeCheckbox);

        let cardBody = (function () {
            let construct = function (urlBase) {
                form.construct(urlBase);

                let defaultRfcId = container.getDataValue('selected'); // could be 'undefined' and it's right
                if (typeof defaultRfcId !== "undefined") {
                    facturacion.requiereFactura();
                    container.show();
                    form.setDefaultRfc(defaultRfcId);
                }
            };

            let getIdRfc = function () {
                return form.getIdRfc();
            };

            let getUsoCfdiVal = function () {
                return form.getUsoCfdiVal();
            };

            let validate = function () {
                return form.validate();
            };

            let container = new BsCollapse('facturacion-body');
            
            let form = (function () { // row1 controls all the form
                let construct = function (urlBase) {
                    guardar.construct(urlBase);
                    informe.construct(urlBase);
                };

                let validate = function () {
                    let selValue = select.getSelectedValue();

                    return !(selValue === '0' || selValue === '1');
                };

                let getIdRfc = function () {
                    return select.getSelectedValue();
                };

                let getUsoCfdiVal = function () {
                    return informe.getUsocfdiVal();
                };

                let setDefaultRfc = function (rfcId) {
                    select.selectOption(rfcId);
                    informe.show(rfcId);
                    //console.log('set default ' + rfcId);
                };

                let onSelectChange = function () {
                    facturacion.validate();
                    let value = $(this).val();
                    if (value === '0') {
                        stage2.hide();
                        guardar.hide();
                        informe.hide()
                    } else if (value === '1') {
                        stage2.show();
                        guardar.show();
                        informe.hide();
                    } else {
                        stage2.hide();
                        guardar.hide();
                        informe.show(value);
                    }
                };

                //let container = new ElementBase('row1');

                let select = new InputSelect('select1', onSelectChange);

                let stage2 = (function () {

                    /**
                     * Function to be called from inside row2, row3, etc
                     */
                    let validateForm = function () {
                        validate();
                    };

                    let validate = function () {
                        let row2_ok = row2.validate();
                        let row3_ok = row3.validate();
                        let stage3_ok = stage3.validate();
                        let stage4_ok = stage4.validate();

                        if (row2_ok && row3_ok && (stage3_ok || stage4_ok)) {
                            guardar.enable();
                        } else {
                            guardar.disable();
                        }
                    };

                    let show = function () {
                        row2.show();
                        //row2.validateForm = callbackValidate;
                        //console.log(row2.validateForm);
                        row3.show();
                    };

                    let hide = function () {
                        row2.hide();
                        row3.hide();
                        stage3.hide();
                        stage4.hide();
                    };

                    let getFormData = function () {
                        //let formData = {};
                        let formData = row2.getFormData({});
                        formData = row3.getFormData(formData);
                        formData = stage3.getFormData(formData);
                        formData = stage4.getFormData(formData);

                        return formData;
                    };

                    let row2 = (function () {

                        let validate = function () {
                            let globals = new Globals();
                            let rfc_ok = rfc.validateRegex(globals.rfcPattern);
                            let razonsocial_ok = razonsocial.validateRegex(globals.atLeastTwoChars);
                            let cp_ok = cp.validateRegex(globals.cpPattern);
                            return rfc_ok && razonsocial_ok && cp_ok;
                        };

                        let onRfcInput = function () {
                            rfc.toUpper();
                            validateForm();
                        };

                        let onRazonsocialInput = function () {
                            facturacion.validate();
                            validateForm();
                        };

                        let onCpInput = function () {
                            let globals = new Globals();
                            cp.forceRegexMatch(globals.forceCp);
                            if (cp.validateRegex(globals.cpPattern)) {
                                cps.ajaxCp.request(cp.getVal(), onCpAjaxSuccess, onCpAjaxError);
                            } else {
                                stage3.hide();
                                stage4.hide();
                            }
                            validateForm();
                        };

                        let onCpAjaxSuccess = function (data) {
                            lastRequestedSuccess = true;
                            stage3.hide();
                            stage4.row5.ingestDataFromAjax(data);
                            stage4.show();
                        };

                        let onCpAjaxError = function () {
                            lastRequestedSuccess = false;
                            stage3.show();
                            stage4.hide();
                        };

                        let show = function () {
                            container.show();
                        };

                        let hide = function () {
                            container.hide();
                            rfc.empty();
                            razonsocial.empty();
                            cp.empty();
                            lastRequestedCp = '';
                        };

                        let getFormData = function (formData) {
                            formData.rfc = rfc.getVal();
                            formData.razonsocial = razonsocial.getVal();
                            formData.cp = cp.getVal();

                            return formData;
                        };

                        let container = new Container('row2');

                        let rfc = new InputText('rfc', onRfcInput);

                        let razonsocial = new InputText('razonsocial', onRazonsocialInput);

                        let cp = new InputText('fac_cp', onCpInput);

                        return {
                            show: show,
                            hide: hide,
                            validate: validate,
                            getFormData: getFormData,
                        };
                    })();

                    let row3 = (function () {

                        let validate = function () {
                            let globals = new Globals();
                            let calle_ok = calle.validateRegex(globals.atLeastTwoChars);
                            let numext_ok = numext.validateRegex(globals.atLeastOneChar);
                            return calle_ok && numext_ok;
                        };

                        let onCalleInput = function () {
                            validateForm()
                        };

                        let onNumextInput = function () {
                            validateForm();
                        };

                        let show = function () {
                            container.show();
                        };

                        let hide = function () {
                            container.hide();
                            calle.empty();
                            numext.empty();
                            numint.empty();
                        };

                        let getFormData = function (formData) {
                            formData.calle = calle.getVal();
                            formData.numext = numext.getVal();
                            formData.numint = numint.getVal();
                            return formData;
                        };

                        let container = new Container('row3');

                        let calle = new InputText('calle', onCalleInput);

                        let numext = new InputText('numext', onNumextInput);

                        let numint = new InputText('numint');

                        return {
                            show: show,
                            hide: hide,
                            validate: validate,
                            getFormData: getFormData,
                        };
                    })();

                    let stage3 = (function () {

                        let validate = function () {
                            let globals = new Globals();
                            let colonia_ok = colonia4.validateRegex(globals.atLeastTwoChars);
                            let estado_ok = estado4.validateNotEmptyOption();
                            let ciudad_ok = ciudad4.validateNotEmptyOption();

                            return colonia_ok && estado_ok && ciudad_ok;
                        };

                        let show = function () {
                            container.show();
                        };

                        let hide = function () {
                            container.hide();
                            colonia4.empty();
                            estado4.selectOption('0');
                            ciudad4.removeOptions('...Selecciona...');
                            ciudad4.disable();
                        };

                        let onColonia4Input = function () {
                            validateForm();
                        };

                        let onEstado4Change = function () {
                            cps.ajaxCiudades.request(estado4.getSelectedValue(), onCiudadesAjaxSuccess,
                                onCiudadesAjaxError);

                            validateForm();
                        };

                        let onCiudad4Change = function () {
                            validateForm();
                        };

                        let onCiudadesAjaxSuccess = function (data) {
                            ciudad4.removeOptions('...Selecciona...');
                            ciudad4.appendOptionsFromAjaxData(data);
                            ciudad4.enable();
                        };

                        let onCiudadesAjaxError = function () {
                            ciudad4.removeOptions('...Selecciona...');
                            ciudad4.disable();
                        };

                        let getFormData = function (formData) {
                            formData.colonia4 = colonia4.getVal();
                            formData.estado4 = estado4.getSelectedValue();
                            formData.ciudad4 = ciudad4.getSelectedValue();
                            return formData;
                        };

                        let container = new Container('row4');

                        let colonia4 = new InputText('colonia4', onColonia4Input);

                        let estado4 = new InputSelect('estado4', onEstado4Change);

                        let ciudad4 = new InputSelect('ciudad4', onCiudad4Change);

                        return {
                            show: show,
                            hide: hide,
                            validate: validate,
                            getFormData: getFormData,
                        }
                    })();

                    let stage4 = (function () {

                        let validate = function () {
                            let row5_ok = row5.validate();
                            let row6_ok = row6.validate();
                            return row5_ok || row6_ok; // Con uno de los dos que sea válido es suficiente
                        };

                        let show = function () {
                            row5.show();
                            row6.show();
                        };

                        let hide = function() {
                            row5.hide();
                            row6.hide();
                        };

                        let getFormData = function(formData) {
                            formData = row5.getFormData(formData);
                            formData = row6.getFormData(formData);

                            return formData;
                        };

                        let row5 = (function () {
                            let validate = function () {
                                return colonia5.validateNotEmptyOption();
                            };

                            let show = function () {
                                container.show();
                            };

                            let hide = function () {
                                container.hide();
                                colonia5.removeOptions('...Selecciona la Colonia...');
                                colonia5.enable();
                                ciudad5.empty();
                                estado5.empty();
                            };

                            let ingestDataFromAjax = function (data) {
                                // noinspection JSUnresolvedVariable
                                colonia5.appendOptionsFromAjaxDataV2(data.colonias);
                                ciudad5.setVal(data.ciudad.caption);
                                estado5.setVal(data.estado.caption);
                            };

                            let onColonia5Change = function () {
                                if (colonia5.validateNotEmptyOption()) {
                                    row6.hide();
                                } else {
                                    row6.show();
                                }

                                validateForm();
                            };

                            let getFormData = function (formData) {
                                formData.colonia5 = colonia5.getSelectedValue();

                                return formData;
                            };

                            let container = new Container('row5');

                            let colonia5 = new InputSelect('colonia5', onColonia5Change);

                            let ciudad5 = new InputText('ciudad5');

                            let estado5 = new InputText('estado5');

                            return {
                                show: show,
                                hide: hide,
                                validate: validate,
                                ingestDataFromAjax: ingestDataFromAjax,
                                colonia5: colonia5,
                                getFormData: getFormData,
                            };
                        })();

                        let row6 = (function () {
                            let validate = function () {
                                return colonia6.validateRegex(new Globals().atLeastTwoChars);
                            };

                            let show = function () {
                                container.show();
                            };

                            let hide = function() {
                                container.hide();
                                colonia6.empty();
                            };

                            let onColonia6Input = function () {
                                if (colonia6.validateRegex(new Globals().atLeastTwoChars)) {
                                //if (validate()) { // No se sustituye por validate() para evitar problemas si se
                                    // tuviera que agregar otro form-element
                                    row5.colonia5.disable();
                                } else {
                                    row5.colonia5.enable();
                                }

                                validateForm();
                            };

                            let getFormData = function (formData) {
                                formData.colonia6 = colonia6.getVal();

                                return formData;
                            };

                            let container = new Container('row6');

                            let colonia6 = new InputText('colonia6', onColonia6Input);

                            return {
                                show: show,
                                hide: hide,
                                validate: validate,
                                getFormData: getFormData,
                            };
                        })();

                        return {
                            show: show,
                            hide: hide,
                            validate: validate,
                            row5: row5,
                            getFormData: getFormData,
                        };
                    })();

                    return {
                        show: show,
                        hide: hide,
                        getFormData: getFormData,
                    };
                })();
                
                let guardar = (function () {
                    let urlHome;
                    let urlAjax;

                    let construct = function (urlBase) {
                        urlHome = urlBase;
                        urlAjax = urlBase + 'orden/ajaxNewRfc';
                    };

                    let show = function () {
                        container.show();
                    };

                    let hide = function() {
                        container.hide();
                    };

                    let enable = function () {
                        button.enable();
                    };

                    let disable = function () {
                        button.disable();
                    };

                    let onButtonClick = function () {
                        //console.log('Ajax url...' + urlAjax);
                        let remote = new Remote(urlAjax, 'POST', onSuccessAjax, onErrorAjax);
                        remote.setDataObject(stage2.getFormData());
                        remote.request();
                    };

                    let onSuccessAjax = function (data) {
                        // noinspection JSUnresolvedVariable
                        window.location.replace(urlHome + "orden/pago/" + data.newRfcId);
                        //console.log('Success ajax');
                    };

                    let onErrorAjax = function () {
                        window.location.replace(urlHome);
                    };

                    let container = new Container('row7');

                    let button = new Button('facturacion-guardar', onButtonClick);

                    return {
                        construct: construct,
                        show: show,
                        hide: hide,
                        enable: enable,
                        disable: disable,
                    };
                })();

                let informe = (function () {
                    let urlHome;
                    let urlAjax;
                    let usocfdi;

                    let construct = function (urlBase) {
                        urlHome = urlBase;
                        urlAjax = urlBase + "orden/ajaxRfc/";
                    };

                    let show = function (rfcId) {
                        let remote = new Remote(urlAjax + rfcId, 'GET', onAjaxSuccess, onAjaxError);
                        remote.request();
                        //container.show();
                    };

                    let hide = function() {
                        container.hide();
                        container.clear()
                    };

                    let getUsoCfdiVal = function () {
                        return usocfdi.getSelectedValue();
                    };

                    let onAjaxSuccess = function (data) {
                        // noinspection JSUnresolvedVariable
                        container.replace(data.payload);
                        usocfdi = new InputSelect('usocfdi');
                        container.show();
                    };

                    let onAjaxError = function () {
                        console.log('ajaxRfc error');
                        //window.location.replace(urlHome);
                    };

                    let container = new Container('row8');

                    return {
                        construct: construct,
                        show: show,
                        hide: hide,
                        getUsocfdiVal: getUsoCfdiVal,
                    };
                })();

                return {
                    construct: construct,
                    validate: validate,
                    setDefaultRfc: setDefaultRfc,
                    getIdRfc: getIdRfc,
                    getUsoCfdiVal: getUsoCfdiVal,
                }
            })();

            return {
                construct: construct,
                container: container,
                validate: validate,
                getIdRfc: getIdRfc,
                getUsoCfdiVal: getUsoCfdiVal,
            };
        })();

        return {
            construct: construct,
            validate: validate,
            requiereFactura: requiereFactura,
            getData: getData,
        };
    })();

    let formaPago = (function () {

        let container = new Container('forma-de-pago');

        return {
            container: container,
        }
    })();

    let tarjeta = (function () {
        let urlAjax = '';
        let url3DForm = '';

        let construct = function (urlBase) {
            //urlAjax = urlBase + 'orden/ajaxConfirmarTarjeta';
            urlAjax = urlBase + 'orden/ajaxConfirmar3DSecure';
            url3DForm = urlBase + 'payment/tcform/';
        };

        let onClickConfirmar = function () {
            btnConfirmar.changeCaption('Procesando...').disable();
            ajax = new Remote(urlAjax, 'POST', onAjaxSuccess3D, onAjaxFails);
            ajax.setDataObject(facturacion.getData());
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                // noinspection JSUnresolvedVariable
                window.location = data.urlOpenpay;
                return;
            }
            window.location = urlTienda;
        };

        let onAjaxSuccess3D = function (data) {
            if (data.success) {
                // noinspection JSUnresolvedVariable
                window.location = url3DForm + data.payload;
                return;
            }
            console.log(data);
            window.location = urlTienda;
        };

        let onAjaxFails = function () {
            window.location = urlTienda;
        };

        let btnConfirmar = new Button('confirmar-tarjeta', onClickConfirmar);

        return {
            construct: construct,
        };
    })();

    let banco = (function () {
        let urlAjax = '';
        //let urlSuccess = '';

        let construct = function (urlBase) {
            urlAjax = urlBase + 'orden/ajaxConfirmarBanco';
            //urlSuccess = urlBase + 'pedidos/cargo/';
            //urlSuccess = urlBase + 'pedidos/pedido/';
        };

        let onClickConfirmar = function () {
            btnConfirmar.changeCaption('Procesando...').disable();
            ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, onAjaxFails);
            ajax.setDataObject(facturacion.getData());
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            //return;
            if (data.success) {
                // noinspection JSUnresolvedVariable
                window.location = urlSuccess + data.pedido;
                return;
            }
            window.location = urlTienda;
        };

        let onAjaxFails = function () {
            window.location = urlTienda;
        };

        let btnConfirmar = new Button('confirmar-banco', onClickConfirmar);

        return {
            construct: construct,
        };
    })();

    let tienda = (function () {
        let urlAjax = '';
        //let urlSuccess = '';

        let construct = function (urlBase) {
            urlAjax = urlBase + 'orden/ajaxConfirmarTienda';
            //urlSuccess = urlBase + 'pedidos/pedido/';
        };

        let onClickConfirmar = function () {
            btnConfirmar.changeCaption('Procesando...').disable();
            ajax = new Remote(urlAjax, 'POST', onAjaxSuccess);
            ajax.setDataObject(facturacion.getData());
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                // noinspection JSUnresolvedVariable
                window.location = urlSuccess + data.pedido;
                return;
            }
            window.location.replace(urlTienda);
        };

        let onAjaxFails = function () {
            window.location = urlTienda;
        };

        let btnConfirmar = new Button('confirmar-tienda', onClickConfirmar);

        return {
            construct: construct,
        };
    })();

    return {
        construct: construct,
    };
})();

let copiarPedido = (function () {
    let urlAjax;
    let urlOrden;

    let init = function (urlBase) {
        urlAjax = urlBase + 'cart/ajaxCopiarPedido/';
        urlOrden = urlBase + 'orden';
        //$('.copy-to-cart').click(onClickLink);
        $('.btn-copiar-pedido').click(onClickLink);
    };

    let onClickLink = function (event) {
        event.preventDefault();
        //let ajax = new Remote(urlAjax + $(this).data('pedidoid'), 'POST', onAjaxSuccess);
        let ajax = new Remote(urlAjax + $(this).data('id'), 'POST', onAjaxSuccess);
        ajax.request();
    };

    let onAjaxSuccess = function () {
        window.location.replace(urlOrden);
    };

    return {
        init: init,
    };
})();

let copiarCotizacion = (function () {
    let urlAjax;
    let urlOrden;

    let init = function (urlBase) {
        urlAjax = urlBase + 'cart/ajaxCopiarCotizacion/';
        urlOrden = urlBase + 'orden';
        $('.btn-ordenar-pedido').click(onClickLink);
    };

    let onClickLink = function (event) {
        //event.preventDefault();
        let ajax = new Remote(urlAjax + $(event.currentTarget).data('id'), 'POST', onAjaxSuccess);
        ajax.request();
    };

    let onAjaxSuccess = function () {
        window.location.replace(urlOrden);
    };

    return {
        init: init,
    };
})();

let forgot = (function () {
    let urlAjax;

    let construct = function (urlBase) {
        urlAjax = urlBase + 'identity/ajaxForgot';
    };

    let onEmailInput = function () {
        if (email.validateRegex(new Globals().emailPattern)) {
            button.enable();
        } else {
            button.disable();
        }
    };

    let onButtonClick = function () {
        let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, onAjaxFails);
        ajax.pushDataItem('email', email.getVal());
        ajax.request();
        button.disable();
    };

    let onAjaxSuccess = function () {
        form.hide();
        //button.disable();
        successAlert.jQSel.parent().removeClass('d-none');
    };

    let onAjaxFails = function () {
        form.hide();
        //button.disable();
        errorAlert.jQSel.parent().removeClass('d-none');
    };

    let form = new Container('form-forgot');
    let errorAlert = new Container('forgot-error');
    let successAlert = new Container('forgot-success');
    let email = new InputText('forgot-email', onEmailInput);
    let button = new Button('forgot-btn', onButtonClick);

    return {
        construct: construct,
    };
})();

let recover = (function () {
    let urlAjax;

    let construct = function (urlBase) {
        urlAjax = urlBase + 'identity/ajaxRecover';
    };

    let onFormInput = function () {
        let pass_ok = newPass.validateRegex(new Globals().passwordPattern);
        let confirm_ok = confirm.validateIdentical(newPass.getVal());
        if (pass_ok && confirm_ok) {
            button.enable();
        } else {
            button.disable();
        }
    };

    let onButtonClick = function () {
        let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, onAjaxFails);
        ajax.pushDataItem('pass', newPass.getVal());
        ajax.pushDataItem('token', button.jQSel.data('token'));
        ajax.request();
    };

    let onAjaxSuccess = function () {
        form.hide();
        successAlert.jQSel.parent().removeClass('d-none');
    };

    let onAjaxFails = function () {
        form.hide();
        errorAlert.jQSel.parent().removeClass('d-none');
    };

    let form = new Container('form-recover');
    let errorAlert = new Container('recover-error');
    let successAlert = new Container('recover-success');
    let newPass = new InputText('recover-new', onFormInput);
    let confirm = new InputText('recover-confirm', onFormInput);
    let button = new Button('recover-btn', onButtonClick);

    return {
        construct: construct,
    };
})();

let polygons = (function () {
    let thisUrlBase;
    let remotePolygons;

    let init = function (urlBase) {
        thisUrlBase = urlBase ;
        let ajax = new Remote(urlBase + 'maps/ajaxPolygons', 'GET', onSuccessAjax);
        ajax.request();
        //displayMap();
    };

    let onSuccessAjax = function (data) {
        remotePolygons = data.payload;
        displayMap();
    };

    let onSuccessRelatedAjax = function(data) {
        gmap.updatePolygons(data.payload);
    };

    let onMapClicked = function (lat, lng) {
      console.log('location ' + lat + ' ' + lng);
      let ajax = new Remote(thisUrlBase + 'maps/ajaxRelatedPolygons', 'POST', onSuccessRelatedAjax);
      ajax.pushDataItem('lat', lat);
      ajax.pushDataItem('lng', lng);
      ajax.request();
    };

    let displayMap = function () {
        if (typeof google === 'undefined') {
            console.log('no se ha cargado google maps js');
            setTimeout(displayMap, 1000);
            return;
        }
        gmap.drawPolygons('map', 25.671389, -100.308611, remotePolygons, onMapClicked);
    };

    return {
        init: init,
    };
})();

let gmap = (function () {
    let polygonsZoom = 10;
    let htmlIdContainer;
    let container;
    let map;
    let marker;
    let thisCallbackLocation;
    let lat;
    let lng;
    let alt = 0;
    let zoom = 19;
    let MTY_BOUNDS = {
        north: 25.998013,
        south: 25.244696,
        west: -100.949936,
        east: -99.808731,
    };

    //let reintentos = 0;
    let isDefault = false;

    let drawPolygons = function (htmlId, latCenter, lngCenter, polygons, callbackLocation) {
        thisCallbackLocation = callbackLocation;
        displayByCoords(htmlId, latCenter, lngCenter);

        domPolygons.draw(polygons);
    };

    let updatePolygons = function (polygons) {
        domPolygons.draw(polygons);
    };

    let displayByCoords = function (htmlId, lat, lng) {
        htmlIdContainer = htmlId;
        container = document.getElementById(htmlId);
        map = new google.maps.Map(container, {
            center:{lat: lat, lng: lng},
            zoom: polygonsZoom,
        });
        map.addListener('click', onClickMapPolygons);
    };

    let displayByAddress2 = function (htmlId, addressObj, callbackLocation) {
        console.log(addressObj);
        isDefault = false;
        htmlIdContainer = htmlId;
        thisCallbackLocation = callbackLocation;

        geocoder = new google.maps.Geocoder();

        if (typeof addressObj.streetAddr === 'undefined') {
            onCoordsProvided(addressObj.lat, addressObj.lng, zoom);
        } else {
            geocoder.geocode({
                address: addressObj.streetAddr,
                bounds: MTY_BOUNDS,
                /*bounds: new google.maps.LatLngBounds(
                    new google.maps.LatLng(25.244696, -100.949936),
                    new google.maps.LatLng(25.998013, -99.808731)
                ),*/
                componentRestrictions: {
                    //country: 'MX',
                    locality: addressObj.colonia,
                    //administrativeArea: addressObj.municipio,
                    //postalCode: addressObj.cp,
                },
                region: 'MX',
            }, onGeocodeResult);
        }
    };

    let displayByAddress = function (htmlId, address, callbackLocation) {
        thisCallbackLocation = callbackLocation;
        container = document.getElementById(htmlId);

        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            address: address,
            region: 'MX',
        }, onGeocodeResult);
    };

    let displayDefault = function () {
        isDefault = true;
        let mtyCenter = {streetAddr: 'zaragoza 1000 in 64000', colonia: 'Monterrey Centro'};

        geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            address: mtyCenter.streetAddr,
            componentRestrictions: {
                locality: mtyCenter.colonia,
            },
            region: 'MX',
        }, onGeocodeResult);
    };

    let onClickMapPolygons = function (event) {
        domPolygons.clear();
        thisCallbackLocation(event.latLng.lat(), event.latLng.lng());
    };

    let onClickMapAddress = function (event) {
        let location = event.latLng;
        marker.setPosition(event.latLng);
        map.panTo(location);
        lat = location.lat();
        lng = location.lng();
        reportLocation();
    };

    let reportLocation = function () {
        thisCallbackLocation(lat, lng, alt);
    };

    let onGeocodeResult = function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            let location = results[0].geometry.location;
            lat = location.lat();
            lng = location.lng();
            reportLocation();

            requestElevation(location);

            let esteZoom;
            if (isDefault) {
                esteZoom = 12;
                //reintentos = 0;
            } else {
                esteZoom = zoom;
            }
            onCoordsProvided(location.lat(), location.lng(), esteZoom);
        } else {
            console.log('Geocode no pudo localizar la dirección: ' + status);
            if (!isDefault) {
                displayDefault();
            }
        }
    };

    let onCoordsProvided = function (centerLat, centerLng, mapZoom) {
        let center = {
            lat: centerLat,
            lng: centerLng,
        };
        map = new google.maps.Map(requireContainer(), {
            center: center,
            zoom: mapZoom,
            restriction: {
                latLngBounds: MTY_BOUNDS,
                strictBounds: false,
            },
        });
        map.addListener('click', onClickMapAddress);
        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: center,
        });
        onMarkerDrag(marker);
    };

    let onMarkerDrag = function (marker) {
        google.maps.event.addListener(marker, 'dragend', function (event) {
            let location = event.latLng;
            map.panTo(location);
            lat = location.lat();
            lng = location.lng();
            reportLocation();
        })
    };

    let requestElevation = function (location) {
        let locations = {locations: [location]};

        let elevationService = new google.maps.ElevationService();
        elevationService.getElevationForLocations(locations, function(results, status) {
            if (status === google.maps.ElevationStatus.OK) {
                if (results[0]) {
                    //console.log('resultado' + results[0].elevation.toFixed(5));
                    alt = results[0].elevation.toFixed(5);
                    reportLocation();
                }
            } else {
                console.log('Elevation was not successful ' + status);
            }
        });
    };

    let requireContainer = function () {
        //console.log(typeof container);
        if (typeof container === "undefined") {
            container = document.getElementById(htmlIdContainer);
        }

        return container;
    };

    let domPolygons = (function () {
        let items = [];

        let draw = function (polygons) {
            polygons.forEach(function (polygon) {
                let googlePolygon = new google.maps.Polygon({
                    paths: polygon.paths,
                    strokeColor: '#' + polygon.color,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#' + polygon.color,
                    fillOpacity: 0.35
                });
                googlePolygon.setMap(map);
                googlePolygon.addListener('click', onClickMapPolygons);
                items.push(googlePolygon);
            });
            console.log(items);
        };

        let clear = function () {
            items.forEach(function (item) {
                item.setMap(null);
            });
            items = [];
        };

        return {
            draw: draw,
            clear: clear,
        };
    })();

    return {
        display2: displayByAddress2,
        display: displayByAddress,
        drawPolygons: drawPolygons,
        updatePolygons: updatePolygons,
    };
})();

let keepalive = (function () {
    let FIRST_INTERVAL_MILLISECS = 240000; // 4 mins
    let INTERVAL_MILLISECS = 300000; // 5 mins
    let urlAjax;

    let construct = function (urlBase) {
        urlAjax = urlBase + 'index/ajaxKeepalive';
        setTimeout(ping, FIRST_INTERVAL_MILLISECS);
    };

    let ping = function () {
        let ajax = new Remote(urlAjax, 'POST', onAjaxFinish, onAjaxFinish);
        ajax.request();
    };

    let onAjaxFinish = function () {
        setTimeout(ping, INTERVAL_MILLISECS);
    };

    return {
        construct: construct,
    };
})();
