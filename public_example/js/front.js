
var front = {

    urlBase: '/',
    tooltipSelector: $('[data-toggle="tooltip"]'),
    logoutSelector: $('#logout'),
    searchInputSelector: $('#search-input'),
    //paginadorBodySelector: $('#paginadorBody'),
    cartListSelector: $('#cart-list'),
    isProd: false,

    onReady: function(urlBase, uri, afterReadyCommands) {
        //console.log(afterReadyCommands);
        if (afterReadyCommands.isprod === true) {
            front.isProd = true;
        }

        front.urlBase = urlBase;
        front.start(uri);
        front.tooltipSelector.tooltip({trigger: 'hover'});

        if (afterReadyCommands.sucursales === true) {
            eligeTienda.modalSelector.modal('show');
        }
        if (afterReadyCommands.keepalive === true) {
            keepalive.construct(urlBase);
        }
    },

    start: function(uri) {

        eligeTienda.start(front.urlBase); // Siempre se inicia porque está en todas las páginas
        eligeSubclase.construct(front.urlBase);
        buscador.construct(front.urlBase);


        switch(uri) {
            case 'index/index':
                cart.addStoreListeners();
                paginator.construct(front.urlBase, cart.addStoreListeners, undefined, true);
                accordion.construct();
                cart.construct(front.urlBase);
                break;
            case 'index/contacto':
                contacto.init(urlBase);
                break;
            case 'identity/login':
                login.start(front.urlBase);
                break;
            case 'identity/registration':
                identity.construct(front.urlBase, uri);
                //registration.start(front.urlBase);
                break;
            case 'identity/forgot':
                forgot.construct(front.urlBase);
                break;
            case 'identity/recover':
                recover.construct(front.urlBase);
                break;
            case 'tienda/index':
                //paginator.construct(front.urlBase, cart.store.attachButtonsListeners);
                paginator.construct(front.urlBase, cart.addStoreListeners);
                accordion.construct();
                break;
            case 'tienda/detalle':
                //cart.store.attachButtonsListeners();
                cart.addStoreListeners();
                //paginator.construct(front.urlBase, cart.store.attachButtonsListeners, true);
                paginator.construct(front.urlBase, cart.addStoreListeners, true);
                //breadcrumb.init();
                accordion.construct();
                break;
            case 'cuenta/changePass':
                changePass.start(front.urlBase);
                break;
            case 'cuenta/proyectos':
                cps.start(front.urlBase);
                nuevoProyecto.start(front.urlBase);
                proyectos.start(front.urlBase);
                break;
            case 'cuenta/proyecto':
                contactos.start(front.urlBase);
                horarios.start(front.urlBase);
                break;
            case 'proyectos/index':
                projects.construct(front.urlBase, uri);
                break;
            case 'proyectos/nuevo':
                projects.construct(front.urlBase, uri);
                break;
            case 'proyectos/editar':
                projects.construct(front.urlBase, uri);
                break;
            case 'orden/index':
                orden.construct(front.urlBase, uri);
                break;
            case 'orden/pago':
                cps.start(front.urlBase);
                pedido.start(front.urlBase);
                pago.construct(front.urlBase);
                break;
            case 'payment/tcform':
                payment.construct(front.urlBase, front.isProd);
                break;
            case 'pedidos/index':
            case 'pedidos/pedido':
                pedidos.construct(front.urlBase, uri);
                //copiarPedido.init(front.urlBase);
                break;
            case 'cotizaciones/index':
                copiarCotizacion.init(front.urlBase);
                break;
            case 'maps/index':
                polygons.init(front.urlBase);
                break;
        }

        if(front.logoutSelector.length) { logout.start(front.urlBase); }
        if(front.searchInputSelector.length) { search.start(front.urlBase); }
        if(front.cartListSelector.length) {
            cart.construct(front.urlBase);
        }
    },

};
