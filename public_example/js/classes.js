
var login = {
    urlAjax: '/',
    urlBase: '/',
    emailSelector: $('#email'),
    passSelector: $('#password'),
    btnSelector: $('#loginBtn'),
    errorSelector: $('#loginError'),
    keySelector: $('#nonceKey'),
    valueSelector: $('#nonceValue'),

    start: function(urlBase) {
        login.urlAjax = urlBase + 'identity/ajaxLogin';
        login.urlBase = urlBase;
        login.emailSelector.on('input', login.validateForm);
        login.passSelector.on('input', login.validateForm);
        login.btnSelector.click(login.ajax.request);
    },

    ajax: {
        request: function() {
            //console.log(login.urlAjax);
            $.ajax({
                url: login.urlAjax,
                data: {
                    email: login.emailSelector.val(),
                    pass: login.passSelector.val(),
                    key: $('#nonceKey').text(),
                    value: $('#nonceValue').text(),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(login.ajax.success)
                .fail(login.ajax.error);
        },

        success: function(data) {
            //console.log(data);
            if(data.success === false) {
                console.log(login.errorSelector);
                login.errorSelector.text(data.caption);
                login.errorSelector.parent().removeClass('d-none');
                if( !(typeof data.key === 'undefined' || typeof data.value === 'undefined') ) {
                    login.keySelector.text(data.key);
                    login.valueSelector.text(data.value);
                }
            } else {
                login.errorSelector.parent().addClass('d-none');

                window.location.replace(login.urlBase + data.actionDestino);
            }
        },

        error: function() {
            console.log('ajax login error');
        },
    },

    validateForm: function() {
        var email_ok = utils.validateTextInput(login.emailSelector, /^[a-zA-Z][\w.-]*@[\w.-]+\.\w+$/);
        var pass_ok = utils.validateTextInput(login.passSelector, /\S{6,16}/);
        console.log(pass_ok);
        if(email_ok && pass_ok) {
            //console.log('validado');
            login.btnSelector.prop('disabled', false);
        } else {
            //console.log('no validado');
            login.btnSelector.prop('disabled', true);
        }
    }
};

var registration = {
    urlAjax: '/',
    urlBase: '/',
    nameSelector: $('#name'),
    telefonoSelector: $('#telefono'),
    emailSelector: $('#email'),
    passSelector: $('#password'),
    pass2Selector: $('#password2'),
    btnSelector: $('#registrationBtn'),
    errorSelector: $('#registrationError'),
    keySelector: $('#nonceKey'),
    valueSelector: $('#nonceValue'),

    start: function(urlBase) {
        registration.urlAjax = urlBase + 'identity/ajaxRegistration';
        registration.urlBase = urlBase;
        registration.nameSelector.on('input', registration.validateForm);
        registration.telefonoSelector.on('input', registration.validateForm);
        registration.emailSelector.on('input', registration.validateForm);
        registration.passSelector.on('input', registration.validateForm);
        registration.pass2Selector.on('input', registration.validateForm);
        registration.btnSelector.click(registration.ajax.request);
    },

    validateForm: function() {
        //console.log('validateForm')
        var name_ok = utils.validateTextInput(registration.nameSelector, /\w+/);
        var email_ok = utils.validateTextInput(registration.emailSelector, /^[a-zA-Z][\w.-]*@[\w.-]+\.\w+$/);
        var pass_ok = utils.validateTextInput(registration.passSelector, /\S{6,16}/);
        var pass2_ok = utils.validateIdenticalTextInput(registration.pass2Selector, registration.passSelector);

        if (name_ok && email_ok && pass_ok && pass2_ok) {
            registration.btnSelector.prop('disabled', false);
        } else  {
            registration.btnSelector.prop('disabled', true);
        }
    },

    ajax: {
        request: function() {
            console.log(registration.urlAjax);
            $.ajax({
                url: registration.urlAjax,
                data: {
                    name: registration.nameSelector.val(),
                    telefono: registration.telefonoSelector.val(),
                    email: registration.emailSelector.val(),
                    pass: registration.passSelector.val(),
                    key: $('#nonceKey').text(),
                    value: $('#nonceValue').text(),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(registration.ajax.success)
                .fail(registration.ajax.error);
        },

        success: function(data) {
            console.log('data success ' + data.success);
            if(data.success === false) {
                registration.errorSelector.text(data.caption);
                registration.errorSelector.parent().removeClass('d-none');
                console.log('typeof data key ' + typeof data.key);
                if( !(typeof data.key === 'undefined' || typeof data.value === 'undefined') ) {
                    console.log('data key ' + data.key);
                    registration.keySelector.text(data.key);
                    registration.valueSelector.text(data.value);
                }
            } else {
                registration.errorSelector.parent().addClass('d-none');
                window.location.replace(registration.urlBase);
            }
            //console.log(data);
        },

        error: function() {
            console.log('ajax registration error');
            registration.errorSelector.parent().removeClass('display-none');
        },
    }
};

var changePass = {
    keySelector: $('#nonceKey'),
    valueSelector: $('#nonceValue'),
    //keyHtmlId: '#nonceKey',
    //valueHtmlId: '#nonceValue',
    oldSelector: $('#old'),
    newSelector: $('#new'),
    confirmSelector: $('#confirm'),
    btnSelector: $('#changePassBtn'),

    start: function(urlBase) {
        changePass.ajax.url = urlBase + 'cuenta/ajaxChangePass';
        changePass.form.setupListeners();
    },

    form: {
        setupListeners: function() {
            changePass.newSelector.on('input', changePass.form.validate);
            changePass.oldSelector.on('input', changePass.form.validate);
            changePass.confirmSelector.on('input', changePass.form.validate);
            changePass.btnSelector.click(changePass.ajax.request);
        },

        validate: function() {

            //console.log('validating form');
            let old_ok = utils.validateTextInput(changePass.oldSelector, /\S{6,16}/);
            let new_ok = utils.validateTextInput(changePass.newSelector, /\S{6,16}/);
            let confirm_ok = utils.validateIdenticalTextInput(changePass.confirmSelector, changePass.newSelector);

            if (old_ok && new_ok && confirm_ok) {
                changePass.btnSelector.prop('disabled', false);
            } else {
                changePass.btnSelector.prop('disabled', true);
            }
        },

        empty: function() {
            changePass.form.emptyElement(changePass.oldSelector);
            changePass.form.emptyElement(changePass.newSelector);
            changePass.form.emptyElement(changePass.confirmSelector);
            changePass.btnSelector.prop('disabled', true);
        },

        emptyElement: function(selector) {
            selector.val('');
            utils.falseCleanValid(selector);
        },
    },

    feedback: {
        success: {
            selector: $('#changePassSuccess'),

            show: function() {
                changePass.feedback.danger.hide();
                changePass.feedback.success.selector.parent().removeClass('d-none');
            },

            hide: function() {
                changePass.feedback.success.selector.parent().addClass('d-none');
            },
        },

        danger: {
            selector: $('#changePassError'),

            show: function(caption) {
                changePass.feedback.success.hide();
                changePass.feedback.danger.selector.empty().append(caption);
                changePass.feedback.danger.selector.parent().removeClass('d-none');
            },

            hide: function() {
                changePass.feedback.danger.selector.parent().addClass('d-none');
            }
        },

        hideAll: function() {
            changePass.feedback.success.hide();
            changePass.feedback.danger.hide();
        }
    },

    ajax: {
        url: '/',

        request: function() {
            $.ajax({
                url: changePass.ajax.url,
                data: {
                    old: changePass.oldSelector.val(),
                    new: changePass.newSelector.val(),
                    key: changePass.keySelector.text(),
                    value: changePass.valueSelector.text(),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(changePass.ajax.success)
                .fail(changePass.ajax.error);
        },

        success: function(data) {
            if (data.success) {
                changePass.feedback.success.show();
                changePass.form.empty();

            } else {
                changePass.feedback.danger.show(data.caption);
                if( !(typeof data.key === 'undefined' || typeof data.value === 'undefined') ) {
                    changePass.keySelector.text(data.key);
                    changePass.valueSelector.text(data.value);
                }
            }
        },

        error: function() {
            changePass.feedback.hideAll();
        },
    },
};

var logout = {
    urlAjax: '/',
    urlBase: '/',
    logoutSelector: $('#logout'),
    logoutSmSelector: $('#logout-sm'),

    start: function(urlBase) {
        logout.urlAjax = urlBase + 'identity/ajaxLogout';
        logout.urlBase = urlBase;
        logout.logoutSelector.click(logout.ajax.request)
        logout.logoutSmSelector.click(logout.ajax.request)
    },

    ajax: {
        request: function() {
            $.ajax({
                url: logout.urlAjax,
                type: 'POST',
                dataType: 'json',
            })
                .done(logout.ajax.success)
                .fail(logout.ajax.error);
        },

        success: function() {
            window.location.replace(logout.urlBase);
        },

        error: function() {
            console.log('Error haciendo logout');
        },
    },
};

var search = {
    urlAjax: '/',
    urlProductDetail: '/',
    inputSelector: $('#search-input'),
    collapseSelector: $('#search-collapse'),
    listSelector: $('#search-list'),
    alertSelector: $('#search-alert'),
    //itemClase: '.btn-search-collapse',

    start: function(urlBase) {
        search.urlAjax = urlBase + 'tienda/ajaxSearch';
        search.urlProductDetail = urlBase + 'tienda/detalle/';
        //search.collapseSelector.collapse();
        search.inputSelector.on('input', search.validateInput);
        //search.itemSelector.click(search.itemSelectorClick);
    },

    validateInput: function() {
        if(/\w{3}\w*/.test(search.inputSelector.val())) {
            search.ajax.request()
            //search.collapseSelector.collapse('show');
        } else {
            search.collapseSelector.collapse('hide');
        }
    },

    ajax: {
        request: function() {
            $.ajax({
                url: search.urlAjax,
                data: { q: search.inputSelector.val() },
                type: 'GET',
                dataType: 'json',
            })
                .done(search.ajax.success)
                .fail(search.ajax.error);
        },

        success: function(data) {
            //console.log(data.cantidad);
            if(data.cantidad == 0) {
                //console.log('cantidad 0: ' + data.cantidad);
                search.alertSelector.removeClass('display-none');
                search.listSelector.text('');
            } else {
                //console.log('cantidad: ' + data.cantidad);
                search.alertSelector.addClass('display-none');
                search.listSelector.text('');
                search.listSelector.append(data.payload);
                //$(search.itemClase).click(search.itemSelectorClick);
            }
            search.collapseSelector.collapse('show');
        },

        error: function() {
            //console.log('ajax search error');
            search.collapseSelector.collapse('hide');
        },
    },

    /*itemSelectorClick: function() {
        window.location.replace(search.urlProductDetail + $(this).data('id'));
        //console.log('Se presionó ' + $(this).data('clave'));
    }*/
};

var paginador = {
    urlAjax: '/',
    resumenSelector: $('#paginadorResumen'),
    loadingSelector: $('#paginadorLoading'),
    emptySelector: $('#paginadorEmpty'),
    bodySelector: $('#paginadorBody'),
    categoriaSelector: $('#cfg-categoria'),
    inputNumberClass: "input[type='number']",
    addToCartClass: '.add-to-cart',

    start: function(urlBase) {
        //console.log('starting paginador...');
        paginador.urlAjax = urlBase + 'tienda/ajaxPaginador';
        paginador.ajax.request();
    },

    ajax: {
        request: function() {
            $.ajax({
                url: paginador.urlAjax,
                data: {
                    categoria: paginador.categoriaSelector.text(),
                },
                type: 'GET',
                dataType: 'json',
            })
                .done(paginador.ajax.success)
                .fail(paginador.ajax.error)
                .always( function() { paginador.loadingSelector.addClass('d-none'); });
        },

        success: function(data) {
            paginador.bodySelector.empty();
            paginador.resumenSelector.empty();
            if(data.cantidad == 0) {
                paginador.bodySelector.addClass('d-none');
                paginador.emptySelector.removeClass('d-none');
            } else {
                paginador.resumenSelector.append(data.resumen);
                paginador.emptySelector.addClass('d-none');
                paginador.bodySelector.removeClass('d-none');
                paginador.bodySelector.append(data.payload);
                $(paginador.addToCartClass).click(cart.store.btnAddClick);
            }
        },

        error: function() {
            paginador.emptySelector.removeClass('d-none');
            paginador.resumenSelector.empty();
        },
    },
};

var cart1 = {
    listSelector: $('#cart-list'),
    //listCountItemsString: '#cart-list',
    listEmptySelector: $('#cart-list-empty'),
    listFooterSelector: $('#cart-list-footer'),
    listTotalSelector: $('#cart-list-total'),
    itemsCountSelector: $('#cart-items-count'),
    btnVaciarSelector: $('#cart-btn-vaciar'),

    start: function(urlBase) {
        cart.ajaxItems.urlAjax = urlBase + 'cart/ajaxItems';
        //cart.ajaxAttach.urlAjax = urlBase + 'cart/ajaxAttachProduct/';
        cart.ajaxAttach.urlAjax = urlBase + 'cart/ajaxAttachItem/';
        cart.ajaxChangeCantidad.urlAjax = urlBase + 'cart/ajaxChangeCantidad';
        cart.ajaxClear.urlAjax = urlBase + 'cart/ajaxClear';
        cart.btnVaciarSelector.click(cart.vaciarClick);
        cart.ajaxItems.request();
    },

    refresh: function() {
        let children = cart.listSelector.children('li');
        if(children.length === 0) {
            cart.listTotalSelector.empty().text('0');
            cart.listFooterSelector.addClass('d-none');
            cart.listEmptySelector.removeClass('d-none');
        } else {
            cart.listFooterSelector.removeClass('d-none');
            cart.listEmptySelector.addClass('d-none');
            cart.list.total.refresh();
        }
        cart.nav.refresh();

    },

    attachProducto: function(id, cantidad) {
        cart.list.newItem(id);
        // El ajax no está condicionado a que el producto ya exista en el DOM para asegurar el item en el backend
        cart.ajaxAttach.request(id, cantidad);
    },

    vaciarClick: function() {
        cart.ajaxClear.request();

        cart.listSelector.empty();
        cart.refresh();
    },

    ajaxItems: {
        urlAjax: '/',

        request: function() {
            $.ajax({
                url: cart.ajaxItems.urlAjax,
                type: 'GET',
                dataType: 'json',
            })
                .done(cart.ajaxItems.success)
                .fail(cart.ajaxItems.error)
        },

        success: function(data) {
            cart.list.replaceAllItems(data);
        },

        error: function() {
            console.log('Error en tienda/ajaxItems');
        },
    },

    ajaxAttach: {
        urlAjax: '/',
        lastId: 0,

        request: function(id, cantidad) {
            cart.ajaxAttach.lastId = id;
            /*$.ajax({
                url: cart.ajaxAttach.urlAjax + id,
                type: 'GET',
                dataType: 'json',
            })
                .done(cart.ajaxAttach.success)
                .fail(cart.ajaxAttach.error);*/
            $.ajax({
                url: cart.ajaxAttach.urlAjax,
                data: {
                    hexProductoId: id,
                    cantidad: cantidad,
                },
                type: 'GET',
                dataType: 'json',
            })
                .done(cart.ajaxAttach.success)
                .fail(cart.ajaxAttach.error);
        },

        success: function(data) {
            $('#' + cart.ajaxAttach.lastId)
                .attr('data-precio', data.precio)
                .empty()
                .append(data.html);

            cart.list.configureItemInput(cart.ajaxAttach.lastId);
            cart.refresh();
        },

        error: function() {
            console.log('Error en tienda/ajaxAttachProduct');
        },
    },

    ajaxChangeCantidad: {
        urlAjax: '/',

        request: function(data) {
            $.ajax({
                url: cart.ajaxChangeCantidad.urlAjax,
                data: data,
                type: 'POST',
                dataType: 'json',
            })
                .fail(function() { console.log('Error en ajaxChangeCantidad'); });
        }
    },

    ajaxClear: {
        urlAjax: '/',

        request: function() {
            $.ajax({
                url: cart.ajaxClear.urlAjax,
                type: 'POST',
                dataType: 'json',
            })
                .fail(function() { console.log('Error en ajaxClear'); });
        }
    },

    list: {
        newItem: function(id) {
            if($('#' + id).length) {
                console.log('El item ya existe en la lista');
                return;
            }

            cart.listSelector.append(cart.list.createDomLiItem(id, false, 'Cargando item: ' + id));
        },

        replaceAllItems: function(data) {
            cart.listSelector.empty();
            $.each(data, function(key, value) {
                cart.listSelector.append(cart.list.createDomLiItem(value.id, value.precio, value.html));
                cart.list.configureItemInput(value.id)
            });
            cart.refresh();
        },

        deleteItem: function(liElement) {
            liElement.remove();
            cart.refresh();
        },

        // To add button listener and expand the spinner
        configureItemInput: function(id) {
            // Se selecciona específicamente por htmlID porque si se aplica para todos y se duplica .inputSpinner()
            // se corrompen los elementos existentes
            let inputSelector = $('#input-' + id);
            inputSelector.inputSpinner();

            inputSelector.on('change', cart.list.itemInputChange);
        },

        itemInputChange: function(event) {
            //console.log($(event));
            // este evento debe ser distinto del (todavía no creado) store.itemInputChange(), debido a que se debe hacer el databinding

            let inputElement = $(event.target);
            cart.ajaxChangeCantidad.request({id: event.target.id, cantidad: inputElement.val()});

            // Si la función .parents() falla porque en el DOM superior haya más li items, usar el #id-hex para ubicar el li
            let liElement = inputElement.parents('li')[0];

            // Importante!!! .val() devuelve un string no un número
            if(inputElement.val() === '0' || inputElement.val() === ''){
                cart.list.deleteItem(liElement);
                return;
            }

            let monto = parseFloat($(liElement).data('precio')) * inputElement.val();
            $('#monto-' + liElement.id).empty().text(utils.moneyFormat(monto));

            cart.list.total.refresh();
        },

        // precio y append son opcionales enviando false
        createDomLiItem: function(id, precio, append) {
            let li = $('<li>')
                .attr('id', id)
                .addClass('list-group-item')
                .addClass('p-1');
            if(precio !== false) { li.attr('data-precio', precio); }
            if(append !== false) { li.append(append) }

            return li;
        },

        total: {
            refresh: function() {
                let children = cart.listSelector.children('li');

                let total = 0.0;
                $.each(children, function(key, value) {
                    total += parseFloat($(value).data('precio') * $('#input-' + value.id).val());
                });

                cart.listTotalSelector.empty().text(utils.moneyFormat(total));
            }
        },
    },

    nav: {
        refresh: function() {
            cart.itemsCountSelector.text(cart.listSelector.children('li').length);
        }
    },

    store: {

        attachButtonsListeners: function () {
            $('.add-to-cart').click(cart.store.btnAddClick);
            cart.store.configureItemInput();
        },

        btnAddClick: function(event) {
            cart.attachProducto($(event.target).data('id'), $(event.target).data('cantidad'));
        },

        configureItemInput: function () {
            let inputSelector = $('.add-to-cart-input');
            inputSelector.inputSpinner();
            inputSelector.on('change', cart.store.onItemInputChange);
        },

        onItemInputChange: function (event) {
            let inputElement = $(event.target);
            if(isNaN(parseInt(inputElement.val()))){
                inputElement.val(1);
            }
            $('#' + inputElement.data('btnid')).attr('data-cantidad', inputElement.val());
        },
    },
};

var eligeTienda = {
    urlBase: '/',
    modalSelector: $('#EligeTiendaModal'),
    triggerSelector: $('#EligeTiendaModalTrigger'),
    triggerCaptionSelector: $('#EligeTiendaModalTriggerCaption'),
    //titleSelector: $('#EligeTiendaModalTitle'),
    errorSelector: $('#EligeTiendaModalError'),
    loadingSelector: $('#EligeTiendaModalLoading'),
    listSelector: $('#EligeTiendaModalList'),
    sucursalItemSelector: $('#elige-sucursal-item'),
    btnClass: '.btn-elige-tienda',

    start: function(urlBase) {
        eligeTienda.urlBase = urlBase;
        eligeTienda.ajaxList.url = urlBase + 'sucursales/ajaxList/';
        eligeTienda.ajaxElegir.url = urlBase + 'sucursales/ajaxElegir/';

        eligeTienda.modalSelector.on('show.bs.modal', eligeTienda.modalShow);
    },

    modalShow: function(event) {
        //console.log(event.relatedTarget);

        let trigger = 'elige';
        if (typeof event.relatedTarget === 'undefined') {
            trigger = 'sucursales';
        } else {
            if ($(event.relatedTarget).data('trigger') === 'sucursales') {
                trigger = 'sucursales';
            }
        }

        eligeTienda.ajaxList.request(trigger);
    },

    btnClick: function(event) {
        let btnSelector = $(event.currentTarget);
        //console.log('btn eligeTienda con id: ', btnSelector.data('id'));
        eligeTienda.ajaxElegir.request(btnSelector.data('id'));
        eligeTienda.triggerCaptionSelector.empty().text(btnSelector.data('nombre'));
        eligeTienda.modalSelector.modal('hide');
    },

    ajaxList: {
        url: '/',
        clickableItems: false,

        request: function(trigger) {
            this.clickableItems = trigger === 'elige';
            //console.log(this.clickableItems);
            $.ajax({
                url: eligeTienda.ajaxList.url + trigger,
                type: 'GET',
                dataType: 'json',
            })
                .done(eligeTienda.ajaxList.success)
                .fail(eligeTienda.ajaxList.error)
                .always(function() { eligeTienda.loadingSelector.addClass('d-none'); });
        },

        success: function(data) {
            eligeTienda.errorSelector.addClass('d-none');
            eligeTienda.listSelector.empty().append(data);
            if (eligeTienda.ajaxList.clickableItems) {
                $(eligeTienda.btnClass).click(eligeTienda.btnClick)
            }
        },

        error: function(jqXHR, textStatus, errorThrown) {
            if(jqXHR.status === 302) {
                window.location.replace(eligeTienda.urlBase + 'identity/login');
            }
            //console.log(jqXHR.status);
            eligeTienda.errorSelector.text('No se pudo obtener lista de sucursales');
            eligeTienda.errorSelector.removeClass('d-none');
        },
    },

    ajaxElegir: {
        url: '/',

        request: function(tiendaId) {
            $.ajax({
                url: eligeTienda.ajaxElegir.url + tiendaId,
                type: 'POST',
                dataType: 'json',
            })
                .done(eligeTienda.ajaxElegir.success);
            pedido.publicFun.sucursalChange(tiendaId);
        },

        success: function(data) {
            //eligeTienda.sucursalItemSelector.empty().append(data.payload);
            orden.replaceMetodoSucursalTab(data.payload);
        },
    },
};

var nuevoProyecto = {

    aliasSelector: $('#alias'),
    cpSelector: $('#np_cp'),
    //row2Selector: $('#row2'),
    calleSelector: $('#calle'),
    numextSelector: $('#numext'),
    numintSelector: $('#numint'),
    entreCallesSelector: $('#entre_calles'),
    row3Selector: $('#row3'),
    colonia3Selector: $('#colonia3'),
    estado3Selector: $('#estado3'),
    ciudad3Selector: $('#ciudad3'),
    row4Selector: $('#row4'),
    colonia4Selector: $('#colonia4'),
    estado4Selector: $('#estado4'),
    ciudad4Selector: $('#ciudad4'),
    row5Selector: $('#row5'),
    colonia5Selector: $('#colonia5'),
    btnOkSelector: $('#NuevoProyectoModalOk'),
    modalErrorSelector: $('#NuevoProyectoModalError'),

    start: function(urlBase) {
        //gmap.start();
        nuevoProyecto.button.url = urlBase + 'cuenta/ajaxNuevoProyecto';
        nuevoProyecto.aliasSelector.on('input', nuevoProyecto.form.validate);
        nuevoProyecto.cpSelector.on('input', nuevoProyecto.cp.onInput);
        nuevoProyecto.calleSelector.on('input', nuevoProyecto.form.validate);
        nuevoProyecto.numextSelector.on('input', nuevoProyecto.form.validate);
        nuevoProyecto.colonia3Selector.on('input', nuevoProyecto.form.onInputColonia3);
        nuevoProyecto.estado3Selector.change(nuevoProyecto.estado.onChanged);
        nuevoProyecto.ciudad3Selector.change(nuevoProyecto.form.validate);
        nuevoProyecto.colonia4Selector.change(nuevoProyecto.form.onChangeColonia4);
        nuevoProyecto.colonia5Selector.on('input', nuevoProyecto.form.onInputColonia5);
        nuevoProyecto.btnOkSelector.click(nuevoProyecto.button.onClick);
    },

    form: {
        selector: $('#part1-form'),

        validate: function() {
            let aliasOk = utils.validateTextInput(nuevoProyecto.aliasSelector, /\w{2}\w*/);
            let cpOk = nuevoProyecto.cp.isValid();
            let calleOk = utils.validateTextInput(nuevoProyecto.calleSelector, /\w{2}\w*/);
            let numextOk = utils.validateTextInput(nuevoProyecto.numextSelector, /\w\w*/);
            let colonia3Ok = utils.validateTextInput(nuevoProyecto.colonia3Selector, /\w{2}\w*/);
            let estado3Ok = utils.validateSelect(nuevoProyecto.estado3Selector);
            let ciudad3Ok = utils.validateSelect(nuevoProyecto.ciudad3Selector);
            let colonia4Ok = utils.validateSelect(nuevoProyecto.colonia4Selector);
            let colonia5Ok = utils.validateTextInput(nuevoProyecto.colonia5Selector, /\w{2}\w*/);
            if(aliasOk && cpOk && calleOk && numextOk && (
                ( colonia3Ok && estado3Ok && ciudad3Ok) ||
                colonia4Ok ||
                colonia5Ok
            )) {
                nuevoProyecto.btnOkSelector.attr('disabled', false);
                //nuevoProyecto.direction.setText(nuevoProyecto.direction.generate());
                nuevoProyecto.direction.updateCaption();
            } else {
                nuevoProyecto.btnOkSelector.attr('disabled', true);
            }
        },

        onChangeColonia4: function() {
            if(utils.validateSelect(nuevoProyecto.colonia4Selector)) {
                nuevoProyecto.row5Selector.addClass('d-none');
                nuevoProyecto.direction.colonia = $('#colonia4 option:selected').text();
            } else {
                nuevoProyecto.row5Selector.removeClass('d-none');
            }
            nuevoProyecto.form.validate();
        },

        onInputColonia3: function() {
            nuevoProyecto.direction.colonia = nuevoProyecto.colonia3Selector.val();
            nuevoProyecto.form.validate();
        },

        onInputColonia5: function() {
            nuevoProyecto.direction.colonia = nuevoProyecto.colonia5Selector.val();
            nuevoProyecto.form.validate();
        },

        hide: function() {
            nuevoProyecto.form.selector.addClass('d-none');
        },

        isVisible: function() {
            return !nuevoProyecto.form.selector.hasClass('d-none');
        }
    },

    button: {
        url: '/',

        onClick: function() {
            console.log('click button');
            if (nuevoProyecto.form.isVisible()) {
                if (typeof google !== 'undefined') {
                    gmap.display('map', nuevoProyecto.direction.generate(), nuevoProyecto.direction.setCoords);
                    nuevoProyecto.direction.show();
                    return;
                }
            }

            $.ajax({
                url: nuevoProyecto.button.url,
                data: {
                    alias: nuevoProyecto.aliasSelector.val(),
                    cp: nuevoProyecto.cpSelector.val(),
                    calle: nuevoProyecto.calleSelector.val(),
                    numext: nuevoProyecto.numextSelector.val(),
                    numint: nuevoProyecto.numintSelector.val(),
                    entre_calles: nuevoProyecto.entreCallesSelector.val(),
                    colonia3: nuevoProyecto.colonia3Selector.val(),
                    ciudad: nuevoProyecto.ciudad3Selector.val(),
                    colonia4: nuevoProyecto.colonia4Selector.val(),
                    colonia5: nuevoProyecto.colonia5Selector.val(),
                    lat: nuevoProyecto.direction.lat,
                    lng: nuevoProyecto.direction.lng,
                    alt: nuevoProyecto.direction.alt,
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(nuevoProyecto.button.ajaxSuccess)
                .fail(nuevoProyecto.button.ajaxError);
        },

        ajaxSuccess: function(data) {
            if(data.success) {
                window.location.reload(true);
            } else {
                nuevoProyecto.modalErrorSelector.text(data.caption).removeClass('d-none');
            }
        },

        ajaxError: function() {
            nuevoProyecto.modalErrorSelector.addClass('d-none');
            console.log('error nuevo proyecto');
        },
    },

    cp: {
        lastRequested: '',

        onInput: function() {
            utils.forceRegexMatch(nuevoProyecto.cpSelector, /^0[1-9]\d{0,3}|[1-9]\d{1,4}|\d/);
            if(nuevoProyecto.cp.isValid()) {
                let cpVal = nuevoProyecto.cpSelector.val();
                if(cpVal !== nuevoProyecto.cp.lastRequested) {
                    nuevoProyecto.cp.lastRequested = cpVal;
                    cps.ajaxCp.request(cpVal, nuevoProyecto.cp.ajaxSuccess, nuevoProyecto.cp.ajaxError);
                }
            } else {
                nuevoProyecto.row3Selector.addClass('d-none');
                nuevoProyecto.row4Selector.addClass('d-none');
                nuevoProyecto.cp.emptyColonia();
                nuevoProyecto.row5Selector.addClass('d-none');
                nuevoProyecto.colonia5Selector.val('');
                nuevoProyecto.cp.lastRequested = '';
            }
            nuevoProyecto.form.validate();
        },

        isValid: function() {
            return utils.validateTextInput(nuevoProyecto.cpSelector, /\d{5}/);
        },

        ajaxSuccess: function(data) {
            //nuevoProyecto.row2Selector.removeClass('d-none');
            nuevoProyecto.row3Selector.addClass('d-none');
            nuevoProyecto.row4Selector.removeClass('d-none');
            nuevoProyecto.row5Selector.removeClass('d-none');
            nuevoProyecto.ciudad4Selector.val(data.ciudad.caption);
            nuevoProyecto.estado4Selector.val(data.estado.caption);
            nuevoProyecto.direction.estado = data.estado.caption;
            nuevoProyecto.cp.emptyColonia();
            $.each(data.colonias, function(key, colonia) {
                let option = $('<option>');
                option.attr('value', colonia.id).text(colonia.caption);
                nuevoProyecto.colonia4Selector.append(option);
            })
            //console.log('Ajax success CP');
        },

        ajaxError: function() {
            //nuevoProyecto.row2Selector.removeClass('d-none');
            nuevoProyecto.row3Selector.removeClass('d-none');
            nuevoProyecto.row4Selector.addClass('d-none');
            nuevoProyecto.row5Selector.addClass('d-none');
            //console.log('Ajax error CP');
        },

        emptyColonia: function() {
            nuevoProyecto.colonia4Selector.empty();
            nuevoProyecto.colonia4Selector.removeClass('is-valid');
            nuevoProyecto.colonia4Selector.append($('<option>').attr('value', 0).text('...Selecciona la Colonia...'));
        }
    },

    estado: {
        onChanged: function() {
            //console.log($('#estado3 option:selected').text());
            if (nuevoProyecto.estado3Selector.val() !== 0) {
                nuevoProyecto.direction.estado = $('#estado3 option:selected').text();
            }
            cps.ajaxCiudades.request(nuevoProyecto.estado3Selector.val(), nuevoProyecto.estado.ajaxSuccess, nuevoProyecto.estado.ajaxError);
            nuevoProyecto.form.validate();
        },

        ajaxSuccess: function(data) {
            //console.log(data);
            nuevoProyecto.estado.emptyCiudad();
            $.each(data, function(key, value) {
                let option = $('<option>');
                option.attr('value', key).text(value);
                nuevoProyecto.ciudad3Selector.append(option);
            });
            nuevoProyecto.ciudad3Selector.parent().prop('disabled', false);
        },

        ajaxError: function() {
            //console.log('Ajax success estado');
            nuevoProyecto.estado.emptyCiudad();
            nuevoProyecto.ciudad3Selector.parent().prop('disabled', true);

        },

        emptyCiudad: function() {
            nuevoProyecto.ciudad3Selector.empty();
            nuevoProyecto.ciudad3Selector.removeClass('is-valid');
            nuevoProyecto.ciudad3Selector.append($('<option>').attr('value', 0).text('...Selecciona...'));
        },
    },

    direction: {
        selector: $('#direccion'),
        colonia: '',
        estado: '',
        lat: 0,
        lng: 0,
        alt: 0,

        updateCaption: function() {
            nuevoProyecto.direction.selector.empty().append(nuevoProyecto.direction.generate());
        },

        generate: function() {
            return nuevoProyecto.calleSelector.val() + ' ' +
                nuevoProyecto.numextSelector.val() + ', Col. ' +
                nuevoProyecto.direction.colonia + ', CP ' +
                nuevoProyecto.cpSelector.val() + ', ' +
                nuevoProyecto.direction.estado;
        },

        show: function() {
            nuevoProyecto.direction.selector.parent().removeClass('d-none');
            nuevoProyecto.form.hide();
        },

        setCoords: function(lat, lng, alt) {
            console.log('setting coords to ' + lat + ', ' + lng + ', ' + alt);
            nuevoProyecto.direction.lat = lat;
            nuevoProyecto.direction.lng = lng;
            nuevoProyecto.direction.alt = alt;
        }
    },
};

var proyectos = {
    loadingSelector: $('#proyectosLoading'),
    emptySelector: $('#proyectosEmpty'),
    listSelector: $('#proyectosList'),

    start: function(urlBase) {
        proyectos.ajaxHacerPrincipal.url = urlBase + 'cuenta/ajaxHacerPrincipal/';
        proyectos.list.url = urlBase + 'cuenta/ajaxProyectos';
        proyectos.list.request();
    },

    list: {
        url: '/',

        request: function() {
            $.ajax({
                url: proyectos.list.url,
                type: 'GET',
                dataType: 'json',
            })
                .done(proyectos.list.ajaxSuccess)
                .fail(proyectos.list.ajaxError)
                .always(proyectos.loadingSelector.addClass('d-none'));
        },

        ajaxSuccess: function(data) {
            proyectos.listSelector.empty().append(data).removeClass('d-none');
            $(proyectos.ajaxHacerPrincipal.btnClass).click(proyectos.ajaxHacerPrincipal.request);
        },

        ajaxError: function() {
            proyectos.listSelector.empty();
            proyectos.emptySelector.removeClass('d-none');
        },
    },

    ajaxHacerPrincipal: {
        url: '/',
        btnClass: '.btn-hacer-principal',

        request: function() {
            console.log('pressed ' + $(this).data('id'));
            $.ajax({
                url: proyectos.ajaxHacerPrincipal.url + $(this).data('id'),
                type: 'POST',
                dataType: 'json',
            })
                .fail(proyectos.ajaxHacerPrincipal.error)
                .always(proyectos.ajaxHacerPrincipal.finish);
        },

        error: function() {
            console.log('No se pudo hacer principal el proyecto');
        },

        finish: function() {
            proyectos.list.request();
        },
    },
};

var cambiarProyecto = {
    modalSelector: $('#CambiarProyectoModal'),
    loadingSelector: $('#CambiarProyectoModalLoading'),
    errorSelector: $('#CambiarProyectoModalError'),
    listSelector: $('#CambiarProyectoModalList'),
    proyectoItemSelector: $('#elige-proyecto-item'),
    btnClass: '.btn-cambiar-proyecto',

    start: function(urlBase) {
        cambiarProyecto.ajaxList.url = urlBase + 'cuenta/ajaxListEligeProyecto';
        cambiarProyecto.ajaxElegir.url = urlBase + 'cuenta/ajaxHacerPrincipal/';
        cambiarProyecto.modalSelector.on('show.bs.modal', cambiarProyecto.modalShow)
    },

    modalShow: function() {
        if(cambiarProyecto.loadingSelector.hasClass('d-none')) {
            console.log('No es necesario cargar lista de proyectos');
            return;
        }

        cambiarProyecto.ajaxList.request();
    },

    btnClick: function() {
        console.log('btn cambiarProyecto con id: ', $(this).data('id'));
        cambiarProyecto.ajaxElegir.request($(this).data('id'));
        cambiarProyecto.modalSelector.modal('hide');
    },

    ajaxList: {
        url: '/',

        request: function() {
            $.ajax({
                url: cambiarProyecto.ajaxList.url,
                type: 'GET',
                dataType: 'json',
            })
                .done(cambiarProyecto.ajaxList.success)
                .fail(cambiarProyecto.ajaxList.error)
                .always(function() {cambiarProyecto.loadingSelector.addClass('d-none'); });
        },

        success: function(data) {
            cambiarProyecto.errorSelector.addClass('d-none');
            cambiarProyecto.listSelector.append(data);
            $(cambiarProyecto.btnClass).click(cambiarProyecto.btnClick);
        },

        error: function() {
            cambiarProyecto.errorSelector.text('No se pudo obtener lista de proyectos');
            cambiarProyecto.errorSelector.removeClass('d-none');
        }
    },

    ajaxElegir: {
        url: '/',

        request: function(proyectoId) {
            $.ajax({
                url: cambiarProyecto.ajaxElegir.url + proyectoId,
                type: 'POST',
                dataType: 'json',
            })
                .done(cambiarProyecto.ajaxElegir.success);
            pedido.publicFun.proyectoChange(proyectoId);
        },

        success: function(data) {
            cambiarProyecto.proyectoItemSelector.empty().append(data.html);
            if(!data.isValid) {
                pedido.btn.disable();
                pedido.btn.btnSelector.attr('data-proyecto', '0');
            }
        }
    },
};

var pedido = {
    started: false,
    urlBase: '/',
    tabDomicioSelector: $('#proyectoTabPedido'),
    tabSucursalSelector: $('#sucursalTabPedido'),

    start: function(urlBase) {
        pedido.started = true; // si started es true, se ejecutan las funciones de publicFun:
        pedido.urlBase = urlBase;
        pedido.btn.url = urlBase + 'cart/ajaxTipoEntrega';
        pedido.btnUndo.url = urlBase + 'cart/ajaxUndoTipoEntrega';
        pedido.tabDomicioSelector.on('shown.bs.tab', pedido.tabDomicilio.validate);
        pedido.tabSucursalSelector.on('shown.bs.tab', pedido.tabSucursal.validate);
        pedido.btn.btnSelector.click(pedido.btn.click);
        pedido.btnUndo.btnSelector.click(pedido.btnUndo.click);
    },

    publicFun: {
        proyectoChange: function(proyectoId) {
            if (pedido.started === false) { return; }
            pedido.btn.btnSelector.attr('data-proyecto', proyectoId);
            //console.log('proyectoChange, tipo: %o', pedido.btn.btnSelector.data('tipo'));
            if(pedido.btn.btnSelector.data('tipo') === 'proyecto') {
                pedido.btn.validate('proyecto');
            }
        },

        sucursalChange: function(tiendaId) {
            if (pedido.started === false) { return; }
            pedido.btn.btnSelector.attr('data-sucursal', tiendaId);

            if(pedido.btn.btnSelector.data('tipo') === 'sucursal') {
                //console.log('sucursalChange, tipo: %o', pedido.btn.btnSelector.data('tipo'));
                pedido.btn.validate('sucursal');
            }
        },
    },

    tabDomicilio: {
        validate: function() {
            pedido.btn.validate('proyecto');
        },
    },

    tabSucursal: {
        validate: function() {
            pedido.btn.validate('sucursal');
        },
    },

    btn: {
        url: '/',
        btnSelector: $('#btn-pedido-continuar'),
        errorSelector: $('#btn-pedido-continuar-error'),

        enable: function() {
            pedido.btn.btnSelector.prop('disabled', false);
        },

        disable: function() {
            pedido.btn.btnSelector.prop('disabled', true);
        },

        validate: function(tipo) {
            pedido.btn.btnSelector.attr('data-tipo', tipo);
            pedido.btn.disable();
            //console.log('data-' + tipo + ' %o', pedido.btn.btnSelector.attr('data-' + tipo));
            if(parseInt(pedido.btn.btnSelector.attr('data-' + tipo)) > 0) {
                //console.log('enabling button');
                pedido.btn.enable();
            }
        },

        click: function() {
            console.log('Click en btn-pedido-continuar');

            $.ajax({
                url: pedido.btn.url,
                data: {
                    tipo: pedido.btn.btnSelector.attr('data-tipo'),
                    proyecto: pedido.btn.btnSelector.attr('data-proyecto'),
                    sucursal: pedido.btn.btnSelector.attr('data-sucursal'),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(pedido.btn.ajaxSuccess)
                .fail(pedido.btn.ajaxError);
        },

        ajaxSuccess: function() {
            console.log('ajax success');
            pedido.btn.errorSelector.addClass('d-none');
            window.location.replace(pedido.urlBase + 'orden/pago');
        },

        ajaxError: function() {
            console.log('ajax error');
            pedido.btn.errorSelector.removeClass('d-none');
        },
    },

    btnUndo: {
        url: '/',
        btnSelector: $('#btn-cambiar-tipo-entrega'),

        click: function() {
            //console.log('btn undo clicked');
            $.ajax({
                url: pedido.btnUndo.url,
                type: 'POST',
                dataType: 'json',
            })
                .always(function() {
                    window.location.reload();
                });
        },
    },
};

var contactos = {

    start: function(urlBase) {
        contactos.nuevo.url = urlBase + 'cuenta/ajaxCrearContacto';
        contactos.nuevo.btnOkSelector.click(contactos.nuevo.onClickOk);
        contactos.nuevo.nombreSelector.on('input', contactos.nuevo.validateForm);
        contactos.nuevo.telSelector.on('input', contactos.nuevo.validateTel);
        contactos.nuevo.extensionSelector.on('input', contactos.nuevo.validateExtension);
        contactos.nuevo.movilSelector.on('input', contactos.nuevo.validateMovil);
        contactos.nuevo.emailSelector.on('input', contactos.nuevo.validateForm);
    },

    nuevo: {
        url: '/',
        btnTriggerSelector: $('#btn-proyecto-agregar-contacto'),
        modalSelector: $('#CrearContactoModal'),
        btnOkSelector: $('#CrearContactoModalOk'),
        errorSelector: $('#CrearContactoModalError'),
        nombreSelector: $('#nombre'),
        cargoSelector: $('#cargo'),
        telSelector: $('#tel'),
        extensionSelector: $('#extension'),
        movilSelector: $('#movil'),
        emailSelector: $('#email'),

        validateForm: function() {
            let nombreOk = utils.validateTextInput(contactos.nuevo.nombreSelector, /\w{2}\w*/);
            let telOk = utils.validateTextInput(contactos.nuevo.telSelector, /\d{10}/);
            let emailOk = utils.validateTextInput(contactos.nuevo.emailSelector, /^[a-zA-Z][\w.-]*@[\w.-]+\.\w+$/);
            if(nombreOk && telOk && emailOk) {
                contactos.nuevo.btnOkSelector.attr('disabled', false);
            } else {
                contactos.nuevo.btnOkSelector.attr('disabled', true);
            }
        },

        validateTel: function () {
            utils.forceRegexMatch(contactos.nuevo.telSelector, /^[2-9]\d{0,9}/);
            contactos.nuevo.validateForm();
        },

        validateExtension: function () {
            utils.forceRegexMatch(contactos.nuevo.extensionSelector, /^\d{1,5}/);
        },

        validateMovil: function () {
            utils.forceRegexMatch(contactos.nuevo.movilSelector, /^[2-9]\d{0,9}/);
        },

        onClickOk: function() {
            console.log('onClickOk button Crear Contacto');
            $.ajax({
                url: contactos.nuevo.url,
                data: {
                    proyecto: contactos.nuevo.btnTriggerSelector.data('proyecto'),
                    nombre: contactos.nuevo.nombreSelector.val(),
                    cargo: contactos.nuevo.cargoSelector.val(),
                    tel: contactos.nuevo.telSelector.val(),
                    extension: contactos.nuevo.extensionSelector.val(),
                    movil: contactos.nuevo.movilSelector.val(),
                    email: contactos.nuevo.emailSelector.val(),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(contactos.nuevo.success)
                .fail(contactos.nuevo.error);
        },

        success: function(data) {
            if(data.success) {
                window.location.reload();
            } else {
                contactos.nuevo.errorSelector.text(data.caption).removeClass('d-none');
            }
            //contactos.nuevo.errorSelector.text(data.caption).removeClass('d-none');
        },

        error: function() {
            contactos.nuevo.errorSelector.addClass('d-none');
            console.log('error nuevo contacto');
        },
    },
};

var horarios = {
    start: function(urlBase) {
        horarios.nuevo.url = urlBase + 'cuenta/ajaxNuevoHorario';
        horarios.delete.url = urlBase + 'cuenta/ajaxDeleteHorario';
        horarios.nuevo.btnOkSelector.click(horarios.nuevo.onClickOk);
        horarios.nuevo.diaSelector.change(horarios.nuevo.validateForm);
        horarios.nuevo.horaSelector.change(horarios.nuevo.validateHoras);
        horarios.delete.btnsSelector.click(horarios.delete.onClick);
    },

    nuevo: {
        url: '/',
        btnTriggerSelector: $('#btn-proyecto-editar-horario'),
        modalSelector: $('#EditarHorarioModal'),
        btnOkSelector: $('#EditarHorarioModalOk'),
        errorSelector: $('#EditarHorarioModalError'),
        lunesSelector: $('#lunes'),
        martesSelector: $('#martes'),
        miercolesSelector: $('#miercoles'),
        juevesSelector: $('#jueves'),
        viernesSelector: $('#viernes'),
        sabadoSelector: $('#sabado'),
        diaSelector: $('.dia-checkbox'),
        diaChecked: '.dia-checkbox:checked',
        horaSelector: $('.hora-checkbox'),
        inicioSelector: $('#inicio'),
        finSelector: $('#fin'),

        validateForm: function() {
            //console.log('Validando form');
            if($(horarios.nuevo.diaChecked).length) {
                horarios.nuevo.btnOkSelector.attr('disabled', false);
            } else {
                horarios.nuevo.btnOkSelector.attr('disabled', true);
            }
        },

        validateHoras: function() {
            if(parseInt(horarios.nuevo.inicioSelector.val()) >= parseInt(horarios.nuevo.finSelector.val())) {
                horarios.nuevo.finSelector.val(parseInt(horarios.nuevo.inicioSelector.val()) + 60);
            }

            horarios.nuevo.validateForm();
        },

        onClickOk: function() {
            console.log('onClickOk button Nuevo Horario');
            $.ajax({
                url: horarios.nuevo.url,
                data: {
                    proyecto: horarios.nuevo.btnTriggerSelector.data('proyecto'),
                    lunes: horarios.nuevo.lunesSelector.prop('checked') ? 1 : 0,
                    martes: horarios.nuevo.martesSelector.prop('checked') ? 1 : 0,
                    miercoles: horarios.nuevo.miercolesSelector.prop('checked') ? 1 : 0,
                    jueves: horarios.nuevo.juevesSelector.prop('checked') ? 1 : 0,
                    viernes: horarios.nuevo.viernesSelector.prop('checked') ? 1 : 0,
                    sabado: horarios.nuevo.sabadoSelector.prop('checked') ? 1 : 0,
                    inicio: horarios.nuevo.inicioSelector.val(),
                    fin: horarios.nuevo.finSelector.val(),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(horarios.nuevo.success)
                .fail(horarios.nuevo.error);
        },

        success: function(data) {
            if(data.success) {
                window.location.reload(true);
            } else {
                horarios.nuevo.errorSelector.text(data.caption).removeClass('d-none');
            }
            //horarios.nuevo.errorSelector.text(data.caption).removeClass('d-none');
        },

        error: function() {
            horarios.nuevo.errorSelector.addClass('d-none');
            console.log('error nuevo contacto');
        },
    },

    delete: {
        url: '/',
        btnsSelector: $('.btn-delete-horario'),
        lastClickedBtnSelector: null,

        onClick: function() {
            //console.log($(this).parent().parent());
            //$(this).parent().parent().remove();
            horarios.delete.lastClickedBtnSelector = $(this);
            $.ajax({
                url: horarios.delete.url,
                data: {
                    proyecto: $(this).data('proyecto'),
                    inicio: $(this).data('inicio'),
                    fin: $(this).data('fin'),
                },
                type: 'POST',
                dataType: 'json',
            })
                .done(horarios.delete.success)
                .fail(horarios.delete.error);
        },

        success: function() {
            horarios.delete.lastClickedBtnSelector.parent().parent().remove();
            horarios.delete.lastClickedBtnSelector = null;
        },

        error: function() {
            console.log('Error borrando horario');
        },
    },
};

var cps = {

    start: function(urlBase) {
        cps.ajaxCp.url = urlBase + 'cps/ajaxCp/';
        cps.ajaxCiudades.url = urlBase + 'cps/ajaxCiudades/';
    },

    ajaxCp: {
        url: '/',

        request: function(cp, callbackOk, callbackKo) {
            $.ajax({
                url: cps.ajaxCp.url + cp,
                type: 'GET',
                dataType: 'json',
            })
                .done(callbackOk)
                .fail(callbackKo);
        },
    },

    ajaxCiudades: {
        url: '/',

        request: function(estadoId, callbackOk, callbackKo) {
            $.ajax({
                url: cps.ajaxCiudades.url + estadoId,
                type: 'GET',
                dataType: 'json',
            })
                .done(callbackOk)
                .fail(callbackKo);
        },


    },
};

var utils = {
    validateSelect: function(selector) {
        //console.log(selector.val());
        if (selector.val() === '0') {
            return utils.falseCleanValid(selector);
        }
        return utils.trueValid(selector);
    },

    validateTextInput: function(selector, regex){
        if( !selector.val() ) {
            return utils.falseCleanValid(selector);
        }
        if( regex.test(selector.val()) ) {
            return utils.trueValid(selector);
        }
        return utils.falseNotValid(selector);
    },

    validateIdenticalTextInput: function(selector, selector2) {
        //console.log('validateIdentical');
        if( !selector.val() ) {
            return utils.falseCleanValid(selector);
        }
        if( selector.val() === selector2.val()) {
            return utils.trueValid(selector);
        }
        return utils.falseNotValid(selector);
    },

    forceRegexMatch: function(selector, regex) {
        selector.val( selector.val().match(regex) );
    },

    toUpper: function( selector ) {
        selector.val( selector.val().toUpperCase() );
    },

    falseCleanValid: function(selector) {
        //console.log('falseWithCleanValid');
        selector.removeClass('is-invalid').removeClass('is-valid');
        return false;
    },

    falseNotValid: function(selector) {
        selector.removeClass('is-valid').addClass('is-invalid');
        return false;
    },

    trueValid: function(selector) {
        selector.removeClass('is-invalid').addClass('is-valid');
        return true;
    },

    moneyFormat: function(number) {
        return number.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    },
};
