<?php
declare(strict_types=1);

use App\Application;
use Dotenv\Dotenv;

error_reporting(E_ALL);
$rootPath = dirname(__DIR__);

/** @noinspection PhpIncludeInspection */
require_once "{$rootPath}/vendor/autoload.php";

Dotenv::create($rootPath)->load();

try {
    echo (new Application($rootPath))->run();
} catch (Exception $e) {
    echo "{$e->getMessage()}<br>";
    echo nl2br(htmlentities($e->getTraceAsString()));
}
