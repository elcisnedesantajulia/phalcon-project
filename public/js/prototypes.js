
class ElementBase {
    constructor(id) {
        this.jQSel = $('#' + id);
    }

    hasDom() {
        return this.jQSel.length > 0;
    }

    getDataValue(key) {
        return this.jQSel.data(key);
    }

    disable() {
        this.jQSel.parent().attr('disabled', true);

        return this;
    }

    enable() {
        this.jQSel.parent().removeAttr('disabled');

        return this;
    }
}

class Container extends ElementBase {
    constructor(id) {
        super(id);
    }

    replace(newPayload, afterReplaceCallbak, scrollTop) {
        this.jQSel.empty().append(newPayload);
        //$('html, body').scrollTop(0);
        if (typeof scrollTop != 'undefined') {
            $('html, body').animate({scrollTop: 0}, 'fast');
        }

        //scroll(0, 0);
        if (typeof afterReplaceCallbak === 'function') {
            afterReplaceCallbak();
        }

        return this;
    }

    append(payload, afterAppendCallback) {
        this.jQSel.append(payload);

        if (typeof afterAppendCallback === 'function') {
            afterAppendCallback();
        }

        return this;
    }

    clear() {
        this.jQSel.empty();

        return this;
    }

    show() {
        this.jQSel.removeClass('d-none');

        return this;
    }

    hide() {
        this.jQSel.addClass('d-none');

        return this;
    }
}

class Modal extends Container {
    constructor(id, onShow, onHidden, onClickBtnok) {
        super(id);
        if (typeof onShow === 'function') {
            this.jQSel.on('show.bs.modal', onShow);
        }
        if (typeof onHidden === 'function') {
            this.jQSel.on('hidden.bs.modal', onHidden);
        }
        this.btnok = new Button(id + '-footer-btnok', onClickBtnok);
        this.modalbody = new Container(id + '-body');
    }
}

class Loader extends Container {
    constructor(name) {
        super(name + '-loader');
        this.loading = new Container(name + '-loading');
        this.empty = new Container(name + '-empty');
        this.header = new Container(name + '-header');
        this.body = new Container(name + '-body');
        this.footer = new Container(name + '-footer');
    }
}

class Button extends ElementBase {
    constructor(id, onClickCallback) {
        super(id);
        if (typeof onClickCallback === 'function') {
            this.jQSel.click(onClickCallback);
        }
    }

    enable() {
        this.jQSel.prop('disabled', false);

        return this;
    }

    disable() {
        this.jQSel.prop('disabled', true);

        return this;
    }

    changeCaption(newCaption) {
        if (typeof newCaption === 'string')
        this.jQSel.html(newCaption);

        return this;
    }
}

class Link extends ElementBase {
    constructor(id, onClickCallback) {
        super(id);
        if (typeof onClickCallback === 'function') {
            this.jQSel.click(onClickCallback);
        }
    }

    enable() {
        this.jQSel.attr('href', '#')
    }

    disable() {
        this.jQSel.removeAttr('href');
    }
}

class FormElementBase extends ElementBase{

    setValid() {
        this.jQSel.removeClass('is-invalid').addClass('is-valid');
    }

    setInvalid() {
        this.jQSel.removeClass('is-valid').addClass('is-invalid');
    }

    removeClassesValidInvalid() {
        this.jQSel.removeClass('is-valid').removeClass('is-invalid');
    }
}

class InputText extends FormElementBase {
    constructor(id, onInputCallback, onFocusOutCallback) {
        super(id);
        if (typeof onInputCallback === 'function') {
            this.jQSel.on('input', onInputCallback);
        }

        if (typeof onFocusOutCallback === 'function') {
            this.jQSel.focusout(onFocusOutCallback);
        }
        // Avoid sending form when pressing enter
        this.jQSel.keypress(function (event) {
            if (event.charCode === 13) {
                event.preventDefault();
                event.target.blur();
            }
        });
    }

    setVal(value) {
        //if (typeof value === 'string') {
            this.jQSel.val(value);
        //}

        return this;
    }

    getVal() {
        return this.jQSel.val();
    }

    isEmpty() {
        return this.jQSel.val() === '';
    }

    validateRegex(regex) {
        if (!this.jQSel.val()) {
            this.jQSel.removeClass('is-valid').removeClass('is-invalid');
            return false;
        }
        if ( !(regex instanceof RegExp) ) {
            return false;
        }
        if (regex.test(this.jQSel.val())) {
            this.jQSel.removeClass('is-invalid').addClass('is-valid');
            return true;
        }
        this.jQSel.removeClass('is-valid').addClass('is-invalid');
        return false;
    }

    validateIdentical(text) {
        if (!this.jQSel.val()) {
            this.jQSel.removeClass('is-valid').removeClass('is-invalid');
            return false;
        }
        if (this.jQSel.val() === text) {
            this.jQSel.removeClass('is-invalid').addClass('is-valid');
            return true;
        }
        this.jQSel.removeClass('is-valid').addClass('is-invalid');
    }

    forceRegexMatch(regex) {
        this.jQSel.val(this.jQSel.val().match(regex));
    }

    toUpper() {
        this.jQSel.val(this.jQSel.val().toUpperCase());
    }

    empty() {
        this.jQSel.val('');
        this.jQSel.removeClass('is-valid').removeClass('is-invalid');

        return this;
    }
}

class InputCheckbox extends FormElementBase {
    constructor(id, onChangeChecked) {
        super(id);
        if (typeof onChangeChecked === 'function') {
            this.jQSel.change(onChangeChecked);
        }
    }

    isChecked() {
        return this.jQSel.is(':checked');
    }

    check() {
        this.jQSel.prop("checked", true);
    }

    uncheck() {
        this.jQSel.prop("checked", false);
    }

    setStatusFromBooleanable(booleanable) {
        if (booleanable === 1 || booleanable === '1' || booleanable === 'true' || booleanable === 'TRUE' || booleanable === true) {
            this.check();
        } else {
            this.uncheck();
        }

        return this;
    }
}

class InputSelect extends FormElementBase {
    constructor(id, onChangeSelection) {
        super(id);
        this.strId = id;
        if (typeof onChangeSelection === 'function') {
            this.jQSel.change(onChangeSelection);
        }
    }

    removeOptions(emptyValue) {
        this.jQSel.empty();
        this.jQSel.removeClass('is-valid');
        //console.log(typeof emptyValue);
        if (typeof emptyValue === 'string') {
            this.jQSel.append($('<option>').attr('value', '0').text(emptyValue));
        }
    }

    appendOptionsFromAjaxData(data) {
        if (typeof data !== 'object') {
            return;
        }
        let jQsel = this.jQSel;
        $.each(data, function (key, value) {
            let option = $('<option>');
            option.attr('value', key).text(value);
            jQsel.append(option);
        })
    }

    appendOptionsFromAjaxDataV2(data) { //Corregir cuando ciudades se muestre alfabéticamente
        if (typeof data !== 'object') {
            return;
        }
        let jQsel = this.jQSel;
        $.each(data, function (key, value) {
            let option = $('<option>');
            option.attr('value', value.id).text(value.caption);
            jQsel.append(option);
        })
    }

    selectOption(value) {
        this.jQSel.val(value);
        this.jQSel.removeClass('is-valid');

        return this;
    }

    selectEmptyOption() {
        this.jQSel.val('0');
        this.jQSel.removeClass('is-valid');

        return this;
    }

    getSelectedValue() {
        return this.jQSel.val();
    }

    getCaptionOfSelectedValue() {
        //console.log()
        return $("#" + this.strId + " option:selected").text();
    }

    validateNotEmptyOption() {
        if (this.jQSel.val() === '0') {
            this.jQSel.removeClass('is-valid');
            return false;
        }
        this.jQSel.addClass('is-valid');
        return true;
    }

    enable() {
        //this.jQSel.prop('disabled', false); // for another projects, this lines should be the correct
        this.jQSel.parent().prop('disabled', false);
    }

    disable() {
        //this.jQSel.prop('disabled', true); // for another projects, this lines should be the correct
        this.jQSel.parent().prop('disabled', true);
    }
}

class Remote {
    constructor(url, method, onSuccessCallback, onErrorCallback) {
        this.url = url;
        this.method = method;
        this.requestData = {};
        if (typeof onSuccessCallback === 'function') {
            this.onSuccessCallBack = onSuccessCallback;
        }
        if (onErrorCallback === 'default') {
            this.onErrorCallBack = this.defaultErrorCallback;
        } else if (typeof onErrorCallback === 'function') {
            this.onErrorCallBack = onErrorCallback;
        }
    }

    /**
     * Replace existent requestData object
     * @param newData
     */
    setDataObject(newData) {
        if (typeof newData === 'object') {
            this.requestData = newData;
        }
    }

    /**
     * Add a new element to
     * @param key
     * @param value
     */
    pushDataItem(key, value) {
        if (typeof key === 'string') {
            this.requestData[key] = value;
        }
    }

    /**
     * TO BE DEPRECATED!!! Only for compatibility
     * @param key
     * @param value
     */
    pushData(key, value) {
        this.pushDataItem(key, value)
    }

    request() {
        $.ajax(this.generateConfig())
            .done(this.onSuccessCallBack)
            .fail(this.onErrorCallBack);
    }

    generateConfig() {
        if ($.isEmptyObject(this.requestData)) {
            return {
                url: this.url,
                type: this.method,
                dataType: 'json',
            }
        }
        return {
            url: this.url,
            data: this.requestData,
            type: this.method,
            dataType: 'json',
        }
    }

    defaultErrorCallback() {
        new Alert('Error', 'Error desconocido', 'error');
    }
}

class Alert {
    constructor(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            type: type,
            styling: 'bootstrap3',
        });
    }
}

class Globals {
    constructor() {
        this.emailPattern = /^[a-zA-Z][\w.-]*@[\w.-]+\.\w+\s*$/;
        this.rfcPattern = /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]))?$/;
        this.atLeastOneChar = /\S/;
        this.atLeastTwoChars = /\S{2}/;
        this.passwordPattern = /\S{6,16}/;
        //Asiste durante la escritura para que no escriba letras ni se pase de caracteres
        this.forceCp = /^0[1-9]\d{0,3}|[1-9]\d{1,4}|\d/;
        this.cpPattern = /\d{5}/;
        //Asiste durante la escritura para que no escriba letras ni se pase de caracteres
        this.forceTel = /^[2-9]\d{0,9}/;
        this.telPattern = /\d{10}/;
        //Asiste durante la escritura para que no escriba letras ni se pase de caracteres
        this.forceTc = /^\d{1,16}/;
        this.tcPattern = /\d{15,16}/;
        //Asiste durante la escritura para que no escriba letras ni se pase de caracteres
        this.forceCvv = /^\d{1,4}/;
        this.cvvPattern = /\d{3,4}/;
    }
}
