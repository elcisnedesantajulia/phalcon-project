
let front = (function () {
    //let urlBase;
    //let resource;

    let construct = function (urlBase, resourcename) {
        login.construct(urlBase);
        register.construct(urlBase);
    }

    let onClickReset = function () {
        new PNotify({
            title: 'Coming soon!',
            text: 'Implementando funcionalidad',
            type: 'info',
            styling: 'bootstrap3',
        });
        console.log('on click reset pass');
    };

    let reset = new Link('guest-reset-pass', onClickReset);

    return {
        construct: construct,
    };
})();

let login = (function () {
    let urlBase;
    let urlAjax;

    let construct = function (urlbase) {
        urlBase = urlbase;
        urlAjax = urlbase + 'guest/ajaxLogin';
    };

    let replaceToken = function (objectTokens) {
        form.jQSel.attr('data-key', objectTokens.key);
        form.jQSel.attr('data-token', objectTokens.token);
    };

    let onClickLogin = function (event) {
        event.preventDefault();
        //console.log(event);
        let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, onAjaxFails);
        ajax.setDataObject({
            email: email.getVal(),
            pass: pass.getVal(),
            key: form.jQSel.attr('data-key'),
            token: form.jQSel.attr('data-token'),
        });
        ajax.request();
    };

    let onAjaxSuccess = function (data) {
        if (data.success) {
            if (typeof data.actionDestino === 'string') {
                window.location = urlBase + data.actionDestino;
                return;
            }
            window.location = urlBase;
        } else {
            if (typeof data.key === 'string' && typeof data.token === 'string') {
                replaceToken(data);
                register.replaceToken(data);
            }
            new Alert('Error', data.caption, 'error');
        }
    };

    let onAjaxFails = function () {
        window.location = urlBase;
    };

    let form = new Container('guest-login-form')
    let email = new InputText('guest-login-email');
    let pass = new InputText('guest-login-pass');
    /*let linkLogin =*/ new Link('guest-login-submit', onClickLogin);

    return {
        construct: construct,
        replaceToken: replaceToken,
    }
})();

let register = (function () {
    let urlBase;
    let urlAjax;
    let isValid = false;

    let construct = function (urlbase) {
        urlBase = urlbase;
        urlAjax = urlbase + 'guest/ajaxRegister';
    };

    let replaceToken = function (objectTokens) {
        form.jQSel.attr('data-key', objectTokens.key);
        form.jQSel.attr('data-token', objectTokens.token);
    };

    let onChangeForm = function () {
        let globals = new Globals();
        let name_ok = name.validateRegex(globals.atLeastTwoChars);
        let email_ok = email.validateRegex(globals.emailPattern);
        let pass_ok = pass.validateRegex(globals.passwordPattern);
        let confirm_ok = confirm.validateIdentical(pass.getVal());

        if (name_ok && email_ok && pass_ok && confirm_ok) {
            linkRegister.enable();
            isValid = true;
        } else {
            linkRegister.disable();
            isValid = false;
        }
    };

    let onClickLogin = function (event) {
        event.preventDefault();
        if (!isValid) {
            return;
        }
        let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, onAjaxFails);
        ajax.setDataObject({
            name: name.getVal(),
            email: email.getVal(),
            pass: pass.getVal(),
            key: form.jQSel.attr('data-key'),
            token: form.jQSel.attr('data-token'),
        });
        ajax.request();
    };

    let onAjaxSuccess = function (data) {
        if (data.success) {
            window.location = urlBase;
        } else {
            if (typeof data.key === 'string' && typeof data.token === 'string') {
                replaceToken(data);
                login.replaceToken(data);
            }
            new Alert('Error', data.caption, 'error');
        }
    };

    let onAjaxFails = function () {
        window.location = urlBase;
    };

    let form = new Container('guest-register-form')
    let name = new InputText('guest-register-name', onChangeForm)
    let email = new InputText('guest-register-email', onChangeForm);
    let pass = new InputText('guest-register-pass', onChangeForm);
    let confirm = new InputText('guest-register-confirm', onChangeForm);
    let linkRegister = new Link('guest-register-submit', onClickLogin);

    return {
        construct: construct,
        replaceToken: replaceToken,
    };
})();