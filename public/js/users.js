
let users = (function () {
    let urlBase;

    let construct = function (urlbase) {
        urlBase = urlbase;
        create.construct(urlbase);
        edit.construct(urlbase);
        changePass.construct(urlbase);
        restorePass.construct(urlbase);
        block.construct(urlbase);
        unblock.construct(urlbase);
    };

    let create = (function () {
        let urlAjax;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'acl/ajaxNewUser';
        };

        let onShowModal = function () {
            modal.btnok.disable();
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.setDataObject(form.getValuesObject());
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error creando usuario', data.caption, 'error');
            }
        };

        let form = (function () {

            let clean = function () {
                name.empty();
                email.empty();
                profile.selectEmptyOption();
                pass.empty();
                confirm.empty();
            };

            let getValuesObject = function () {
                return {
                    name: name.getVal(),
                    email: email.getVal(),
                    profile: profile.getSelectedValue(),
                    pass: pass.getVal(),
                };
            };

            let validate = function () {
                let globals = new Globals();
                let name_ok = name.validateRegex(globals.atLeastTwoChars);
                let email_ok = email.validateRegex(globals.emailPattern);
                let profile_ok = profile.validateNotEmptyOption();
                let pass_ok = pass.validateRegex(globals.passwordPattern);
                let confirm_ok = confirm.validateIdentical(pass.getVal());

                if (name_ok && email_ok && profile_ok && pass_ok && confirm_ok) {
                    modal.btnok.enable();
                } else {
                    modal.btnok.disable();
                }
            };

            let name = new InputText('nu-name', validate);
            let email = new InputText('nu-email', validate);
            let profile = new InputSelect('nu-profile', validate);
            let pass = new InputText('nu-pass', validate);
            let confirm = new InputText('nu-confirm', validate);

            return {
                clean: clean,
                getValuesObject: getValuesObject,
            };
        })();

        let modal = new Modal('nu-modal', onShowModal, form.clean, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    let edit = (function () {
        let urlAjax;
        let userId;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'acl/ajaxEditUser';
        }

        let onShowModal = function (event) {
            let button = $(event.relatedTarget)
            userId = button.data('id');
            form.setValues({
                name: button.data('name'),
                email: button.data('email'),
                profile: button.data('profile'),
            });
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.setDataObject(form.getValuesObject());
            ajax.pushDataItem('id', userId);
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error editando usuario', data.caption, 'error');
            }
        };

        let form = (function () {

            let clean = function () {
                name.empty();
                profile.selectEmptyOption();
            };

            let getValuesObject = function () {
                return {
                    name: name.getVal(),
                    profile: profile.getSelectedValue(),
                };
            };

            let setValues = function (objectValues) {
                name.setVal(objectValues.name);
                email.setVal(objectValues.email);
                profile.selectOption(objectValues.profile);
            }

            let validate = function () {
                let globals = new Globals();
                let name_ok = name.validateRegex(globals.atLeastTwoChars);
                let profile_ok = profile.validateNotEmptyOption();

                if (name_ok && profile_ok) {
                    modal.btnok.enable();
                } else {
                    modal.btnok.disable();
                }
            };

            let name = new InputText('eu-name', validate);
            let email = new InputText('eu-email', validate);
            let profile = new InputSelect('eu-profile', validate);

            return {
                clean: clean,
                getValuesObject: getValuesObject,
                setValues: setValues,
            }
        })();

        let modal = new Modal('eu-modal', onShowModal, form.clean, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    let changePass = (function () {
        let urlAjax;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'index/ajaxChangePass';
        };

        let onShowModal = function () {
            modal.btnok.disable();
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.setDataObject(form.getValuesObject());
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error cambiando password', data.caption, 'error');
            }
        };

        let form = (function () {

            let clean = function () {
                old.empty();
                pass.empty();
                confirm.empty();
            };

            let getValuesObject = function () {
                return {
                    old: old.getVal(),
                    pass: pass.getVal(),
                };
            };

            let validate = function () {
                let globals = new Globals();
                let old_ok = old.validateRegex(globals.passwordPattern);
                let pass_ok = pass.validateRegex(globals.passwordPattern);
                let confirm_ok = confirm.validateIdentical(pass.getVal());

                if (old_ok && pass_ok && confirm_ok) {
                    modal.btnok.enable();
                } else {
                    modal.btnok.disable();
                }
            };

            let old = new InputText('chp-old', validate);
            let pass = new InputText('chp-pass', validate);
            let confirm = new InputText('chp-confirm', validate);

            return {
                clean: clean,
                getValuesObject: getValuesObject,
            };
        })();

        let modal = new Modal('chp-modal', onShowModal, form.clean, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    let restorePass = (function () {
        let urlAjax;
        let userId;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'acl/ajaxRestorePass';
        };

        let onShowModal = function (event) {
            let button = $(event.relatedTarget)
            userId = button.data('id');
            form.setNameValue(button.data('name'));
            modal.btnok.disable();
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.setDataObject(form.getValuesObject());
            ajax.pushDataItem('id', userId);
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error restaurando password', data.caption, 'error');
            }
        };

        let form = (function () {

            let clean = function () {
                name.empty();
                pass.empty();
                confirm.empty();
            };

            let getValuesObject = function () {
                return {
                    pass: pass.getVal(),
                };
            };

            let setNameValue = function (value) {
                name.setVal(value);
            }

            let validate = function () {
                //let globals = new Globals();
                let pass_ok = pass.validateRegex(new Globals().passwordPattern);
                let confirm_ok = confirm.validateIdentical(pass.getVal());

                if (pass_ok && confirm_ok) {
                    modal.btnok.enable();
                } else {
                    modal.btnok.disable();
                }
            };

            let name = new InputText('rp-name', validate);
            let pass = new InputText('rp-pass', validate);
            let confirm = new InputText('rp-confirm', validate);

            return {
                clean: clean,
                getValuesObject: getValuesObject,
                setNameValue: setNameValue,
            };
        })();

        let modal = new Modal('rp-modal', onShowModal, form.clean, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    let block = (function () {
        let urlAjax;
        let userId;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'acl/ajaxBlockUser';
        }

        let onShowModal = function (event) {
            let button = $(event.relatedTarget)
            userId = button.data('id');
            modal.modalbody.replace('Confirma que deseas bloquear al usuario ' + button.data('name'));
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.pushDataItem('id', userId);
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error bloqueando usuario', data.caption, 'error');
            }
        };

        let modal = new Modal('bu-modal', onShowModal, null, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    let unblock = (function () {
        let urlAjax;
        let userId;

        let construct = function (urlBase) {
            urlAjax = urlBase + 'acl/ajaxUnblockUser';
        }

        let onShowModal = function (event) {
            let button = $(event.relatedTarget)
            userId = button.data('id');
            modal.modalbody.replace('Confirma que deseas desbloquear al usuario ' + button.data('name'));
        };

        let onClickBtnok = function () {
            let ajax = new Remote(urlAjax, 'POST', onAjaxSuccess, 'default');
            ajax.pushDataItem('id', userId);
            ajax.request();
        };

        let onAjaxSuccess = function (data) {
            if (data.success) {
                location.reload();
            } else {
                new Alert('Error desbloqueando usuario', data.caption, 'error');
            }
        };

        let modal = new Modal('ubu-modal', onShowModal, null, onClickBtnok);

        return {
            construct: construct,
        };
    })();

    return {
        construct: construct,
    };
})();
