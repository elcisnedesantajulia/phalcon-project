
let logout = (function () {
    let urlGuest;
    let urlAjax;

    let construct = function (urlBase) {
        urlGuest = urlBase + 'guest';
        urlAjax = urlBase + 'index/ajaxLogout';
    };

    let onClickLogout = function () {
        let ajax = new Remote(urlAjax, 'POST', onAjaxFinish, onAjaxFinish)
        ajax.request();
    };

    let onAjaxFinish = function () {
        window.location = urlGuest;
    };

    /*let logoutLink =*/ new Link('profile-logout', onClickLogout);

    return {
        construct: construct,
    }
})();